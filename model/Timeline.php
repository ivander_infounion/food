<?php

class TimelineTable extends EntityTable {
    function __construct() {
        parent::__construct();
        $this->addField(new Field('created', 'date'));
        $this->addField(new Field('desc', 'text', false, ''));

        $this->addManyToOne('foodset');
    }
}

class Timeline extends Entity
{
    function getName(){
        return "Расписание на ".$this->getCreated('d.m.Y');
    }

    //custom fields
    function getCreated($date = null) {
        return $this->getDateTime('created', $date);
    }
    function getActual() {
        $time = strtotime($this->getCreated());
        if ($time <= time()) {
            return true;
        } else {
            return false;
        }
    }
    function getDesc()
    {
        return $this->getField('desc');
    }

    //db_links
    function getFoodset()
    {
        return new Foodset($this->getFoodsetId());
    }

    function getFoodsetId()
    {
        return $this->getField('foodset');
    }

    function getFoodsetName()
    {
        return $this->getFoodset()->getName();
    }
}


class TimelineCollection extends Collection {

    function getByParams(array $params = array(), $order = 'created DESC') {
        if (isset($params['actual'])) {
            $datetime = date("Y-m-d");
            $this->addFilter("timeline_created <= '$datetime'");
        }
        return parent::getByParams($params, $order);
    }


}
?>