<?php
/* (c) 2015 InfoUnion CMS v3.0, voloshanivsky@gmail.com */
abstract class RecordfileTable extends RecordwithlangTable {
  function __construct() {
    parent::__construct();
    $this->addFields(array(
        'filename' => 'varchar(255)',
        'ext' => 'varchar(4)',
        'key' => 'varchar(15)',
    ));
  }
}

abstract class Recordfile extends Recordwithlang {

  static function getFile($ctrl, $key) {
    $db = DB::getInstance();
    $ctrl = $db->real_escape_string($ctrl);
    $key = $db->real_escape_string($key);
    $entity = strtolower($ctrl);
    $id = $db->fetchSingle("SELECT {$entity}_id FROM $entity WHERE {$entity}_key='$key'");
    if ($id) {
      $entity = ucfirst($entity);
      return new $entity($id);
    } else {
      return false;
    }
  }

  function getName($lang = 1) {
    return $this->getFieldLang('name',$lang);
  }
  function getActive($lang = 1) {
    return $this->getFieldLang('active', $lang);
  }
  function getExt() {
    return $this->getField('ext');
  }
  function isPicture() {
    if (in_array(strtolower($this->getExt()), array('jpg','jpeg','png','gif','bmp'))) {
      return true;
    } else {
      return false;
    }
  }
  function getKey() {
    return $this->getField('key');
  }
  function getFilename() {
    return $this->getField('filename');
  }
  function getRealFilename() {
    return $this->getFilename().'_'.$this->getId().'.'.$this->getExt();
  }
  function getDir($controller, $id = null) {
    $dir = 'content/'.$controller;
    if ($id) {
      $dir .= '/'.$id;
    }
    return $dir;
  }
  function getRealPath($controller, $id = null) {
    return $this->getDir($controller, $id).'/'.$this->getRealFilename();
  }
  function getRealPathPict($controller, $id = null) {
    return $this->getDir($controller, $id).'/'.$this->getRealFilename();
  }
  function getRealPreview($name = 'preview') {
    return $name.'_'.$this->getId().'.'.$this->getExt();
  }
  public function withPreview() {
    return false;
  }
  function getRealPathPreview($controller, $id = null, $namepreview = 'preview') {
    return $this->getDir($controller, $id).'/'.$this->getRealPreview($namepreview);
  }
  function getDownloadLink($ctrl) {
    return "download/{$ctrl}file/{$this->getKey()}/";
  }

  function setExt($value) {
    return $this->setField('ext', $value);
  }
  function setFilename($value) {
    return $this->setField('filename', $value);
  }
  function setOrder($value) {
    return $this->setField('order', $value);
  }

  function delete() {
    if (file_exists($this->getRealPath())) {
      unlink($this->getRealPath());
    }
    if (file_exists($this->getRealPathPreview())) {
      unlink($this->getRealPathPreview());
    }
    return parent::delete();

  }

}

abstract class RecordfileCollection extends RecordwithlangCollection {

  function add($data) {
    $data['key'] = substr(md5(time() . rand() . $data[0]), 0, 15);
    return parent::add($data);
  }

}
?>