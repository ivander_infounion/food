<?php
  /* (c) 2015 InfoUnion CMS v3.0, voloshanivsky@gmail.com */
abstract class PagewithlangTable extends EntityTable {
  function __construct() {
    parent::__construct();
    $this->addFields(array(

    ));
    $this->addManyToMany('lang');
  }
}

abstract class Pagewithlang extends Entitywithlang {

  function getUrl($lang = 1) {
    return $this->getFieldLang('url',$lang);
  }
  function getActive($lang = 1) {
    return $this->getFieldLang('active',$lang);
  }
  function getName($lang = 1) {
    return $this->getFieldLang('name',$lang);
  }
  function getHeader($lang = 1, $def = null) {
    return $this->getFieldLang('header',$lang, $def);
  }
  function getTitle($lang = 1, $def = null) {
    return $this->getFieldLang('title',$lang, $def);
  }
  function getFKeyw($lang = 1) {
    return $this->getFieldLang('fkeyw',$lang);
  }
  function getHKeyw($lang = 1) {
    return $this->getFieldLang('hkeyw',$lang);
  }
  function getKeyw($lang = 1) {
    return $this->getFieldLang('keyw',$lang);
  }
  function getDescrip($lang = 1) {
    return $this->getFieldLang('descrip',$lang);
  }
  function getContent($lang = 1) {
    return $this->getFieldLang('content',$lang);
  }

  function update($data) {
    $isset_lang = $this->getArrayIds('lang');
    foreach ($data['lang'] as $key => $value) {
      if (!in_array($value, $isset_lang)) {
        $isset_lang[] = $value;
      }
    }
    $data['lang'] = $isset_lang;    
    return parent::update($data);
  }
  function delete() {
    return parent::delete();
  }

}

abstract class PagewithlangCollection extends Collectionwithlang {

  function add($data) {
    $item = parent::add($data);
    return $item;
  }

  function getByParams(array $params = array(), $order = 'order') {
    $this->addJoin($this->Name()."haslang");
    if (isset($params['lang'])) {
      $lang = $params['lang'];
    } else {
      $lang = 1;
    }
    $this->addFilter("lang_id='$lang'");
    $fields = array('active');
    foreach ($fields as $f) {
      if (isset($params[$f])) {
        $this->addFilter($this->Name()."haslang_$f='{$params[$f]}'");
      }
    }    
    return parent::getByParams($params, $order);
  } 
}
?>