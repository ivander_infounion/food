<?php
  /* (c) 2015 InfoUnion CMS v3.0, voloshanivsky@gmail.com */
class ArticleTable extends PagewithlangTable {
  function __construct() {
    parent::__construct();
    $this->addFields(array(
      'created' => 'datetime',
      'pict' => 'varchar(4)',
      'link' => 'varchar(255)',
    ));
    $this->addManyToOne('section');
    $this->addManyToMany('lang');
  }
}

class Article extends Pagewithlang {

  static $pict_types = array("");
  function getCreated($date = null) {
    return $this->getDateTime('created', $date);
  }
  function getActual() {
    $time = strtotime($this->getCreated());
    if ($time <= time()) {
      return true;
    } else {
      return false;
    }
  }  
  // methods for pictures
  function getPict($pictname = null) {
    return $this->getField('pict');
  }
  function getDir() {
    return "content/".$this->Name()."/".$this->getId();
  }
  function getRealPictName($pictname = null) {
    return "picture$pictname-".$this->getId().'.'.$this->getPict($pictname);
  }
  function getRealPathPict($pictname = null) {
    return $this->getDir()."/".$this->getRealPictName($pictname);
  }
  function getRealPreviewName($pictname = null) {
    return "preview$pictname-".$this->getId().'.'.$this->getPict($pictname);
  }
  function getRealPathPreview($pictname = null) {
    return $this->getDir()."/".$this->getRealPreviewName($pictname);
  }
  function getPreviewSize($key = null, $pict = 'pict') {
    $sizes = array(
        'pict' => array('w'=>500, 'h'=>320),
      );
    $size = $sizes[$pict];
    if ($key) {
      return $size[$key];
    }
    return $size;
  }
  function setPict($value, $pictname = null) {
    return $this->setField('pict'.$pictname, $value);
  }
  function deletePictures() {
    $types = self::$pict_types;
    foreach ($types as $pictname) {
      if (file_exists($this->getRealPathPict($pictname))) {
        unlink($this->getRealPathPict($pictname));
      }
      if (file_exists($this->getRealPathPreview($pictname))) {
        unlink($this->getRealPathPreview($pictname));
      }
    }
  }
  // end pictures

  function getSectionId() {
    return $this->getField('section');
  }
  function getSection() {
    return new Section($this->getSectionId());
  }
  function getLink() {
    return $this->getField('link');
  }
  function getPreview($lang = 1) {
    return $this->getFieldLang('preview', $lang);
  }
  function getAuthor($lang = 1) {
    return $this->getFieldLang('author', $lang);
  }

  function getFiles($active = null) {
    $col = new ArticlefileCollection();
    $params = ['article' => $this->getId()];
    if ($active) {
      $params['active'] = 1;
    }
    return $col->getByParams($params, "order");
  }

  function delete() {
    $files = $this->getFiles();
    foreach ($files as $f) {
      $f->delete();
    }
    $this->deletePictures();
    return parent::delete();
  }

}

class ArticleCollection extends PagewithlangCollection {

  function add($data) {
    $item = parent::add($data);
    if (!file_exists($item->getDir())) {
      mkdir($item->getDir(),0755);
    }
    return $item;
  }

  function getByParams(array $params = array(), $order = 'created DESC') {
    if (isset($params['actual'])) {
      $datetime = date("Y-m-d H:i:s");
      $this->addFilter("article_created <= '$datetime'");
    }    
    return parent::getByParams($params, $order);
  }

}
?>