<?php
  /* (c) 2015 InfoUnion CMS v3.0, voloshanivsky@gmail.com */
class MenuTable extends EntityTable {
  function __construct() {
    parent::__construct();
    $this->addFields(array(
      'ctrl' => 'varchar(30)',
      'action' => 'varchar(30)',
      'params' => 'varchar(255)',
      'icon' => 'varchar(100)',
    ));
    $this->addField(new Field('active', 'int', false, 0));
    $this->addField(new Field('order', 'int', false, 999));
    $this->addField(new Field('parent', 'int', false, 0));
    $this->addField(new Field('depth', 'int', false, 0));
    $this->addField(new Field('children', 'int', false, 0));
    $this->addManyToMany("lang");
  }
}

class Menu extends Entitywithlang {

  function getName($lang = 1) {
    $result = $this->getFieldLang('name', $lang);
    if ($result == '') {
      return $this->getFieldLang('name', 1);
    }
    return $result;
  }
  function getCtrl() {
    return $this->getField('ctrl');
  }
  function getDepth() {
    return $this->getField('depth');
  }
  function getParentId() {
    return $this->getField('parent');
  }
  function getParent() {
    if ($this->getParentId()==0) {
      return false;
    }
    return new Menu($this->getParentId());
  }
  function getAction() {
    return $this->getField('action');
  }
  function getParams() {
    return $this->getField('params');
  }
  function getLink() {
    if ($this->getCtrl() == '') {
      return '#';
    }
    $link = Settings::get('baseurl') . $this->getCtrl().'/';
    if ($this->getAction() != '') {
      $link .= $this->getAction().'/';
    }
    if ($this->getParams() != '') {
      $link .= '?'.$this->getParams();
    }
    return $link;
  }
  function getBreadcrumbs($lang = null) {
    if (!$lang) {
      $lang = Settings::get('deflang');
    }
    if ($this->getDepth() >= 2) {
      $par = $this->getParent()->getParent();
      $breadcrumbs[$this->getDepth()-2]['title'] = $par->getName($lang);
      $breadcrumbs[$this->getDepth()-2]['url'] = $par->getLink();
    }
    if ($this->getDepth() >= 1) {
      $par = $this->getParent();
      $breadcrumbs[$this->getDepth()-1]['title'] = $par->getName($lang);
      $breadcrumbs[$this->getDepth()-1]['url'] = $par->getLink();
    }
    $breadcrumbs[$this->getDepth()]['title'] = $this->getName($lang);
    $breadcrumbs[$this->getDepth()]['url'] = $this->getLink();
    return $breadcrumbs;
  }
  function getIcon() {
    return $this->getField('icon');
  }
  function getHtmlIcon() {
    if ($this->getIcon() == '') {
      return '';
    }
    return '<i class="fa fa-'.$this->getIcon().'"></i>';
  }
  function getOrder() {
    return $this->getField('order');
  }
  function getActive() {
    return $this->getField('active');
  }

  function getCountChildren() {
    return $this->getField('children');
  }
  function getChildren($active = null) {
    $col = new MenuCollection();
    $params = array('parent'=>$this->getId());
    if ($active) {
      $params['active'] = 1;
    }
    $col = $col->getByParams($params);
    if (!$col->count()) {
      return false;
    }
    return $col;
  }

  function getData() {
    $data = parent::getData();
    if ($data['depth'] == 0) {
      $data['parent1'] = $data['id'];
      $data['parent2'] = $data['id'];
    } elseif ($data['depth'] == 1) {
      $data['parent1'] = $data['parent'];
      $data['parent2'] = $data['id'];
    } elseif ($data['depth'] == 2) {
      $data['parent1'] = $this->getParent()->getParent()->getId();
      $data['parent2'] = $data['parent'];
    }
    return $data;
  }

  function setActive($value) {
    return $this->setField('active', $value);
  }
  function setOrder($value) {
    return $this->setField('order', $value);
  }
  function setChildren($value) {
    return $this->setField('children', $value);
  }
  function setTreeValues($depth, $parent, $order) {
    $data = array('depth'=>$depth, 'parent'=>$parent, 'order'=>$order);
    return $this->update($data);
  }
}

class MenuCollection extends Collectionwithlang {

  function add($data) {
    if (!isset($data['order'])) {
      $db = DB::getInstance();
      $ord = $db->fetchSingle("SELECT MAX(menu_order) FROM menu WHERE 1 ORDER BY menu_order");
      if ($ord) {
        $data['order'] = $ord+1;
      }
    }
    return parent::add($data);
  }

  function getByParams(array $params = array(), $order = 'order') {
    return parent::getByParams($params, $order);
  }

  function getMenuForAdmin($admin) {
    $this->addJoin('access',' LEFT JOIN ',' ON(access_menu=menu_id)');
    $this->addJoin('adminhasaccess');
    $this->addFilter("admin_id=$admin");
    $this->addFilter('menu_active=1');
    $this->addGroupBy();
    return $this->getCustomIterator('','order');
  }
}
?>