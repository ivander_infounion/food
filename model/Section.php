<?php
  /* (c) 2015 InfoUnion CMS v3.0, voloshanivsky@gmail.com */
class SectionTable extends PagewithlangTable {
  function __construct() {
    parent::__construct();
    $this->addFields(array(

    ));
    $this->addField(new Field('mainmenu', 'tinyint(1)', false, 0));
    $this->addField(new Field('footmenu', 'tinyint(1)', false, 0));
    $this->addField(new Field('order', 'int', false, 0));
    $this->addField(new Field('parent', 'int', false, 0));
    $this->addField(new Field('depth', 'int', false, 0));
    $this->addField(new Field('children', 'int', false, 0));
    $this->addManyToOne('module');
    $this->addManyToMany('lang');
  }
}

class Section extends Pagewithlang {

  static function getByCtrl($ctrl) {
    $db = DB::getInstance();
    $ctrl = $db->real_escape_string($ctrl);
    $id = $db->fetchSingle("SELECT section_id FROM section NATURAL JOIN module WHERE module_controller='$ctrl'");
    if ($id) {
      return new Section($id);
    } else {
      return false;
    }
  }

  function getMainMenu() {
    return $this->getField('mainmenu');
  }
  function getFootMenu() {
    return $this->getField('footmenu');
  }
  function getDepth() {
    return $this->getField('depth');
  }
  function getParentId() {
    return $this->getField('parent');
  }
  function getParent() {
    if ($this->getParentId()==0) {
      return false;
    }
    return new $this->name($this->getParentId());
  }
  function getOrder() {
    return $this->getField('order');
  }
  function getCountChildren() {
    return $this->getField('children');
  }
  function getChildren($active = null, $lang = 1, $mainmenu = null) {
    if ($this->getCountChildren() == 0) {
      return false;
    }
    $col = new SectionCollection();
    $params = array('parent'=>$this->getId(), 'lang' => $lang);
    if ($active) {
      $params['active'] = 1;
    }
    if ($mainmenu) {
      $params['mainmenu'] = 1;
    }
    $col = $col->getByParams($params);
    if (!$col->count()) {
      return false;
    }
    return $col;
  }

  function getModuleId() {
    return $this->getField('module');
  }
  function getModule() {
    return new Module($this->getModuleId());
  }
  function getCtrl() {
    return $this->getModule()->getController();
  }
  function getIcon() {
    return $this->getModule()->getIcon();
  }
  function getHtmlIcon() {
    return $this->getModule()->getHtmlIcon();
  }
  function getLink() {
    $ctrl = $this->getCtrl();
    $link = Settings::get('baseurl') . $ctrl .'/';
    if (in_array($ctrl, array('textpage'))) {
      $link .= 'edit/'.$this->getId().'/';
    } elseif (in_array($ctrl, array('contact', 'mainpage'))) {
      $link = Settings::get('baseurl') .'textpage/';
      $link .= 'edit/'.$this->getId().'/';
    } else {
      $link .= 'list/?sid='.$this->getId();
    }
    return $link;
  }

  function getBreadcrumbs($lang = null) {
    if (!$lang) {
      $lang = Settings::get('deflang');
    }
    if ($this->getDepth() >= 2) {
      $par = $this->getParent()->getParent();
      $breadcrumbs[$this->getDepth()-2]['title'] = $par->getName($lang);
      $breadcrumbs[$this->getDepth()-2]['url'] = $par->getLink();
    }
    if ($this->getDepth() >= 1) {
      $par = $this->getParent();
      $breadcrumbs[$this->getDepth()-1]['title'] = $par->getName($lang);
      $breadcrumbs[$this->getDepth()-1]['url'] = $par->getLink();
    }
    $breadcrumbs[$this->getDepth()]['title'] = $this->getName($lang);
    $breadcrumbs[$this->getDepth()]['url'] = $this->getLink();
    return $breadcrumbs;
  }

  function getData() {
    $data = parent::getData();
    if ($data['depth'] == 0) {
      $data['parent1'] = $data['id'];
      $data['parent2'] = $data['id'];
    } elseif ($data['depth'] == 1) {
      $data['parent1'] = $data['parent'];
      $data['parent2'] = $data['id'];
    } elseif ($data['depth'] == 2) {
      $data['parent1'] = $this->getParent()->getParent()->getId();
      $data['parent2'] = $data['parent'];
    }
    return $data;
  }

  function setOrder($value) {
    return $this->setField('order', $value);
  }
  function setChildren($value) {
    return $this->setField('children', $value);
  }
  function setTreeValues($depth, $parent, $order) {
    $data = array('depth'=>$depth, 'parent'=>$parent, 'order'=>$order);
    return $this->setFields($data);
  }
  function setPict($value) {
    return $this->setField('pict', $value);
  }

  function delete() {
    return parent::delete();
  }
  function update($data) {
    return parent::update($data);
  }

}

class SectionCollection extends PagewithlangCollection {

  function add($data) {
    if (!isset($data['order'])) {
      $db = DB::getInstance();
      $ord = $db->fetchSingle("SELECT MAX(section_order) FROM section WHERE 1 ORDER BY section_order");
      if ($ord) {
        $data['order'] = $ord+1;
      }
    }
    $item = parent::add($data);
    return $item;
  }

  function getByParams(array $params = array(), $order = 'order') {
    return parent::getByParams($params, $order);
  }

}
?>