<?php
  /* (c) 2015 InfoUnion CMS v3.0, voloshanivsky@gmail.com */
class LangnameTable extends EntityTable {
  function __construct() {
    parent::__construct();
    $this->addField(new Field('name', 'varchar(255)', false, ''));
    $this->addField(new Field('shortname', 'varchar(255)', false, ''));
    $this->addField(new Field('translate_id', 'int', false, 0));
    $this->addManyToOne('lang');
  }
}

class Langname extends Entity {

  function getTranslateId() {
    return $this->getField('translate_id');
  }
  function getName() {
    return $this->getField('name');
  }
  /**
   * Сокращенное название для вывода переключалки на паблике
   * в основном используют сокращенное название
   * @return string
   */
  function getShortname() {
    return $this->getField('shortname');
  }

  static function getNameByParams($item_lang, $lang_needed) {
    $db = DB::getInstance();
    $q = "SELECT langname_id FROM langname WHERE lang_id = $item_lang and langname_translate_id = $lang_needed";
    $id = $db->fetchSingle($q);
    if ($id) {
      $langname = new Langname($id); 
      return $langname->getName();
    } else {
      return "";
    }
  }

  function setName($name) {
    return $this->setField('name', $name);
  }
  function setShortname($value) {
    return $this->setField('shortname', $value);
  }
}

class LangnameCollection extends Collection {

}
?>