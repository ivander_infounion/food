<?php
/* (c) 2013-2017 voloshanivsky@gmail.com */
/**
* Для получения перевода статичных фраз
*
* @param $strategy = default|key
* что выводить, когда нет значения или оно пустое
* default - выводить значение основного языка
* key - выводить ключ значения
*/
class Translatelist {
  
  private $values;
  private $values_default;
  private $keys;
  private $strategy;
  
  function __construct($lang = 1, $group = null, $strategy = "default") {
    // $col = new TranslateCollection();
    // $this->keys = $col->getFetchValues("translate_name");
    $col = new TranslateCollection();
    $this->values = $col->getTranslate($lang, $group);
    if ($strategy == 'default') {
      $col = new TranslateCollection();
      $this->values_default = $col->getTranslate(1, $group);
    }
    $this->strategy = $strategy;
  }

  public function val($key) {
    $result = false;
    if (isset($this->values[$key])) {
      if ($this->values[$key] != "") {
        $result = $this->values[$key];
      }
    }
    if (!$result) {      
      if ($this->strategy == "default") {
        if (isset($this->values_default[$key])) {
          if ($this->values_default[$key] != "") {
            return $this->values_default[$key];
          }
        }
        return $key;
      } elseif ($this->strategy == "key") {
        return $key;
      } else {
        return "err";
      }
    } else {
      return $result;
    }
  }

  public function getList() {
    return $this->values;
  }
}
?>