<?php
/* (c) 2014 Voloshanivsky, voloshanivsky@gmail.com */
class adminHasaccessTable extends AssociativeTable {
  function __construct() {
    parent::__construct();
    $this->addFields(array(
    ));
    $this->addField(new Field('level', 'int', false, 0));
  }
}

class adminHasaccess extends Entity {
  function getLevel() {
    return $this->getField('level');
  }
  function getAccessId() {
    return $this->getField('access');
  }
  function setLevel($value) {
    return $this->setField('level', $value);
  }
  function delete() {
    return parent::delete();
  }
}

class adminHasaccessCollection extends Collection {

  function getByParams($params, $order = 'id') {
    if (isset($params['admin'])) {
      $this->addFilter("admin_id=".$params['admin']);
    }
    return $this->getCustomIterator('',$order);
  }

}
?>