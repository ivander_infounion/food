<?php
  /* (c) 2015 InfoUnion CMS v3.0, voloshanivsky@gmail.com */
class MainpagelangTable extends EntityTable {
  function __construct() {
    parent::__construct();
    $this->addFields(array(
      'about' => 'text',
    ));
    $this->addManyToOne('lang');
  }
}

class Mainpagelang extends Entity {

  function getAbout() {
    return $this->getField('about');
  }

}

class MainpagelangCollection extends Collection {

  function getLangPages() {
    return $this->getCustomIterator('','lang_id');
  }

}
?>