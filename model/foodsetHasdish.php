<?php

class foodsetHasdishTable extends AssociativeTable {
    function __construct() {
    parent::__construct();
    $this->addFields(array(
    ));
}
}


class foodsetHasdish extends Entity {

    function getFoodset(){
        return new Foodset($this->getFoodsetId());
    }
    function getFoodsetId(){
        return $this->getField('foodset');
    }
    function getFoodsetName(){
        return $this->getFoodset()->getName();
    }

    function getDish(){
        return new Dish($this->getDishId());
    }
    function getDishId(){
        return $this->getField('dish');
    }
    function getDishName(){
        return $this->getDish()->getName();
    }
}


class foodsetHasdishCollection extends Collection {


}
?>