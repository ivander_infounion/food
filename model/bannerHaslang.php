<?php
  /* (c) 2015 InfoUnion CMS v3.0, voloshanivsky@gmail.com */
class bannerHaslangTable extends recordHaslangTable {
  function __construct() {
    parent::__construct();
    $this->addFields(array(
    	'content'=>"varchar(255)",
    	'namebtn'=>"text",
    ));
  }
}

class bannerHaslang extends recordHaslang {
	function getContent(){
		return $this->getField('content');
	}
	function getNameBtn(){
		return $this->getField('namebtn');
	}
}

class bannerHaslangCollection extends recordHaslangCollection {


}
?>