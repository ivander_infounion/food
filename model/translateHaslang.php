<?php
  /* (c) 2015 InfoUnion CMS v3.0, voloshanivsky@gmail.com */
class translateHaslangTable extends AssociativeTable {
  function __construct() {
    parent::__construct();
    $this->addFields(array(
    	'value' => 'varchar(255)'
    ));
  }
}

class translateHaslang extends Entity {
  function getNameEntityParent() {
    return substr($this->Name(),0,-7);
  }
  function getParent() {
    $ent = ucfirst($this->getNameEntityParent());
    return new $ent($this->getField($this->getNameEntityParent()));
  }
  function getLangId() {
    return $this->getField('lang');
  }
	function getValue(){
		return $this->getField('value');
	}
 
  function update($data) {
    $this->setFields($data);
    return true;
  }  
}

class translateHaslangCollection extends Collection {

  function add($data) {
    $fields = $this->table->getNameFields();
    foreach ($fields as $field) {
      if ((!isset($data[ $field . $data['lang'] ])) && (!in_array($field,array('id')))) {
        $this->addInsertDefault(strtolower($this->Name()).'_'.$field, '');
      }
    }
    $item = parent::add($data);
    return $item;
  }

}
?>