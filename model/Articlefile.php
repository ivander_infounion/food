<?php
  /* (c) 2015 InfoUnion CMS v3.0, voloshanivsky@gmail.com */
class ArticlefileTable extends RecordfileTable {
  function __construct () {
    parent::__construct();
    $this->addFields(array(

    ));
    $this->addManyToOne('article');
  }
}
class Articlefile extends Recordfile {

  function getArticleId() {
    return $this->getField('article');
  }
  function getDir() {
    return parent::getDir(substr($this->Name(),0,-4), $this->getArticleId());
  }
  function getRealPath() {
    return parent::getRealPath(substr($this->Name(),0,-4), $this->getArticleId());
  }
  function getRealPathPict() {
    return $this->getRealPath();
  }
  function getRealPathPreview() {
    return parent::getRealPathPreview(substr($this->Name(),0,-4), $this->getArticleId());
  }
  function withPreview() {
    return true;
  }
  function getPreviewSize($key = null) {
    $size = array('w'=>261, 'h'=>182);
    if ($key) {
      return $size[$key];
    }
    return $size;
  }

}

class ArticlefileCollection extends RecordfileCollection {

  function add($data) {
    $item = parent::add($data);
    if (!file_exists($item->getDir())) {
      mkdir($item->getDir(),0755);
    }
    return $item;
  }
  function getByParams(array $params = array(), $order = 'order') {
    return parent::getByParams($params, $order);
  }  
}

?>