<?php
  /* (c) 2015 InfoUnion CMS v3.0, voloshanivsky@gmail.com */
class settHaslangTable extends AssociativeTable {
  function __construct() {
    parent::__construct();
    $this->addFields(array(
        'address' => 'varchar(255)',
        'title' => 'varchar(255)',
        'hkeyw' => 'varchar(255)',
        'fkeyw' => 'varchar(255)',
        'keyw' => 'varchar(255)',
        'descrip' => 'varchar(255)',
        'copyright' => 'varchar(255)',
    ));
  }
}

class settHaslang extends Entity {

  function getLangId() {
    return $this->getField('lang');
  }
  function getFooter() {
    return $this->getField('footer');
  }
  function getTitle() {
    return $this->getField('title');
  }
  function getHkeyw() {
    return $this->getField('fkeyw');
  }
  function getFkeyw() {
    return $this->getField('fkeyw');
  }
  function getKeyw() {
    return $this->getField('keyw');
  }
  function getDescrip() {
    return $this->getField('descrip');
  }
  function getCopyright() {
    return $this->getField('copyright');
  }

  function update($data) {
    $this->setFields($data);
    return true;
  }

}

class settHaslangCollection extends Collection {

  function add($data) {
    $fields = $this->table->getNameFields();
    foreach ($fields as $field) {
      if ((!isset($data[ $field . $data['lang'] ])) && ($field != 'id')) {
        $this->addInsertDefault('setthaslang_'.$field, '');
      }
    }
    return parent::add($data);
  }

}
?>