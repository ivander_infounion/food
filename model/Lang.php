<?php
  /* (c) 2015 InfoUnion CMS v3.0, voloshanivsky@gmail.com */
class LangTable extends EntityTable {
  function __construct() {
    parent::__construct();
    $this->addFields(array(
        'sname' => 'varchar(3)',
    ));
    $this->addField(new Field('active', 'tinyint', false, 0));
    $this->addField(new Field('order', 'int', false, 1));

    $this->addManyToMany('menu',false);
    $this->addManyToMany('translate',false);
    $this->addManyToMany('module',false);
    $this->addManyToMany('section',false);
    $this->addManyToMany('sett',false);

    $this->addManyToMany('banner',false);
    $this->addManyToMany('article',false);
    $this->addManyToMany('articlefile',false);

  }
}

class Lang extends Entity {

  public $names;
  function __construct($id) {
    parent::__construct($id);
    $col = new LangnameCollection();
    $list = $col->getByParams(array('lang' => $id));
    foreach ($list as $value) {
      $this->names[ $value->getTranslateId() ] = $value;
    }
  }
  static function count_lang() {
    $col = new LangCollection();
    return $col->getCustomIterator('')->count();
  }
  static function getLangByName($name) {
    return Entity::getSingle('sname', $name, 'lang');
  }
  /**
   * @desc find entity Lang by 'id' or 'sname'
   * @param  int|string $value можно передавать как id так и ключ
   * @return Lang
   */
  static function getLang($value) {
    if (!$value) {
      return 1;
    } else {
      $l = Lang::getLangByName($value);
      if ($l) {
        return $l;
      } else {
        $l = new Lang($value);
        try {
          $name = $l->getSname();
        } catch (Exception $e) {
          throw new NotFoundException("Not found entity Lang by '$value'");
        }
        return $l;
      }
    }
  }
  static function getRealName($value) {
    $lang = self::getLang($value);
    return $lang->getSname();
  }
  static function getRealId($value) {
    $lang = self::getLang($value);
    return $lang->getId();
  }

  function getName($lang = 1) {
    $lang = $this->getRealId($lang);
    if (isset($this->names[$lang])) {
      return $this->names[$lang]->getName();
    } else {
      return $this->names[1]->getName();      
    }
  }
  function getShortname($lang = 1) {
    $lang = $this->getRealId($lang);
    if (isset($this->names[$lang])) {
      return $this->names[$lang]->getShortname();
    } else {
      return $this->names[1]->getShortname();;
    }
  }
  function getSname() {
    return $this->getField('sname');
  }
  function getUrl() {
    if ($this->getId() != 1) {
      return Settings::get('d') . $this->getSname().'/';
    } else {
      return Settings::get('d');
    }
  }
  function getActive() {
    return $this->getField('active');
  }
  function getFlag() {
    $d = Settings::get('d');
    return '<img src="'.$d.'admin/img/flags/'.$this->getSname().'.png">';
  }

  function setActive($act) {
    return $this->setField('active', $act ? 1 : 0);
  }
  function setOrder($value) {
    return $this->setField('order', $value);
  }
  function update($data) {
    $lang_names = new LangnameCollection();
    $data_add = ['lang' => $this->getId()];
    $data_update = [];
    foreach ($data['lang'] as $value) {
      if (isset($data['name'.$value])) {
        $name = $data['name'.$value];
      } else {
        $name = "";
      }
      if (isset($data['shortname'.$value])) {
        $shortname = $data['shortname'.$value];
      } else {
        $shortname = "";
      }
      if (isset($this->names[$value])) {
        $data_update['name'] = $name;
        $data_update['shortname'] = $shortname;
        $this->names[$value]->update($data_update);
      } else {
        $data_add['translate_id'] = $value;
        $data_add['name'] = $name;
        $data_add['shortname'] = $shortname;
        $lang_names->add($data_add);
      }
    }
    $this->setFields($data);
    return true;
  }

}

class LangCollection extends Collection {
  function add($data) {
    if (!isset($data['order'])) {
      $db = DB::getInstance();
      $ord = $db->fetchSingle("SELECT MAX(lang_order) FROM lang WHERE 1 ORDER BY lang_order");
      if ($ord) {
        $data['order'] = $ord+1;
      }
    }
    $item = parent::add($data);
    $colname = new LangnameCollection();
    $data_add = array('lang' => $item->getId());
    foreach ($data['lang'] as $value) {
      if (isset($data['name'.$value])) {
        $data_add['name'] = $data['name'.$value];
      } else {
        $data_add['name'] = "";
      }
      if (isset($data['shortname'.$value])) {
        $data_add['shortname'] = $data['shortname'.$value];
      } else {
        $data_add['shortname'] = "";
      }
      $data_add['translate_id'] = $value;
      $colname->add($data_add);
    }
    return $item;
  }  
  function getLangs() {
    return $this->getCustomIterator('', "order");
  }
  function getPublicLangs() {
    return $this->getCustomIterator('lang_active=1', "order");
  }
}
?>