<?php
  /* (c) 2015 InfoUnion CMS v3.0, voloshanivsky@gmail.com */
class BannerTable extends RecordfileTable {
  function __construct () {
    parent::__construct();
    $this->addFields(array(
      'link' => 'varchar(255)',
    ));
    $this->addManyToMany('url', false);
  }
}
class Banner extends Recordfile {
  
  function getContent($l = 1) {
    return trim($this->getFieldLang('content',$l));
  }
  function getNamebtn($l = 1) {
    return $this->getFieldLang('namebtn',$l);
  }
  function getLink() {
    return $this->getField('link');
  }
  function getPict() {
    return $this->getExt();
  }
  function getRealPathPict() {
    return $this->getRealPath();
  }
  function getUrlId() {
    return $this->getField('url');
  }
  function getDir() {
    return parent::getDir($this->Name());
  }
  function getRealPath() {
    return parent::getRealPath($this->Name());
  }
  function getRealPathPreview() {
    if (!$this->withPreview()) {
      return $this->getRealPath();
    }
    return parent::getRealPathPreview($this->Name());
  }
  function toggleActive($l = 1) {
    if($this->getActive($l)){
      $v = 0;
    }else{
      $v = 1;
    }
    return $this->setActive($v,$l);
  }

  function withPreview() {
    return false;
  }
  function getPreviewSize($key = null) {
    $size = array('w'=>950, 'h'=>330);
    if ($key) {
      return $size[$key];
    }
    return $size;
  }

}

class BannerCollection extends RecordfileCollection {

  function add($data) {
    $item = parent::add($data);
    if (!file_exists($item->getDir())) {
      mkdir($item->getDir(),0755);
    }
    return $item;
  }
  // function getByUrl($url, $active, $order = 'order') {
  //   $this->addJoin('urlhasbanner');
  //   $this->addFilter("url_id=".$url);
  //   if ($active) {
  //     $this->addFilter("bannerhaslang_active=1");
  //   }
  //   return $this->getCustomIterator('', $order);
  // }
  function getByParams($params = array(), $order = 'order') {
    if (isset($params['url'])) {
      $this->addJoin('urlhasbanner');
      $this->addFilter('url_id='.$params['url']);
    }
    return parent::getByParams($params, $order);
  }
}

?>