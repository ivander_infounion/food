<?php
/* (c) 2013-2017 voloshanivsky@gmail.com */
class TranslateTable extends EntityTable {
  function __construct() {
    parent::__construct();
    $this->addFields(array(
        'name' => 'varchar(30)',
    ));
    $this->addField(new Field('group', 'varchar(4)', false, ''));
    $this->addManyToMany('lang');
  }
}

class Translate extends Recordwithlang {
  static function getByName($name) {
    $db = DB::getInstance();
    $name = $db->real_escape_string($name);
    $id = $db->fetchSingle("SELECT translate_id FROM translate WHERE translate_name='$name'");
    if ($id) {
      return new Translate($id);
    } else {
      return false;
    }
  }
  static function getValueByName($name, $lang) {
    $db = DB::getInstance();
    $name = $db->real_escape_string($name);
    $lang = Lang::getRealId($lang);
    return $db->fetchSingle("SELECT translatehaslang_value FROM translate NATURAL JOIN translatehaslang WHERE translate_name='$name' AND lang_id = ".$lang);
  }

  function getGroup() {
    return $this->getField('group');
  }
  function getName() {
    return $this->getField('name');
  }
  function getValue($lang) {
    return $this->getFieldLang('value',$lang);
  }

  function setName($name) {
    return $this->setFieldLang('name', $name);
  }
  function setValue($value, $lang) {
    return $this->setFieldLang('name',$lang, $value);
  }
  function update($data) {
    return parent::update($data);
  }

}

class TranslateCollection extends RecordwithlangCollection {

  function add($data) {
    $tr = Translate::getByName($data['name']);
    if ($tr) {
      throw new Exception("Translation of this key already exists!");
    }
    return parent::add($data);
  }

  function getRecords($order = 'name') {
    return $this->getCustomIterator('', $order);
  }

  function getTranslate($lang, $group = null) {
    $lang = Lang::getRealId($lang);
    $this->addJoin("translatehaslang");
    $this->addFilter("lang_id = $lang");
    if ($group) {
      $this->addFilter("(translate_group='$group' OR translate_group='')");
    }
    // $iteration = new RowIterator("SELECT translate_name AS name, translate_$lang AS value
    //                               FROM translate
    //                               WHERE $where ORDER BY name COLLATE utf8_unicode_ci", new RowFetchKeyValue());
    
    // return $iteration->toArray();    
    $list = parent::getFetchAssoc("translate_name AS name, translatehaslang_value AS value", "", array(), "translate_name COLLATE utf8_unicode_ci");
    $result = array();
    foreach ($list as $key => $value) {
      $result[ $value['name'] ] = $value['value'];
    }
    return $result;
  }

  function getByText($text, $order = 'translate_name COLLATE utf8_unicode_ci') {
    if ($text != '') {
      $this->addFilter("(translate_name LIKE '%$text%' OR translatehaslang_value LIKE '%$text%')");
    }
    $this->addGroupBy();
    return parent::getByParams(array(), $order);
  }

}


?>