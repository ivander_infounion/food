<?php
  /* (c) 2015 InfoUnion CMS v3.0, voloshanivsky@gmail.com */
abstract class pageHaslangTable extends AssociativeTable {
  function __construct() {
    parent::__construct();
    $this->addFields(array(
        'url' => 'varchar(255)',
        'name' => 'varchar(255)',
        'header' => 'varchar(255)',
        'title' => 'varchar(255)',
        'hkeyw' => 'varchar(255)',
        'fkeyw' => 'varchar(255)',
        'keyw' => 'varchar(255)',
        'descrip' => 'varchar(255)',
        'content' => 'text',
    ));
    $this->addField(new Field('active', 'tinyint(1)', false, 0));
  }
}

abstract class pageHaslang extends Entity {

  function getUrlId() {
    return $this->getUrlEntity()->getId();
  }
  function getUrl() {
    return $this->getField('url');
  }
  function getUrlEntity() {
    return Url::getByNameLang($this->getUrl(), $this->getLangId());
  }

  function getNameEntityParent() {
    return substr($this->Name(),0,-7);
  }
  function getParent() {
    $ent = ucfirst($this->getNameEntityParent());
    return new $ent($this->getField($this->getNameEntityParent()));
  }
  function getLangId() {
    return $this->getField('lang');
  }
  function getActive() {
    return $this->getField('active');
  }
  function getName() {
    return $this->getField('name');
  }
  function getHeader() {
    return $this->getField('header');
  }
  function getFooter() {
    return $this->getField('footer');
  }
  function getTitle() {
    return $this->getField('title');
  }
  function getHkeyw() {
    return $this->getField('hkeyw');
  }
  function getFkeyw() {
    return $this->getField('fkeyw');
  }
  function getKeyw() {
    return $this->getField('keyw');
  }
  function getDescrip() {
    return $this->getField('descrip');
  }
  function getContent() {
    return $this->getField('content');
  }

  function setName($name) {
    return $this->setField('name', $name);
  }
  function setActive($act) {
      $this->getUrlEntity()->setActive($act);
    return $this->setField('active', $act ? 1 : 0);
  }

  function update($data) {
    if (isset($data['url'])) {
      $data['url'] = Settings::translit($data['url']);
      if ($data['url'] != $this->getUrl()) {
        $url = $this->getUrlEntity();
        $url->setName($data['url']);
      }
    }
    $this->setFields($data);
    return true;
  }

  function delete() {
    $url = $this->getUrlEntity();
    $url->delete();
    return parent::delete();
  }

}

abstract class pageHaslangCollection extends Collection {

  function add(array $data = array()) {
    $fields = $this->table->getNameFields();
    foreach ($fields as $field) {
      if ((!isset($data[ $field . $data['lang'] ])) && (!in_array($field,array('id','active')))) {
        $this->addInsertDefault(strtolower($this->Name()).'_'.$field, '');
      }
    }
    $data['url'] = Settings::translit($data['url']);
    $item = parent::add($data);
    $col = new UrlCollection();
    $url = $col->add(array('name'=>$item->getUrl(), 'ctrl'=>substr($this->Name(),0,-7), 'itemid'=>$item->getId(), 'lang'=>$item->getLangId()));
    return $item;
  }

  function deleteByQuery($query, $ids) {
//    foreach ($ids as $table => $id) {
//      $ent .= $table.'Has';
//    }
//    $ent = substr($ent,0,-3);
//    $ent = substr($this->Name(), 0, -10);
    $item = Entity::getByIds($this->Name(), $ids);
    if ($item) {
      $url = $item->getUrlEntity();
      $url->delete();
    }
    return parent::deleteByQuery($query);
  }

}
?>