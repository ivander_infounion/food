<?php
  /* (c) 2015 Voloshanivsky, voloshanivsky@gmail.com */
class AccessTable extends EntityTable {
  function __construct() {
    parent::__construct();
    $this->addFields(array(
      'ctrl' => 'varchar(30)',
    ));
    $this->addField(new Field('menu', 'int', false, 0));
    $this->addField(new Field('order', 'int', false, 999));
    $this->addManyToMany('admin', false);
  }
}

class Access extends Entity {

  static $levels = array(
    0 => 'close',
    1 => 'view',
    2 => 'edit',
    3 => 'full_access',
  );
  function getCtrl() {
    return $this->getField('ctrl');
  }
  function getMenuId() {
    return $this->getField('menu');
  }
  function getMenu() {
    if ($this->getMenuId() == 0) {
      return false;
    }
    return new Menu($this->getMenuId());
  }
  function getMenuName() {
    if ($this->getMenuId() == 0) {
      return '';
    }
    return $this->getMenu()->getName();
  }
  function getOrder() {
    return $this->getField('order');
  }

  function setOrder($value) {
    return $this->setField('order', $value);
  }

}

class AccessCollection extends Collection {
  function getAccessList() {
    $this->addJoin('menu',' LEFT JOIN ',' ON (access_menu=menu_id)');
    $this->addGroupBy('access_id');
    return $this->getCustomIterator('','menu_order, access_ctrl');
  }
  function getList() {
    return $this->getCustomIterator('');
  }
}
?>