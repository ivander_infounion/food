<?php
  /* (c) 2015 InfoUnion CMS v3.0, voloshanivsky@gmail.com */
class UrlTable extends EntityTable {
  function __construct() {
    parent::__construct();
    $this->addFields(array(
        'name' => 'varchar(255)',
        'ctrl' => 'varchar(30)',
    ));
    $this->addField(new Field('itemid', 'int', false, 0));
    $this->addField(new Field('lang', 'tinyint(1)', false, 0));
    $this->addField(new Field('active', 'tinyint(1)', false, 0));

    $this->addIndex('name');
    $this->addIndex(array('ctrl','itemid','lang'));
    $this->addManyToMany('banner');
  }
}

class Url extends Entity {
  static function getByName($value) {
    return Entity::getSingle('name', $value, 'url');
  }
  static function getByNameLang($name, $lang) {
    //все таки лучше сделать что для одной страницы может быть одинаковый урл для разных языков
    //тем более добавляем с контроллером не "Haslang", без lang будет неоднозначно
    $db = DB::getInstance();
    $name = $db->real_escape_string($name);
    $lang = $db->real_escape_string($lang);
    $entity = 'url';
    $id = $db->fetchSingle("SELECT {$entity}_id FROM $entity WHERE {$entity}_name='$name' AND {$entity}_lang=$lang");
    if ($id) {
      $entity = ucfirst($entity);
      return new $entity($id);
    } else {
      return false;
    }
  }
  function getArrayIds($entity = 'banner') {
    $colEnt = ucfirst($entity) . 'Collection';
    $col = new $colEnt();
    return $col->getArrayIds(array($this->Name() => $this->getId()));
  }
  function getPageNoException() {
    $Ent = $this->getCtrl().'Haslang';
    $item = new $Ent($this->getItemId());
    try {
      $item->getName();
    } catch ( Exception $e ) {
      return false;
    }
    return $item;
  }
  function getPage() {
//    $Ent = ucfirst($this->getCtrl());
    $Ent = $this->getCtrl().'Haslang';
    $item = new $Ent($this->getItemId());
    try {
      $item->getName();
    } catch ( Exception $e ) {
      throw new NotFoundException('URL: not found page '.$Ent.'('.$this->getItemId().')');
    }
    return $item;
  }
  function getPageName() {
    $page = $this->getPage();
    if ($page) {
      return $page->getName();
    }
    return '';
  }
  function getLang() {
    return $this->getField('lang');
  }
  function getName() {
    return $this->getField('name');
  }
  function getActive() {
    return $this->getField('active');
  }
  function getCtrl() {
    return $this->getField('ctrl');
  }
  function getCtrlName($lang = 1) {
    $ctrl = $this->getCtrl();
    if ($ctrl == 'section') {
      return ($lang == 1) ? 'Site section' : 'Раздел сайта';
    } else {
      return '';
    }
  }
  function getItemId() {
    return $this->getField('itemid');
  }

  function setName($name) {
    $check = Url::getByNameLang($name, $this->getLang());
    if ($check) {
      if ($check->getId() != $this->getId()) {
        throw new NotFoundException("Page already exists with this URL.");
      }
    }
    $result = $this->setField('name', $name);
    return $result;
  }
  function setUrl($name) { // путаю иногда name и url
    return $this->setName($name);
  }
  function setCtrl($value) {
    return $this->setField('ctrl', $value);
  }
  function setItemId($value) {
    return $this->setField('itemid', $value);
  }
  function setActive($act) {
    return $this->setField('active', $act ? 1 : 0);
  }
  function update($data) {//
    $check = Url::getByNameLang($data['name'], $this->getLang());
    if ($check) {
      if ($check->getId() != $this->getId()) {
        throw new NotFoundException("Page already exists with this URL.");
      }
    }
    return $this->setFields($data);
  }

}

class UrlCollection extends Collection {

  function add($data) {
    $check = Url::getByNameLang($data['name'], $data['lang']);
    if ($check) {
      throw new NotFoundException("Page already exists with this URL.");
    }
    $Item = parent::add($data);
    return $Item;
  }

  function getPagesForReview() {
    $this->addJoin("sectionhaslang", " LEFT JOIN ", " ON(url_ctrl='section' AND url_itemid=sectionhaslang_id)");
    $this->addJoin("section", " LEFT JOIN ", " ON(sectionhaslang.section_id=section.section_id)");
    // $this->addFilter("(url_ctrl='' OR (url_ctrl='section' AND module_id=6))");
    return $this->getCustomIterator("","ctrl, url_itemid");
  }
}
?>