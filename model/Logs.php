<?php
  /* (c) 2009-2010 Sanya Vol., shv@ukr.net */
class LogsTable extends EntityTable {
  function __construct() {
    parent::__construct();
    $this->addFields(array(
        'created' => 'datetime',
        'operation' => 'varchar(10)',
        'table' => 'varchar(30)',
        'ip' => 'varchar(20)',
        'comment' => 'text',
    ));
    $this->addField(new Field('itemid', 'int', false, 0));
    $this->addManyToOne('admin');
  }
}

class Logs extends Entity {

  function getCreated($date = null) {
    return $this->getField('created');
  }
  function getIp() {
    return $this->getField('ip');
  }
  function getOperation() {
    return $this->getField('operation');
  }
  function getTable() {
    return $this->getField('table');
  }
  function getItemId() {
    return $this->getField('itemid');
  }
  function getAdminId() {
    return $this->getField('admin');
  }
  function getAdmin() {
    return new Admin($this->getAdminId());
  }
  function getAdminLogin() {
    return $this->getAdmin()->getLogin();
  }
  function getComment() {
    return $this->getField('comment');
  }

}

class LogsCollection extends Collection {
  private static $instance = NULL;
  /**
  * @desc Get instance of class
  * @return LogsCollection
  */
  final static function getInstance() {
    if (self::$instance == NULL) {
      self::$instance = new self;
    }
    return self::$instance;
  }
  function add($data) {
    $data['created'] = date("Y-m-d H:i:s");
    $data['ip'] = $_SERVER['REMOTE_ADDR'];
    return parent::add($data);
  }
  function addlog($table, $itemid, $oper, $admin_id = null, $comm = '') {
    if (!$admin_id) {
      $admin = Admin::getAuthenticatedInstance();
      if ($admin) {
        $admin_id = $admin->getId();
      } else {
        return false;
      }
    }

    $data['admin'] = $admin_id;
    $data['table'] = $table;
    $data['itemid'] = $itemid;
    $data['operation'] = $oper;
    $data['comment'] = $comm;
    return $this->add($data);
  }
  function getLogs($admin = null, $operation = null, $table = null, $order = 'logs_created DESC')  {
    if ($admin) {
      if (is_int($admin)) {
        $this->addFilter("admin_id=$admin");
      } else {
        $this->addJoin('admin');
        $this->addFilter("admin_login='$admin'");
      }
    }
    if ($operation) {
      $this->addFilter("logs_operation='$operation'");
    }
    if ($table) {
      $this->addFilter("logs_table='$table'");
    }
    return $this->getCustomIterator('', $order);
  }
}
?>