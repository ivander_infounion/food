<?php
  /* (c) 2009-2010 Sanya Vol., shv@ukr.net */
class SettTable extends EntityTable {
  function __construct() {
    parent::__construct();
    $this->addFields(array(
        'counters' => 'text',
        'countershead' => 'text',
        'countersmain' => 'text',
        'phone' => 'varchar(255)',
        'email' => 'varchar(255)',
        'facebook' => 'varchar(255)',
        'twitter' => 'varchar(255)',
        'google' => 'varchar(255)',
        'youtube' => 'varchar(255)',
        'pinterest' => 'varchar(255)',
        'yelp' => 'varchar(255)',
        'vk' => 'varchar(255)',
        'linkedin' => 'varchar(255)',
    ));
    $this->addManyToMany('lang');
  }
}

class Sett extends Entitywithlang {

  function getFacebook() {
    return $this->getField('facebook');
  }
  function getTwitter() {
    return $this->getField('twitter');
  }
  function getGoogle() {
    return $this->getField('google');
  }
  function getYoutube() {
    return $this->getField('youtube');
  }
  function getPinterest() {
    return $this->getField('pinterest');
  }
  function getYelp() {
    return $this->getField('yelp');
  }
  function getVK() {
    return $this->getField('vk');
  }
  function getLinkedIn() {
    return $this->getField('linkedin');
  }
  function getEmail() {
    return $this->getField('email');
  }
  function getPhone() {
    return $this->getField('phone');
  }
  function getCounters($lang = 1) {
    return $this->getField('counters');
  }
  function getCountershead($lang = 1) {
    return $this->getField('countershead');
  }
  function getCountersmain($lang = 1) {
    return $this->getField('countersmain');
  }

  function getAddress($lang = 1) {
    return $this->getFieldLang('address',$lang);
  }
  function getTitle($lang = 1) {
    return $this->getFieldLang('title',$lang);
  }
  function getFKeyw($lang = 1) {
    return $this->getFieldLang('fkeyw',$lang);
  }
  function getHKeyw($lang = 1) {
    return $this->getFieldLang('hkeyw',$lang);
  }
  function getKeyw($lang = 1) {
    return $this->getFieldLang('keyw',$lang);
  }
  function getDescrip($lang = 1) {
    return $this->getFieldLang('descrip',$lang);
  }
  function getCopyright($lang = 1) {
    return $this->getFieldLang('copyright',$lang);
  }

}

class SettCollection extends Collectionwithlang {

}
?>