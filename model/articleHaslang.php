<?php
  /* (c) 2015 InfoUnion CMS v3.0, voloshanivsky@gmail.com */
class articleHaslangTable extends pageHaslangTable {
  function __construct() {
    parent::__construct();
    $this->addFields(array(
      'preview' => 'text'
    ));
    $this->addField(new Field('author', 'varchar(255)', false, ''));
  }
}

class articleHaslang extends pageHaslang {

  function getPreview() {
    return $this->getField('preview');
  }
  function getAuthor() {
    return $this->getField('author');
  }
}

class articleHaslangCollection extends pageHaslangCollection {


}
?>