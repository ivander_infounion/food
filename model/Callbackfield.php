<?php
  /* (c) 2010 Sanya Vol., shv@ukr.net */
class CallbackfieldTable extends EntityTable {
  function __construct() {
    parent::__construct();
    $this->addFields(array(
        'name' => 'varchar(255)',
        'type' => 'varchar(255)',
        'fieldname' => 'text',
        'data' => 'text'
    )); 
    $this->addManyToOne('callback');
  }
}

class Callbackfield extends Entity {
 
 static public function getFieldMeta($callback_id,$field_name){
   $id =  db::getInstance()->fetchSingle("SELECT callbackfield_id FROM callbackfield WHERE callback_id = ".$callback_id." AND callbackfield_name = '".$field_name."'");
   return new Callbackfield($id);
 } 
 function getName(){
  return $this->getField('name');
 }
 function getType(){
  return $this->getField('type');
 }
 function getData(){
  return $this->getField('data');
 }
 function getCallbackId(){
  return $this->getField('callback');
 }
 function getCallback(){
  return new Callback($this->getField('callback'));
 }
 function setData($data){
  return $this->setField('data',$data);
 }
}

class CallbackfieldCollection extends Collection {
  
  function add($data) {
    return parent::add($data);
  }

  function getByParams($params, $order = 'id') { 
    $fields = array('status','formtype',);
    foreach ($fields as $f) {
      if (isset($params[$f])) {
        $this->addFilter("callbackfield_$f='{$params[$f]}'");
      }
    }
    if (isset($params['callback'])) {
        $this->addFilter("callback_id='".$params['callback']."'");
    }
    return $this->getCustomIterator('', $order);
  }
}
?>
