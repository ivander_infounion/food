<?php
class DishTable extends EntityTable {
  function __construct() {
    parent::__construct();
    $this->addField(new Field('name', 'varchar(255)', false, ''));
    $this->addField(new Field('pict', 'varchar(4)', false, ''));
    $this->addField(new Field('weight', 'varchar(255)', false, ''));
    $this->addField(new Field('desc', 'text', false, ''));
    $this->addField(new Field('price', 'decimal(5,2)', false, 0));
    
    $this->addManyToMany('foodset',false);
    $this->addManyToOne('category');
  }
}

class Dish extends Entity {

  //custom fields
  function getName(){
    return $this->getField('name');
  }
  function getWeight(){
    return $this->getField('weight');
  }
  function getDesc(){
    return $this->getField('desc');
  }
  function getPrice(){
    return $this->getField('price');
  }

  //db_links
  function getCategory(){
    return new Category($this->getCategoryId());
  }
  function getCategoryId(){
    return $this->getField('category');
  }
  function getCategoryName(){
    return $this->getCategory()->getName();
  }


 
}

class DishCollection extends Collection {

  
}
?>