<?php
  /* (c) 2009-2010 Sanya Vol., shv@ukr.net */
class LogsmailTable extends EntityTable {
  function __construct() {
    parent::__construct();
    $this->addFields(array(
        'sended' => 'datetime',
        'from' => 'varchar(255)',
        'to' => 'varchar(255)',
        'subject' => 'varchar(255)',
        'content' => 'text',
        'status' => 'varchar(255)',
        'status_code' => 'int',
    ));
    $this->addField(new Field('type','varchar(255)',false,''));
    $this->addField(new Field('lang_id','varchar(255)',false,''));
  }
}

class Logsmail extends Entity {

  function getSended($date = null) {
    return $this->getField('sended');
  }
  function getDelivered($date = null) {
    return $this->getField('delivered');
  }
  function getFrom() {
    return $this->getField('from');
  }
  function getTo() {
    return $this->getField('to');
  }
  function getSubject() {
    return $this->getField('subject');
  }
  function getContent() {
    return $this->getField('content');
  }
  function getStatusCode() {
    return $this->getField('status_code');
  }
  function getStatus() {
    return $this->getField('status');
  }
  function getType() {
    return $this->getField('type');
  }
  function getLangid() {
    return $this->getField('lang');
  }
}

class LogsmailCollection extends Collection {
  
  
  public static function addlog($data){
    $c = new LogsmailCollection();
    return $c->add($data);
  }

  function getByParams($params = array(), $order = 'sended DESC')  {
    $fields = array('from','to','subject','status_code','type');
    foreach ($fields as $f) {
      if (isset($params[$f])) {
        $this->addFilter("logsmail_$f='{$params[$f]}'");
      }
    }
    $fields = array('sended_from','sended_to');
    foreach ($fields as $f) {
      if (isset($params['sended_from'])) {
        $this->addFilter("logsmail_sended >= '{$params['sended_from']}'");
      }
      if (isset($params['sended_to'])) {
        $this->addFilter("logsmail_sended <= '{$params['sended_to']}'");
      }
    }
    return $this->getCustomIterator('', $order);
  }
}
?>