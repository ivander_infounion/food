<?php
/* (c) 2014 voloshanivsky@gmail.com */

class AdminTable extends EntityTable {
  function __construct () {
    parent::__construct();
    $this->addFields(array(
      'name' => 'varchar(255)',
      'login' => 'varchar(255)',
      'pass' => 'varchar(200)',
      'key' => 'varchar(255)',
      'email' => 'varchar(255)',
    ));
    $this->addField(new Field('active', 'tinyint(1)', false, 1));
    $this->addField(new Field('wrongpass', 'tinyint(1)', false, 0));

    //settings (visual and other)
    $this->addField(new Field('del', 'tinyint(1)', false, 0));
    $this->addField(new Field('lang', 'tinyint(1)', false, 1));
    $this->addField(new Field('skin', 'varchar(16)', false, ''));
    $this->addField(new Field('leftmenu', 'tinyint(1)', false, 0));
    $this->addField(new Field('pict', 'varchar(4)', false, ''));

    $this->addManyToMany('access');

    $this->addIndex('login');
    $this->addIndex('key');
  }
}
class Admin extends Entity {
  const COOKIE_ID = 'aid';
  const COOKIE_KEY = 'akey';
  const COOKIE_R = 'ar';
  static $active = array(0 => 'close',
                         1 => 'open',
                         2 => 'banned',
                         );
  static $actives = array(0 => array('ru'=>'Закрыто','ua'=>'Закрито','en'=>'Close'),
                         1 => array('ru'=>'Открыто','ua'=>'Відкрито','en'=>'Open'),
                         2 => array('ru'=>'Заблокировано','ua'=>'Заблоковано','en'=>'Banned'),
                         );
  static $skins = array('' => array('ru'=>'Тема по умолчанию','ua'=>'Тема за замовчуванням','en'=>'Default theme'),
                         'skin-1' => array('ru'=>'Тема 1','ua'=>'Тема 1','en'=>'Theme 1'),
                         'skin-2' => array('ru'=>'Тема 2','ua'=>'Тема 2','en'=>'Theme 2'),
                         'skin-3' => array('ru'=>'Тема 3','ua'=>'Тема 3','en'=>'Theme 3'),
                         );
  static protected $instance;

  static function getAuthenticatedInstance() {
    if ( !isset(self::$instance)) {
      self::$instance = self::getByKey();
    }
    return self::$instance;
  }
  /**
  * Return Admin object
  *
  * @param string Login
  * @return Admin
  */
  static function getByLogin($login) {
    return Entity::getSingle('login', $login, 'Admin');
  }
  static function getByEmail($login) {
    return Entity::getSingle('email', $login, 'Admin');
  }
  static protected function getByKey() {
    if (isset($_COOKIE[self::COOKIE_KEY])) {
      $key = $_COOKIE[self::COOKIE_KEY];
    } else {
      $key = '';
    }
    if (isset($_COOKIE[self::COOKIE_ID])) {
      $id = $_COOKIE[self::COOKIE_ID];
    } else {
      $id = '';
    }

    try{
      $admin = new Admin($id);
      if ($admin->getField('key') == $key) {
        return $admin;
      }
    } catch (EntityException $e){}
    return NULL;
  }

  function getArrayIds($entity) {
    $colEnt = ucfirst($entity) . 'Collection';
    $col = new $colEnt();
    return $col->getArrayIds(array($this->Name() => $this->getId()));
  }
  function getDel() {
    return $this->getField('del');
  }
  function getLangId() {
    return $this->getField('lang');
  }
  function getLang() {
    return new Lang($this->getLangId());
  }
  function getSkin() {
    return $this->getField('skin');
  }
  function getLeftMenu() {
    return $this->getField('leftmenu');
  }
  function getActive() {
    return $this->getField('active');
  }
  function getActiveName($lang = null) {
    $act = self::$active;
    $key = $act[$this->getField('active')];
    $lang = Lang::getRealName($lang);
    return Translate::getValueByName($key, $lang);
  }
  function getName() {
    return $this->getField('name');
  }
  function getWrongpass() {
    return $this->getField('wrongpass');
  }
  function getLogin() {
    return $this->getField('login');
  }
  function getKey() {
    return $this->getField('key');
  }
  function getPass() {
     return $this->getField('pass');
  }
  function getEmail() {
    return $this->getField('email');
  }
  function getPict($pictname = null) {
    return $this->getField('pict');
  }
  function getDir() {
    return "content/avatar";
  }
  function getRealPictName($pictname = null) {
    return "avatar-".$this->getId().'.'.$this->getPict($pictname);
  }
  function getRealPathPict($pictname = null) {
    return $this->getDir()."/".$this->getRealPictName($pictname);
  }

  //access, menu
  function getAccess() {
    $col = new adminHasaccessCollection();
    $col = $col->getByParams(array('admin'=>$this->getId()));
    $res = array();
    foreach ($col as $item) {
      $res[$item->getAccessId()] = $item->getLevel();
    }
    $col = new AccessCollection();
    $col = $col->getList();
    foreach ($col as $item) {
      if (!isset($res[$item->getId()])) {
        $res[$item->getId()] = 0;
      }
    }
    return $res;
  }
  function getMenu() {
    $col = new MenuCollection();
    if ($this->getLogin() == 'root') {
      return $col->getByParams(array(),'menu_order');
    } else {
      return $col->getByParams(array('active'=>1),'menu_order');
    }
    return $col->getMenuForAdmin($this->getId());
  }

  function resetKey() {
    $this->setField('key',md5($this->id.mt_rand()));
  }

  function setLeftMenu($value) {
    return $this->setField('leftmenu', $value);
  }
  function setSkin($value) {
    return $this->setField('skin', $value);
  }
  function setDel($value) {
    return $this->setField('del', $value);
  }
  function setPass($pass) {
     return $this->setField('pass', $pass);
  }
  function setPict($value) {
    return $this->setField('pict', $value);
  }
  function setActive($act) {
    return $this->setField('active', $act);
  }
  function setWrongpass($value) {
    return $this->setField('wrongpass', $value);
  }
  function incWrongpass() {
    $wp = $this->getWrongpass() + 1;
    if ($wp >= 5) {
      $this->setActive(2);
    }
    return $this->setWrongpass($wp);
  }

  function setCookie($remember = null) {
    if ($remember) {
      setcookie(Admin::COOKIE_ID, $this->getId(), time()+60*60*24*365, Settings::get('cookieurl'));
      setcookie(Admin::COOKIE_KEY, $this->getKey(), time()+60*60*24*365, Settings::get('cookieurl'));
      setcookie(Admin::COOKIE_R, 1, time()+60*60*24*365, Settings::get('cookieurl'));
    } else {
      setcookie(Admin::COOKIE_ID, $this->getId(), time()+3600, Settings::get('cookieurl'));
      setcookie(Admin::COOKIE_KEY, $this->getKey(), time()+3600, Settings::get('cookieurl'));
      setcookie(Admin::COOKIE_R, '', time() - 365*24*3600, Settings::get('cookieurl'));
    }
  }

  static function unsetCookie() {
    $past = time() - 365*24*3600;
    setcookie(Admin::COOKIE_ID, '', $past, Settings::get('cookieurl'));//('cookieurl'));
    setcookie(Admin::COOKIE_KEY, '', $past, Settings::get('cookieurl'));
    setcookie(Admin::COOKIE_R, '', $past, Settings::get('cookieurl'));
  }

  function delete() {
    if (file_exists($this->getRealPathPict())) {
      unlink($this->getRealPathPict());
    }
    return parent::delete();
  }

  function update(array $data) {
    return $this->setFields($data);
  }
}

class AdminCollection extends Collection {
  function add($params) {
    $params['key'] = md5(time() . $params[0]);
    return parent::add($params);
  }
  function getAdmins($del = null, $order = 'name') {
    if ($del !== null) {
      $this->addFilter("admin_del=$del");
    }
    $this->addFilter("admin_id!=1");
    return $this->getCustomIterator("", $order);
  }
  function getAllAdmins($del = null, $order = 'name') {
    if ($del !== null) {
      $this->addFilter("admin_del=$del");
    }
    return $this->getCustomIterator("", $order);
  }
}

?>