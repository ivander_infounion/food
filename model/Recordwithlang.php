<?php
  /* (c) 2015 InfoUnion CMS v3.0, voloshanivsky@gmail.com */
abstract class RecordwithlangTable extends EntityTable {
  function __construct() {
    parent::__construct();
    $this->addFields(array(

    ));
    $this->addField(new Field('order', 'int', false, 9999));
    $this->addManyToMany('lang');
  }
}

abstract class Recordwithlang extends Entitywithlang {

  function getActive($lang = 1) {
    return $this->getFieldLang('active',$lang);
  }
  function getName($lang = 1) {
    return $this->getFieldLang('name',$lang);
  }
  function getOrder() {
    return $this->getField('order');
  }
  
  function setActive($v,$lang = 1) {
    return $this->setFieldLang('active',$v,$lang);
  }

  function update($data) {
    $isset_lang = $this->getArrayIds('lang');
    foreach ($data['lang'] as $key => $value) {
      if (!in_array($value, $isset_lang)) {
        $isset_lang[] = $value;
      }
    }
    $data['lang'] = $isset_lang;    
    return parent::update($data);
  }

  function delete() {
    return parent::delete();
  }

}

abstract class RecordwithlangCollection extends Collectionwithlang {

  function add($data) {
    $item = parent::add($data);
    return $item;
  }

  function getByParams(array $params = array(), $order = 'order') {
    $this->addJoin($this->Name()."haslang");
    if (isset($params['lang'])) {
      $lang = $params['lang'];
    } else {
      $lang = 1;
    }
    $this->addFilter("lang_id='$lang'");
    $fields = array('active');
    foreach ($fields as $f) {
      if (isset($params[$f])) {
        $this->addFilter($this->Name()."haslang_$f='{$params[$f]}'");
      }
    }     
    return parent::getByParams($params, $order);
  } 
}
?>