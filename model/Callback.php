<?php
  /* (c) 2010 Sanya Vol., shv@ukr.net */
class CallbackTable extends EntityTable {
  function __construct() {
    parent::__construct();
    $this->addFields(array(
        'created' => 'datetime',
        'success' => 'datetime',
        'status' => 'tinyint(1)',
        'formtype' => 'varchar(255)',
    )); 
    $this->addField(new Field('worker', 'int', false, 0));
    $this->addIndex('formtype');
  }
}

class Callback extends Entity {
  
  protected $metadata;
  
  function __construct($id) {
      parent::__construct($id);
      $cbf = new CallbackfieldCollection();
      $fields  = $cbf->getByParams(array('callback'=>$this->getId()))->toArray();
      foreach ($fields as $fkey => $f) {
        foreach ($f->data as $key => $value) {
          $key = str_replace('callbackfield_', '', $key);
          $arr[$key] = $value;
        }
        if ($arr['name']){
          $this->metadata[$arr['name']] = $arr;
        }
      }
  }

  function getMetaField($f){
    return $this->metadata[$f];
  }
  function getMetaFieldValue($f){
    return $this->metadata[$f]['data'];
  }
  function getMetaFieldsArray(){
    return $this->metadata;
  }
  function printMetaFields(){
    return print_r($this->metadata);
  }
  function getSuccess($date = null) {
    return $this->getDateTime('success', $date);
  }
  function getWorkerId() {
    return $this->getField('worker');
  }
  function getWorker() {
    if ($this->getWorkerId() == 0) {
      return false;
    }
    $worker = new Worker($this->getWorkerId());
    try {
      $name = $worker->getName();
      //return $worker;
    } catch ( Exception $e ) {
      return false;
    }
    return $worker;
  }
  function getWorkerName() {
    if (!$this->getWorker()) {
      return '';
    }
    return $this->getWorker()->getName();
  }
  function getCreated($date = null) {
    return $this->getDateTime('created', $date);
  }
  function getStatus(){
    if($this->getFieldMeta('status')==1){
      return true;
    }else{
      return false;
    }
  }
  function setStatus($v){
    return $this->setField('status',$v);
  }
  function getFormtype(){
    return $this->getFieldMeta('formtype');
  }

  function delete() {
    return parent::delete();
  }

  function update($data) {
    foreach ($data as $f) {
      foreach ($f as $key => $value) {
        $this->$f[$key] = $value;
      }
    }
    return parent::update($data);
  }

}

class CallbackCollection extends Collection {
  
  function add($data) {
    return parent::add($data);
  }
  function getByParams($params, $order = 'created') { 
    $fields = array('status','formtype');
    foreach ($fields as $f) {
      if (isset($params[$f])) {
        $this->addFilter("callback_$f='{$params[$f]}'");
      }
    }
    return $this->getCustomIterator('', $order);
  }
}
?>
