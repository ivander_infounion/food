<?php
class FoodsetTable extends EntityTable {
    function __construct() {
        parent::__construct();
        $this->addField(new Field('name', 'varchar(255)', false, ''));
        $this->addField(new Field('desc', 'text', false, ''));
        $this->addField(new Field('price', 'decimal(5,2)', false, 0));

        $this->addManyToMany('dish');

    }
}


class Foodset extends Entity
{

    //custom fields
    function getName()
    {
        return $this->getField('name');
    }

    function getDesc()
    {
        return $this->getField('desc');
    }

    function getPrice()
    {
        return $this->getField('price');
    }
}



class FoodsetCollection extends Collection {

}
?>