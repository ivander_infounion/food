<?php
class CategoryTable extends EntityTable {
  function __construct() {
    parent::__construct();
    $this->addField(new Field('name', 'varchar(255)', false, ''));
  }
}

class Category extends Entity {
  function getName(){
    return $this->getField('name');
  }
}

class CategoryCollection extends Collection {
  
}
?>