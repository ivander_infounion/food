<?php
  /* (c) 2015 InfoUnion CMS v3.0, voloshanivsky@gmail.com */
abstract class recordHaslangTable extends AssociativeTable {
  function __construct() {
    parent::__construct();
    $this->addFields(array(
        'name' => 'varchar(255)',
    ));
    $this->addField(new Field('active', 'tinyint(1)', false, 0));
  }
}

abstract class recordHaslang extends Entity {

  function getNameEntityParent() {
    return substr($this->Name(),0,-7);
  }
  function getParent() {
    $ent = ucfirst($this->getNameEntityParent());
    return new $ent($this->getField($this->getNameEntityParent()));
  }
  function getLangId() {
    return $this->getField('lang');
  }
  function getActive() {
    return $this->getField('active');
  }
  function getName() {
    return $this->getField('name');
  }
  
  function setName($name) {
    return $this->setField('name', $name);
  }
  function setActive($act) {
    return $this->setField('active', $act ? 1 : 0);
  }

  function update($data) {
    $this->setFields($data);
    return true;
  }

  function delete() {
    return parent::delete();
  }

}

abstract class recordHaslangCollection extends Collection {

  function add($data) {
    $fields = $this->table->getNameFields();
    foreach ($fields as $field) {
      if ((!isset($data[ $field . $data['lang'] ])) && (!in_array($field,array('id','active')))) {
        $this->addInsertDefault(strtolower($this->Name()).'_'.$field, '');
      }
    }
    $item = parent::add($data);
    return $item;
  }
}
?>