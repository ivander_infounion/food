<?php
  /* (c) 2015 InfoUnion CMS v3.0, voloshanivsky@gmail.com */
class moduleHaslangTable extends AssociativeTable {
  function __construct() {
    parent::__construct();
    $this->addFields(array(
    	'name' => 'varchar(255)'
    ));
  }
}

class moduleHaslang extends Entity {
  function getNameEntityParent() {
    return substr($this->Name(),0,-7);
  }
  function getParent() {
    $ent = ucfirst($this->getNameEntityParent());
    return new $ent($this->getField($this->getNameEntityParent()));
  }
  function getLangId() {
    return $this->getField('lang');
  }
	function getName(){
		return $this->getField('name');
	}
 
  function update($data) {
    $this->setFields($data);
    return true;
  }
}

class moduleHaslangCollection extends Collection {

  function add(array $data = array()) {
    $fields = $this->table->getNameFields();
    foreach ($fields as $field) {
      if ((!isset($data[ $field . $data['lang'] ])) && (!in_array($field,array('id')))) {
        $this->addInsertDefault(strtolower($this->Name()).'_'.$field, '');
      }
    }
    $item = parent::add($data);
    return $item;
  }

}
?>