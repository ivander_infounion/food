<?php
/* (c) 2010 Sanya Vol., shv@ukr.net */

class ModuleTable extends EntityTable {
  function __construct () {
    parent::__construct();
    $this->addFields(array(
       'controller' => 'varchar(50)',
       'url' => 'varchar(150)',
       'icon' => 'varchar(50)',
    ));
    $this->addField(new Field('order', 'int', false, 999));
    $this->addManyToMany("lang");

  }
}
class Module extends Entitywithlang {

  static function getIdByController($controller) {
    $db = DB::getInstance();
    $value = $db->real_escape_string($controller);
    $id = $db->fetchSingle("SELECT module_id FROM module WHERE module_controller='$value'");
    return $id;
  }
  function getName($lang = 1) {
    $result = $this->getFieldLang('name', $lang);
    if ($result == '') {
      return $this->getFieldLang('name', 1);
    }
    return $result;
  }
  function getController() {
    return $this->getField('controller');
  }
  function getUrl() {
    return $this->getField('url');
  }
  function getIcon() {
    $icon = $this->getField('icon');
    if ($icon == '') {
      return 'file';
    }
    return $icon;
  }
  function getHtmlIcon() {
    return '<i class="fa fa-'.$this->getIcon().'"></i>';
  }
  function getOrder() {
    return $this->getField('order');
  }

  function setUrl($value) {
    return $this->setField('url',$value);
  }
  function setOrder($value) {
    return $this->setField('order', $value);
  }
  function update($data) {
    return parent::update($data);
  }
}

class ModuleCollection extends Collectionwithlang {
  function getModules($order = 'order') {
    return $this->getCustomIterator('', $order);
  }
}

?>