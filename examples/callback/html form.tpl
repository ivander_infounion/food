<div class="landing-order" id="js-landing-order">
    <div class="container">
        <form onsubmit="return false" enctype="multipart/form-data" name="order_landing_divan" destination="sendcallback" id="order_landing_divan">
            <label class="landing-label landing-label_input-name">
                <input class="landing-input" type="text" placeholder="Ваше имя" name="name" fieldname="Имя" required>
            </label>
            <label class="landing-label">
                <input class="landing-input" type="text" placeholder="Ваш телефон" name="phone" fieldname="Телефон"  required>
            </label>
            <input class="landing-submit btn callback_submit" type="submit" value="Заказать диван">
        </form>
    </div>
</div>