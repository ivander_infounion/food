$(document).ready(function(){

//enable validating

	$( '#js-landing-order' ).find( 'form' ).validate({
	    rules: {
	        landing_name: {
	            required: true
	        },
	        landing_phone: {
	            required: true
	        }
	    },
	    messages: {
	        landing_name: {
	            required: 'Это обязательное поле'
	        },
	        landing_phone: {
	            required: 'Это обязательное поле'
	        }
	    },
	    highlight: function( element ) {
	        $( element ).addClass( 'has-error' );
	    },
	    unhighlight: function( element ) {
	        $( element ).removeClass( 'has-error' );
	    },
	    submitHandler: function(form) {
	        callback_submit(form) //what to do when form submited and passed validation
	    }
	});

});


function callback_submit(form_dom){
    let data = callback2array(form_dom);
    ajax_query('default','savecallback',{fields: data, formtype:form_dom.id}); 
}
function ajax_cb_default_savecallback(result) {
    show_modal_message(result['name'],result['title'],result['text'])
    $("#"+result['formtype']).trigger("reset");
}
    

function callback2array(theForm) {
  var type;
  let thisform = new Array;
  for(i=0; i<theForm.elements.length; i++){
    let field = {};
    type = theForm.elements[i].type;
    if(type == "text" || type == "password" || type == "hidden" || type == "select-one"){
      field.value = theForm.elements[i].value
      field.type  = 'input'
    } else if(type == "checkbox" || type == "radio"){
      if ( theForm.elements[i].checked ) {
        field.value = 1
      }else{
        field.value = 0;
      }
      field.type  = 'boolean'

    } else if(type == "textarea"){
      field.value = theForm.elements[i].value
      field.type  = 'textarea'
    }
    field.name = theForm.elements[i].name
    if(theForm.elements[i].hasAttribute('required')){
      field.required = true
    }else{
      field.required = false
    }
    field.fieldname =  theForm.elements[i].getAttribute("fieldname");
    thisform.push(field);
  }
  return thisform;
}

function show_modal_message(name,title,text){
  if(name){
    $('#modal_message_name').html(name);
  }
  if(title){
    $('#modal_message_title').html(title);
  }
  if(text){
    $('#modal_message_text').html(text);
  }
  $('#js-modal-message').modal('show');
}