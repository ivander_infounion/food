<?php
function smarty_function_getvalue($params, &$smarty)
{
	$view = View::getInstance();
  foreach ($params as $key => $value) {
    $view->assign($key, $value);
  }
  if ($params['item']) {
    $function_name = "get".ucfirst($params['name']);
    if (method_exists($params['item'], $function_name)) {
      $value = $params['item']->$function_name();
    } else {
      $value = false;
    }
  } else {
    $value = false;    
  }
  if ($params['truevalue']){
  	$var = $params['var'];
  	if(!isset($params['returnfalse'])){
  		$params['returnfalse'] = '';
  	}
  	if($value == $params['truevalue']){
  		$view->assign($var, $params['returntrue']);
  		return $params['returntrue'];
  	}else{
  		$view->assign($var, $params['returnfalse']);
  		return $params['returnfalse'];
  	}
  } else {
    if (isset($params['returnfalse']) && $value === false) {
      return $params['returnfalse'];
    } else {
      return $value;
    }
  }
	
}
?>