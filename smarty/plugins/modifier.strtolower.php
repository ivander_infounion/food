<?php
function smarty_modifier_strtolower($string, $code = 'utf-8')
{
  return mb_strtolower($string, $code);
}
?>
