<?php
function smarty_function_makebuttonlink($params, &$smarty)
{
  $controller = $params['c'];
  $action = $params['a'];
  $text = $params['text'];
  if (isset($params['title'])) {
    $alt = "title='".$params['title']."'";
  } else {
    $alt = '';
  }
  if (isset($params['icon'])) {
    $icon = "<i class='icon-".$params['icon']."'></i> ";
  } else {
    $icon = '';
  }  
  unset($params['c']);
  unset($params['a']);
  unset($params['title']);
  unset($params['icon']);
  unset($params['text']);
  $url = FrontController::MakeUrl($controller, $action, $params);
  if ( true ) {
    $url = str_replace('&', '&amp;', $url);
  }
  return <<<HTML
<a class="btn" href="$url" $alt>$icon$text</a>  
HTML;

}
?>