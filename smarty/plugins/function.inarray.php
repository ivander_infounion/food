<?php
function smarty_function_inarray($params, &$smarty)
{ 
  if (isset($params['id'])) {
    if (is_array($params['arr'])) {
      if (in_array($params['id'], $params['arr'])) {
        return '<option value='.$params['id'].' selected>'.$params['name'];
      } else {
        return '<option value='.$params['id'].'>'.$params['name'];
      }
    } else {
      return '<option value='.$params['id'].'>'.$params['name'];
    }
  } else {
    return 'error';
  }
}
?>
