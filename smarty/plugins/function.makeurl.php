<?php
function smarty_function_makeurl($params, &$smarty)
{
  $controller = $params['c'];
  $action = $params['a'];
  unset($params['c']);
  unset($params['a']);
  $url = FrontController::MakeUrl($controller, $action, $params);
  if ( true ) {
    $url = str_replace('&', '&amp;', $url);
  }
  return $url;
}
?>
