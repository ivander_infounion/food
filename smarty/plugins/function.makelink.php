<?php
function smarty_function_makelink($params, &$smarty)
{
  $controller = $params['c'];
  $action = $params['a'];
  $text = $params['text'];
//  $text = htmlspecialchars($params['text']);
  if (isset($params['alt'])) {
    $alt = "title='".$params['alt']."'";
  } else {
    $alt = '';
  }
  if (isset($params['tooltip'])) {
    $alt .= ' rel="tooltip" data-toggle="tooltip" data-placement="'.$params['tooltip'].'"';
  }
  if (isset($params['icon'])) {
    $icon = "<i class='fa fa-".$params['icon']."'></i> ";
  } else {
    $icon = '';
  }
  if (isset($params['class'])) {
    $class = 'class="'.$params['class'].'"';
  } else {
    $class = '';
  }
  unset($params['text']);
  unset($params['c']);
  unset($params['a']);
  unset($params['alt']);
  unset($params['tooltip']);
  unset($params['icon']);
  unset($params['class']);
  $url = FrontController::MakeUrl($controller, $action, $params);
  if ( true ) {
    $url = str_replace('&', '&amp;', $url);
  }
  return <<<HTML
<a href="$url" $alt $class>$icon$text</a>
HTML;

}
?>
