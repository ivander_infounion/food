<?php
function smarty_function_inarray_checker($params, &$smarty)
{ 
  if (isset($params['id'])) {
    if ((isset($params['arr']))&&(in_array($params['id'], $params['arr']))) {
      return 'checked="checked"';
    } else {
      return '';
    }
  } else {
    return 'error';
  }
}
?>
