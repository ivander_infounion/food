$(document).ready(function(){

});


/**
 * Simple search. Generates url on frontend and redirect to generated url. Form tag must contains attr 'inpname' with id of search field input
 * @param {Html form} form 
 */
function searchProd(form) {
  plang = parseInt($("#page_language").html());
  lang = "";
  if (plang != 1) {
    lang = $('#lang_sname').html()+'/';
  }
  let string_search = $("#"+$(form).attr('inpname')).val();
  //strip tags simple analog
  var replaced_string = string_search.replace(/(<([^>]+)>)/ig,"");
  location.href = defUrl()+lang+"search/"+replaced_string+"/";
}

function defUrl() {
  return '/';
}


/**
 * Sending query to server useing ajax with JsHttpRequest libs. 
 * @param {c string} controller name 
 * @param {a string} action name 
 * @param {data any_type} data to send, can be html dom or object 
 */
function ajax_query(c,a,data) {
  var query = new Array;

  query['c'] = c;
  query['a'] = a;
  query['d'] = data;

  name_ajax = '/ajax.php';

  JsHttpRequest.query(name_ajax,query,function(result, errors) {

      debug_layer = $('#debug');
      if (debug_layer && (errors || !result)) {

          debug_layer.show();
          debug_layer.html("Error <br/><pre>"+errors+"</pre><i class='fa fa-remove pull-right cursor-pointer' onclick=" +'$("#debug").hide()'+"></i>");

      }
      if (typeof window['ajax_cb_'+result['a']] == 'function') {
        eval('ajax_cb_'+result['a']+'(result["r"])');
      } else {
        eval('ajax_cb_'+result['c']+'_'+result['a']+'(result["r"])');
      }
    },
    true
  );
}

function form2array(theForm) {
  var type;
  var arr = new Array;
  for(i=0; i<theForm.elements.length; i++){
    type = theForm.elements[i].type;
    if(type == "text" || type == "password" || type == "hidden" || type == "textarea" /*|| type == "button"*/ || type == "select-one"){
      arr[theForm.elements[i].name] = theForm.elements[i].value
    } else if(type == "checkbox" || type == "radio"){
      if ( theForm.elements[i].checked ) {
        arr[theForm.elements[i].name] = theForm.elements[i].value;
      } else {
        arr[theForm.elements[i].name] = 0;
      }
    } else if(type == "file") {
      arr[theForm.elements[i].name] = theForm.elements[i].files;
    }
  }
  return arr;
}

function ajax_cb_sendreview(result) {
  if (result['error'] == true) {
    showPopupMessage(result['status']);
  } else {
    window.location.reload();
  }
}


function callback2array(theForm) {
  var type;
  let thisform = new Array;
  for(i=0; i<theForm.elements.length; i++){
    let field = {};
    type = theForm.elements[i].type;
    if(type == "text" || type == "password" || type == "hidden" || type == "select-one"){
      field.value = theForm.elements[i].value
      field.type  = 'input'
    } else if(type == "checkbox" || type == "radio"){
      if ( theForm.elements[i].checked ) {
        field.value = 1
      }else{
        field.value = 0;
      }
      field.type  = 'boolean'

    } else if(type == "textarea"){
      field.value = theForm.elements[i].value
      field.type  = 'textarea'
    }
    field.name = theForm.elements[i].name
    if(theForm.elements[i].hasAttribute('required')){
      field.required = true
    }else{
      field.required = false
    }
    field.fieldname =  theForm.elements[i].getAttribute("fieldname");
    thisform.push(field);
  }
  return thisform;
}
