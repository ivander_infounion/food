<?php
/* (c) 2015 InfoUnion CMS v3.0, voloshanivsky@gmail.com */
class SettController extends CmsController {

  function __construct($params) {
    $this->entityName = 'Sett';
    $params['ctrl'] = strtolower(substr(get_class($this), 0, -10));
    if (!parent::__construct($params)) {
      return;
    }

  }

  function EditAction() {
    $this->view->assign('editor', true);
    $this->params['id'] = 1;
    $this->view->assign('tablang', true);
    $item = new Sett(1);
    $this->view->assign('langItems', $item->langItems);
    $this->view->assign('robots_txt',file_get_contents("robots.txt"));
    $this->edit_action();
  }

}

class SettAjaxController extends CmsAjaxController {

  function __construct($params) {

    parent::__construct($params);
    $this->entityName = 'Sett';
    $this->colName = $this->entityName.'Collection';

  }

  function checkValidate($item = null) {

    $this->validateFields(array('email'), $item);
    return $this->result;
  }

  function UpdateAction() {
    $item = new $this->entityName($this->params['id']);
    $this->checkValidate($item);
    if ($this->result['error']) {
      return $this->result;
    }
    DB::getInstance()->begin();
    $item->update($this->params);
    LogsCollection::getInstance()->addlog(strtolower($this->entityName), $item->getId(), 'update');
    DB::getInstance()->commit();
    //robots.txt
    $filename = 'robots.txt';
    if (!$file = fopen($filename, 'a')) {
      $this->result['error'] = true;
      $this->result['alert'] = 'error';
      $this->result['status'] = 'Error opening file robots.txt';
      return $this->result;
    }
    ftruncate($file, 0);
    if (fwrite($file, stripslashes($this->params['robots'])) === false) {
      $this->result['error'] = true;
      $this->result['alert'] = 'error';
      $this->result['status'] = 'Error writing file robots.txt';
      return $this->result;
    }
    fclose($file);
    //
    $this->result['c'] = strtolower($this->entityName);
    $this->result['id'] = $item->getId();
    ActionController::addMsg('success', '', $this->conf['saved_ok']);
    return $this->result;

  }

}

?>