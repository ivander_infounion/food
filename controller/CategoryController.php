<?php
/* (c) 2015 InfoUnion CMS v3.0, voloshanivsky@gmail.com */
class CategoryController extends CmsController {

  protected $section;
  function __construct($params) {
    $this->entityName = 'Category';
    $params['ctrl'] = strtolower(substr(get_class($this), 0, -10));
    if (!parent::__construct($params)) {
      return;
    }
    $this->view->assign('menu_content', true);
  }

  function ListAction() {
    $entCol = $this->entityName.'Collection';
    $col = new $entCol();
    $col = $col->getByParams(array());
    $this->list_action($col);
  }

  function NewAction() {
    $this->new_action();
  }

  function EditAction() {
    $item = new $this->entityName($this->params['id']);
    $this->edit_action(null, $item->getName($this->lang));
  }

}

class CategoryAjaxController extends CmsAjaxController {

  function __construct($params) {

    parent::__construct($params);
    $this->entityName = 'Category';
    $this->colName = $this->entityName.'Collection';

  }

  function checkValidate($item = null) {
    $require = array('name');
    $this->validateFields($require, $item);
    return $this->result;
  }

  function AddAction() {
    $this->checkValidate();
    if ($this->result['error']) {
      return $this->result;
    }
    $col = new $this->colName();
    DB::getInstance()->begin();
    $item = $col->add($this->params);
    LogsCollection::getInstance()->addlog(strtolower($this->entityName), $item->getId(), 'add');
    DB::getInstance()->commit();

    $this->result['c'] = strtolower($this->entityName);
    $this->result['id'] = $item->getId();
    ActionController::addMsg('success', '', $this->conf['added_ok']);
    return $this->result;
  }

    function UpdateAction() {
    $item = new $this->entityName($this->params['id']);
    $this->checkValidate($item);
    if ($this->result['error']) {
      return $this->result;
    }
    DB::getInstance()->begin();
    $item->update($this->params);
    LogsCollection::getInstance()->addlog(strtolower($this->entityName), $item->getId(), 'update');
    DB::getInstance()->commit();
    $this->result['c'] = strtolower($this->entityName);
    $this->result['id'] = $item->getId();
    ActionController::addMsg('success', '', $this->conf['saved_ok']);
    return $this->result;
  }

  function DeleteAction() {
    $item = new $this->entityName($this->params['id']);
    //delete or set null relations
    $info = $item->getName(1);
    LogsCollection::getInstance()->addlog(strtolower($this->entityName), $item->getId(), 'delete', $this->admin->getId(), $info);
    return $this->delete();
  }



}

?>