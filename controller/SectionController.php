<?php
/* (c) 2015 InfoUnion CMS v3.0, voloshanivsky@gmail.com */
class SectionController extends CmsController {

  function __construct($params) {
    $this->entityName = 'Section';
    $params['ctrl'] = strtolower(substr(get_class($this), 0, -10));
    if (!parent::__construct($params)) {
      return;
    }

  }

  function ListAction() {
    $entCol = $this->entityName.'Collection';
    $col = new $entCol();
    $col = $col->getByParams(array('depth'=>0));
    $this->list_action($col, $fileTpl);
  }
  function SortingAction() {
      $this->view->assign('plugin_nestable', true);
      $fileTpl = strtolower($this->entityName)."/list-nestable.tpl";
      $this->view->action['name'] = 'sorting';
      $this->view->action['title'] = $this->conf[$this->view->action['name']];
      $this->view->breadcrumbs[2]['title'] = $this->conf['list'];
      $this->view->breadcrumbs[2]['url'] = FrontController::MakeUrl(strtolower($this->entityName),'list');
    $entCol = $this->entityName.'Collection';
    $col = new $entCol();
    $col = $col->getByParams(array('depth'=>0));
    $this->list_action($col, $fileTpl);
  }

  function NewAction() {
    $this->view->assign('editor', true);
    $this->view->assign('tablang', true);
    $this->view->assign('langItems', array(1=>true,2=>false,3=>false));
    $col = new ModuleCollection();
    $this->view->assign('modules', $col->getModules());
    $col = new BannerCollection();
    $this->view->assign('banners', $col->getByParams());
    $this->new_action();
  }

  function EditAction() {
    $this->view->assign('editor', true);
    $this->view->assign('tablang', true);
    $item = new $this->entityName($this->params['id']);
    $this->view->assign('langItems', $item->langItems);
    $col = new ModuleCollection();
    $this->view->assign('modules', $col->getModules());
    $col = new BannerCollection();
    $this->view->assign('banners', $col->getByParams());
    $isset_banners = array();
    foreach ($item->langItems as $langitem) {
      $url = $langitem->getUrlEntity();
      if ($url) {
        $isset_banners[$url->getLang()] = $url->getArrayIds('banner');
      }
    }
    $this->view->assign('isset_banners', $isset_banners);
    $this->edit_action(null, $item->getName($this->lang));
  }

}

class SectionAjaxController extends CmsAjaxController {

  function __construct($params) {

    parent::__construct($params);
    $this->entityName = 'Section';
    $this->colName = $this->entityName.'Collection';

  }

  function ActiveAction() {
    return $this->activeAll();
  }

  function checkValidate($item = null) {

    $require = array('module');
    foreach ($this->params['lang'] as $l) {
      $require[] = 'name'.$l;
      $require[] = 'url'.$l;
    }
    $this->validateFields($require, $item);
    (isset($this->params['mainmenu'])) ? $this->params['mainmenu'] = 1 : $this->params['mainmenu'] = 0;
    (isset($this->params['footmenu'])) ? $this->params['footmenu'] = 1 : $this->params['footmenu'] = 0;
    return $this->result;

  }

  function AddAction() {
    $this->checkValidate();
    if ($this->result['error']) {
      return $this->result;
    }

    $col = new $this->colName();
    DB::getInstance()->begin();
    $item = $col->add($this->params);
    foreach ($item->langItems as $langitem) {
      $this->updateBanners($langitem);
    }
    LogsCollection::getInstance()->addlog(strtolower($this->entityName), $item->getId(), 'add');
    DB::getInstance()->commit();

    $this->result['c'] = strtolower($this->entityName);
    $this->result['id'] = $item->getId();
    ActionController::addMsg('success', '', $this->conf['added_ok']);
    return $this->result;

  }

  function DeleteAction() {
    $item = new $this->entityName($this->params['id']);
    //delete or set null relations
    $info = $item->getName(1);
    LogsCollection::getInstance()->addlog(strtolower($this->entityName), $item->getId(), 'delete', $this->admin->getId(), $info);
    return $this->delete();
  }

  function UpdateAction() {
    $item = new $this->entityName($this->params['id']);
    $this->checkValidate($item);
    if ($this->result['error']) {
      return $this->result;
    }

    DB::getInstance()->begin();
    $item->update($this->params);
    foreach ($item->langItems as $langitem) {
      $this->updateBanners($langitem);
    }
    LogsCollection::getInstance()->addlog(strtolower($this->entityName), $item->getId(), 'update');
    DB::getInstance()->commit();
    $this->result['c'] = strtolower($this->entityName);
    $this->result['id'] = $item->getId();
    ActionController::addMsg('success', '', $this->conf['saved_ok']);
    return $this->result;

  }

  function recursive($array, $depth = 0, $parent = 0, $order = 1) {
    $count = 0;
    foreach ($array as $key => $value) {
      $item = new $this->entityName($value['id']);
      $item->setTreeValues($depth, $parent, $order);
      $order++;
      $count_ch = 0;
      if (isset($value['children'])) {
        $count_ch = $this->recursive($value['children'], $depth+1, $value['id'], $order);
      }
      $order += $count_ch;
      $count++;
      //set children
      $item->setChildren($count_ch);
    }
    return $count;
  }
  function ReordertreeAction() {
    $result = json_decode($this->params, true);
    $this->recursive($result);
    LogsCollection::getInstance()->addlog(strtolower($this->entityName), 0, 'reorder', $this->admin->getId(), $this->params);
    return true;
  }

}

?>