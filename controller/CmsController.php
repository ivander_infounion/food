<?php
/* (c) 2010-2014 Voloshanivsky, voloshanivsky@gmail.com
*  (c) 2015 InfoUnion CMS v.3.0
*/
class CmsController extends ActionController {

  protected $admin;
  protected $sett;
  protected $conf;
  protected $translate;
  public $entityName;
  public $lang;
  public $lang_content;
  public $entlang;
  public $first_lang;
  function __construct($params) {

    parent::__construct($params);

    $this->view->assign('lastmod', time());

    $dl = Settings::get('deflang');
    $this->view->ctrl['title'] = Translate::getValueByName('admin_header',$dl);
    if (!$this->getAdminAccess()) {
      return false;
    }
    $this->entlang = $this->admin->getLang();
    if (empty($this->entlang)) {
      $this->entlang = Lang::getLangByName($dl);
      $this->lang_content = 1;
    } else {
      if ($this->entlang->getActive() == 1) {
        $this->lang_content = $this->entlang->getId();
      } else {
        $this->lang_content = 1;
      }
    }
    $this->view->assign('lang_content', $this->lang_content);
    $this->lang = $this->entlang->getSname();
    $this->view->assign('entlang', $this->entlang);
    $this->view->assign('lang', $this->lang);

    $trans = new TranslateCollection();
    $this->conf = $trans->getTranslate($this->lang, 'cms');
    $this->view->assign('conf', $this->conf);
    // Новая версия получение перевода
    $this->translate = new Translatelist($this->lang, 'cms', 'default');
    $this->view->assign('translate', $this->translate);

    if (isset($params['ctrl'])) {
      $this->view->ctrl['name'] = $params['ctrl'];
    } else {
      $this->view->ctrl['name'] = 'cms';
      $this->view->ctrl['title'] = $this->conf['admin_header'];
    }

//    $main_lang = new Lang(1);
//    $this->main_lang = $main_lang->getSname();
//    $this->view->assign('tablang', $this->main_lang);

    $d = Settings::get('d');
    $this->view->assign('d', $d);
    $langs = new LangCollection();
    $langs = $langs->getLangs();
    $this->view->assign('langs', $langs);
    $this->view->assign('pub_langs', $langs);
    $this->view->assign('lcheck_pub_langs', $langs);
    // $this->view->assign('pub_langs', $langs->getPublicLangs());
    // $this->view->assign('lcheck_pub_langs', $langs->getPublicLangs());
    foreach ($langs as $l) {
      $flags[$l->getSname()] = $l->getFlag();
      $slang[$l->getId()] = $l->getSname();
    }
    $this->view->assign('flags', $flags);
    $this->view->assign('slang', $slang);
    /* langItems - список языков для new.tpl */
    $this->view->assign('langItems', array(1=>true));

    $this->first_lang = $slang[1];

    $this->view->assign('today', date("d.m.Y"));
    $this->view->assign('defdate', date("d.m.Y H:i"));
//    if ($this->sett->getBlocked() == 1 && $this->adminkind->getIdent() != 'root') {
//      $this->displayError('The system is temporarily unavailable! Please try again later.');
//      return;
//    }

    $this->view->current_menu = false;
    $colmenu = new MenuCollection();
    $colmenu = $colmenu->getByParams(array('ctrl'=>$this->view->ctrl['name']));
    if ($colmenu->count() == 1) {// меню контроллера не делится на пункты по действию и параметрам
      $colmenu = $colmenu->toArray();
      $menu = $colmenu[0];
      $this->view->ctrl['title'] = $menu->getName($this->lang);
      $this->view->current_menu = $menu->getData();

      $this->view->breadcrumbs = $menu->getBreadcrumbs($this->lang);
    }
    $colsect = new SectionCollection();
    $this->view->assign('topsections', $colsect->getByParams(array('depth'=>0)));

    // some options for form and other
    //ширина колонки описания поля в формах (часть из 12)
    $this->view->assign('col_label', 2);
    $this->view->assign('editor_kind', 'ckeditor');
    // $this->view->assign('editor_kind', 'summernote');
    $this->view->assign('logo_start', Settings::get('logo_start'));
    $this->view->assign('current_year', date("Y"));

    // STATUS
    if (isset($_SESSION['toastr'])) {
      $toastr = array();
      $i = 0;
      foreach ($_SESSION['toastr'] as $value) {
        $toastr[$i]['kind'] = $value['kind'];
        $toastr[$i]['msg'] = $value['msg'];
        $toastr[$i]['title'] = $value['title'];
        $i++;
      }
      $this->view->assign('toastr', $toastr);
      unset($_SESSION['toastr']);
    }
    return true;
  }

  function MainpageAction() {
    $this->view->display('admin/default.tpl');
  }
  // *** MENU ***
  function getMenuByAction($params = null) {
    if ($this->view->current_section) {
      $sect = new Section($this->view->current_section['id']);
      $brs = $sect->getBreadcrumbs($this->lang_content);
      foreach ($brs as $br) {
        $this->insertBreadcrumbs($br);
      }
    } elseif (!$this->view->current_menu) {
      $colmenu = new MenuCollection();
      $data = array('ctrl'=>$this->view->ctrl['name'], 'action'=>$this->view->action['name']);
      if ($params) {
        $data['params'] = $params;
      }
      $colmenu = $colmenu->getByParams($data)->toArray();
      if (count($colmenu) > 0) {
        $menu = $colmenu[0];
        $this->view->ctrl['title'] = $menu->getName($this->lang);
        $this->view->current_menu = $menu->getData();
//        $this->view->breadcrumbs = $menu->getBreadcrumbs($this->lang);
        $brs = $menu->getBreadcrumbs($this->lang);
        foreach ($brs as $br) {
          $this->insertBreadcrumbs($br);
        }
      }
    }
    if ($this->view->action['name'] != 'list') {
      $count_bread = count($this->view->breadcrumbs);
      $this->view->breadcrumbs[$count_bread]['url'] = '';
      $this->view->breadcrumbs[$count_bread]['title'] = $this->conf[$this->view->action['name']];
    }
  }
  function insertBreadcrumbs($bread_item, $place = null) {
    $count_bread = count($this->view->breadcrumbs);
    if ($place === null) {
      $place = $count_bread;
    } elseif ($place < 0) {
      $place = $count_bread + $place;
    }
    if ($place == 'replace') {
      $this->view->breadcrumbs[$count_bread-1] = $bread_item;
    } else {
      for ($i=$count_bread; $i>$place; $i--) {
        $this->view->breadcrumbs[$i] = $this->view->breadcrumbs[$i-1];
      }
      $this->view->breadcrumbs[$place] = $bread_item;
    }

  }
  function setCurrentMenu($ctrl, $action, $params = null) {
    $colmenu = new MenuCollection();
    $data = array('ctrl'=>$ctrl, 'action'=>$action);
    if ($params) {
      $data['params'] = $params;
    }
    $colmenu = $colmenu->getByParams($data)->toArray();
    if (count($colmenu) > 0) {
      $menu = $colmenu[0];
      $this->view->current_menu = $menu->getData();
      $brs = $menu->getBreadcrumbs($this->lang);
      foreach ($brs as $br) {
        $this->insertBreadcrumbs($br);
      }
    }
    return $this->view->current_menu;
  }

  function createPager($limit, $total) {
    $countpages = floor($total/$limit - 0.001) + 1;
    $this->view->assign('countitems', $total);
    $this->view->assign('countpages', $countpages);
    if (isset($this->params['p'])) {
      $page = $this->params['p'];
    } else {
      $page = 1;
    }
    $this->view->assign('currentpage', $page);
    return " LIMIT ".($page-1)*$limit.", $limit";
  }

  function list_action($list, $fileTpl = null) {

    if ($this->view->action['name'] == '') {
      $this->view->action['name'] = 'list';
      $this->view->action['title'] = $this->conf[$this->view->action['name']];
    }
    $this->getMenuByAction();
    $this->view->assign('list', $list);

    if ($fileTpl) {
      $this->view->assign('fileTpl', $fileTpl);
    } else {
      $this->view->assign('fileTpl', mb_strtolower($this->view->ctrl['name']).'/list.tpl');
    }
    $this->view->display('admin/default.tpl');
  }

  function new_action($fileTpl = null) {
    if ($this->view->action['name'] == '') {
      $this->view->action['name'] = 'new';
      $this->view->action['title'] = $this->conf['create'];
    }
    $this->getMenuByAction();
    if ($fileTpl) {
      $this->view->assign('fileTpl', $fileTpl);
    } else {
      $this->view->assign('fileTpl', mb_strtolower($this->view->ctrl['name']).'/new.tpl');
    }
    $this->view->display('admin/default.tpl');
  }

  function edit_action($fileTpl = null, $title = null) {
    $item = new $this->entityName($this->params['id']);
    $this->view->assign('item', $item);
    if ($this->view->action['name'] == '') {
      $this->view->action['name'] = 'edit';
      $this->view->action['title'] = $this->conf['edit'];
    }
    $this->getMenuByAction();
    if ($title) {
      $this->view->action['title'] = $title;
      $item_bread['url'] = '#';
      $item_bread['title'] = $title;
      $this->insertBreadcrumbs($item_bread, 'replace');
    }

    if ($fileTpl) {
      $this->view->assign('fileTpl', $fileTpl);
    } else {
      $this->view->assign('fileTpl', mb_strtolower($this->view->ctrl['name']).'/edit.tpl');
    }
    $this->view->display('admin/default.tpl');
  }

  function displayError($error = null, $text = null, $login = null) {
    if (!$error) {
      $error = $this->conf['page_not_found'];
    }
    $this->view->assign('error', $error);
    $this->view->assign('text', $text);
    if ($login) {
      $this->view->assign('login',1);
    }
    $this->view->assign('fileTpl','admin/error.tpl');
    $this->view->display('admin/default.tpl');
  }


}

class CmsAjaxController extends AjaxActionController {

  protected $admin;
  public $entityName;
  public $colName;
  public $langs;

  function __construct($params) {
    parent::__construct($params);
    $trans = new TranslateCollection();
    $this->lang = Settings::get('deflang');
    $this->conf = $trans->getTranslate($this->lang);
    $this->getAdminAccess();
    if ($this->admin) {
      $this->lang = $this->admin->getLang()->getSname();
      $trans = new TranslateCollection();
      $this->conf = $trans->getTranslate($this->lang);
    }
    $this->langs = new LangCollection();
    $this->langs = $this->langs->getLangs();
  }
  function ChangelmAction() {
    $lm = $this->admin->getLeftMenu();
    if ($lm == 1) {
      $this->admin->setLeftMenu(0);
    } else {
      $this->admin->setLeftMenu(1);
    }
    return true;
  }
  // active
  function active() {
    $item = new $this->entityName($this->params);
    if($item->getActive() == 1) {
      $item->setActive(0);
      if($this->entityName == 'Admin') {
        $item->resetKey();
      }
    } else {
      $item->setActive(1);
    }
    LogsCollection::getInstance()->addlog(strtolower($this->entityName), $item->getId(), 'active-'.$item->getActive());
    return array('id'=>$item->getId(), 'active'=>$item->getActive(), 'publish'=>$this->conf['publish'], 'stop_public'=>$this->conf['stop_public']);
  }
  function activeAll() {
    $item = new $this->entityName($this->params);
    $act = $item->getActive(1);
    ($act == 1) ? $newact = 0 : $newact = 1;
    foreach ($item->langItems as $il) {
      $il->setActive($newact);
    }
    LogsCollection::getInstance()->addlog(strtolower($this->entityName), $item->getId(), 'active-'.$newact);
    return array('id'=>$item->getId(), 'active'=>$item->getActive(), 'publish'=>$this->conf['publish'], 'stop_public'=>$this->conf['stop_public']);
  }
// Filter
  function FilterreloadAction() {
    $ctrl = $this->params['ctrl'];
    $_SESSION['filter_'.$ctrl] = array();
    unset($this->params['ctrl']);
    foreach ($this->params as $key => $value) {
      if (!empty($value)) {
        $_SESSION['filter_'.$ctrl][ $key ] = $value;
      } else {
        unset($_SESSION['filter_'.$ctrl][ $key ]);
      }
    }
    return true;
  }   
  // *********** PICT *************************
  function getFileInfo($name) {
    $fileName = $_FILES[ $name ]['name'];
    $pos = strrpos($fileName,'.');
    $data['ext'] = substr($fileName,$pos+1);
    $data['filename'] = substr($fileName,0,$pos);
    return $data;
  }
  //old update without langs
//   function updateGallery($item = null, $add_data = array()) {
//     if (!$item) {
//       $item = new $this->entityName($this->params['id']);
//     }
//     $langs = new LangCollection();
//     $langs = $langs->getPublicLangs();
//     $files = $item->getFiles();
//     foreach ($files as $file) {
//       foreach ($langs as $forlang) {
//         $data['name'.$forlang->getSName()] = $this->params['file-name'.$forlang->getSName().'-'.$file->getId()];
//       }
// //      $data = array(
// //        'nameru' => $this->params['file-nameru-'.$file->getId()],
// //        'nameua' => $this->params['file-nameua-'.$file->getId()],
// //        'nameen' => $this->params['file-nameen-'.$file->getId()],
// //      );
//       $file->update($data);
//     }
//     $entcol = $this->entityName.'fileCollection';
//     $colfiles = new $entcol();
//     foreach ($_FILES['files']['name'] as $key => $file) {
//       $data = $add_data;
//       $fileName = $file;
//       $pos = strrpos($fileName,'.');
//       $data['active'] = 1;
//       $data['ext'] = substr($fileName,$pos+1);
//       $data['filename'] = substr($fileName,0,$pos);
//       foreach ($langs as $forlang) {
//         $data['name'.$forlang->getSName()] = '';
//       }
//       $data[strtolower($this->entityName)] = $item->getId();

//       if ($data['ext'] != '') {
//         $item_file = $colfiles->add($data);
//         if ( !move_uploaded_file($_FILES['files']['tmp_name'][$key],$item_file->getRealPath())){
//           ActionController::addMsg('error', '', $this->conf['upl_failed']);
//           $item_file->setExt('');
//         } else {
//           if ($item_file->withPreview()) {
//             $res_pict = Foto::img_cut($item_file->getRealPath(), $item_file->getRealPathPreview(), $item_file->getPreviewSize('w'), $item_file->getPreviewSize('h'));
//           }
//           //create_water
//           if (isset($this->params['create_water'])) {
//             Foto::create_water($item_file->getRealPath());
//           }
//         }
//         LogsCollection::getInstance()->addlog(strtolower($this->entityName).'file', $item_file->getId(), 'add');
//       }
//     }
//   }

 function updateGallery($item = null, $add_data = array(), $resize_kind = 'cut', $watercount = 3) {
    if (!$item) {
      $item = new $this->entityName($this->params['id']);
    }
    $langs = new LangCollection();
    $langs = $langs->getPublicLangs();
    $files = $item->getFiles();
    foreach ($files as $file) {
      foreach ($langs as $forlang) {
        $data['name'.$forlang->getSName()] = $this->params['file-name'.$forlang->getSName().'-'.$file->getId()];
      }
      $data['order']= $file->getOrder();
      $file->update($data);
    }
    $entcol = $this->entityName.'fileCollection';
    $colfiles = new $entcol();
    foreach ($_FILES['files']['name'] as $key => $file) {
      $data = $add_data;
      $fileName = $file;
      $pos = strrpos($fileName,'.');
      $data['active'] = 1;
      $data['ext'] = substr($fileName,$pos+1);
      $data['filename'] = Settings::translit(substr($fileName,0,$pos));
      foreach ($langs as $forlang) {
        $data['name'.$forlang->getSName()] = '';
      }
      $data[strtolower($this->entityName)] = $item->getId();

      if ($data['ext'] != '') {
        $item_file = $colfiles->add($data);
        if ( !move_uploaded_file($_FILES['files']['tmp_name'][$key],$item_file->getRealPath())){
          ActionController::addMsg('error', '', $this->conf['upl_failed']);
          $item_file->setExt('');
        } else {
          if ($item_file->withPreview()) {
            if($resize_kind == 'cut') {
              $res_pict = Foto::img_cut($item_file->getRealPath(), $item_file->getRealPathPreview(), $item_file->getPreviewSize('w'), $item_file->getPreviewSize('h'));
            } elseif($resize_kind == 'cut_height') {
              // для галереи товара
              $size = getimagesize($item_file->getRealPath());
              if ($size[1] > 0) {
                  $pr_height = 153;
                  $ratio = $size[1] / $pr_height;
                  $pr_width = round($size[0] / $ratio, 0);
                  $res_pict = Foto::img_resize_png($item_file->getRealPath(), $item_file->getRealPathPreview(), $pr_width, $pr_height);
              }
            } else {
              $res_pict = Foto::img_resize($item_file->getRealPath(), $item_file->getRealPathPreview(), $item_file->getPreviewSize('w'), $item_file->getPreviewSize('h'));
            }
          }
          //create_water
          if (isset($this->params['create_water'])) {
            if($watercount == 1) {
                Foto::create_water($item_file->getRealPath());
            } elseif($watercount == 2) {
                Foto::create_water($item_file->getRealPath(), $src_water, 'center-top');
                Foto::create_water($item_file->getRealPath(), $src_water, 'center-bottom');
            } else {
                Foto::create_water($item_file->getRealPath(), $src_water, 'center-top');
                Foto::create_water($item_file->getRealPath(), $src_water, 'center');
                Foto::create_water($item_file->getRealPath(), $src_water, 'center-bottom');
            }
          }
        }
        LogsCollection::getInstance()->addlog(strtolower($this->entityName).'file', $item_file->getId(), 'add');
      }
    }
  }

  function addFile( $entity ) {
    $name_col = ucfirst($entity).'fileCollection';
    $col = new $name_col();
    $parent_id = $this->params['id'];
    unset($this->params['id']);
    $fileName = $_FILES['fileadd']['name'];
    $pos = strrpos($fileName,'.');
    $data['ext'] = substr($fileName,$pos+1);
    $data['fname'] = substr($fileName,0,$pos);
    $data['fname'] = $data['fname'];

    if (!isset($this->params['filename'])) {
      $this->params['filename'] = '';
    }
    if ($data['ext'] != '') {
      DB::getInstance()->begin();
      $file = $col->add(array(
                  mb_strtolower($entity) => $parent_id,
                  'ext' => $data['ext'],
                  'filename' => $data['fname'],
                  'name' => $this->params['filename'],
                  'active' => 1,
                  ));
      LogsCollection::getInstance()->addlog(mb_strtolower($entity).'file', $file->getId(), 'add');
      if ( !move_uploaded_file($_FILES['fileadd']['tmp_name'],$file->getRealPath())){
        throw new Exception('File upload failed');
      }

      if ($file->withPreview() && $file->isPicture()) {
        $pr_size = $file->getPreviewSize();
        $res_pict = Foto::img_cut($file->getRealPath(), $file->getRealPathPreview(), $pr_size['w'], $pr_size['h']);
        if ($res_pict['error']) {
          throw new Exception($res_pict['status']);
        }
      }
      DB::getInstance()->commit();
      return $file;
    } else {
      return false;
    }
  }

  function deleteFile( $entity ) {
    $entity = ucfirst($entity).'file';
    $item = new $entity($this->params['id']);
    $par_id = $item->getParentId();
    $item->delete();
    LogsCollection::getInstance()->addlog(mb_strtolower($entity).'file', $this->params['id'], 'del,p='.$par_id);
    $this->result['type'] = 'file';
    $this->result['id'] = $this->params['id'];
    $this->result['status'] = 'Файл удален';
    $_SESSION['status'] = 'Файл удален';
    $this->result['alert'] = 'success';
    return $this->result;
  }


  function deletepict($item, $pict = array(''), $name = null) {
    if (!$name) {
      $name = 'pict';
    }
    foreach ($pict as $p) {
      if (file_exists($item->getRealPathPict($name, $p))) {
        unlink($item->getRealPathPict($name, $p));
      }
    }
    $item->setPict('', $name);
    ActionController::addMessage('deletepict');
    $this->result['status'] = 'Зображення видалено';
    $this->result['alert'] = 'success';
    $this->result['namepict'] = $name;

    return $this->result;
  }
  // *********** VALIDATE *************************
  function setErrorStatus($field, $status) {
    $this->result['error'] = true;
    $this->result['status-kind'] = 'error';
    $this->result['status-title'] = $this->conf['not_saved'];
    $this->result['status-msg'] = $this->conf['check_fields'];
    if (!isset($this->result[$field])) {
      $this->result['items'][count($this->result['items'])] = $field;
    }
    $this->result[$field]['error'] = 'error';
    $this->result[$field]['status'] = $status;
    return $this->result;
  }
  function validateFields($fields, $item = null) {
    if (is_array($fields)) {
      $i = 0;
      foreach ($fields as $value) {
        $this->result['items'][$i] = $value;
        $i++;
        $this->result[$value]['error'] = '';
        $this->result[$value]['status'] = '';
        if (empty($this->params[$value])) {
          $this->setErrorStatus($value, $this->conf['req_field']);
        }
      }
    } else {
      $value = $fields;
      $this->result[$value]['error'] = '';
      $this->result[$value]['status'] = '';
      $this->result['items'][0] = $value;
      if (empty($this->params[$value])) {
         $this->setErrorStatus($value, $this->conf['req_field']);
      }
    }
    (isset($this->params['langin2'])) ? $this->params['langin2'] = 1 : $this->params['langin2'] = 0;
    (isset($this->params['langin3'])) ? $this->params['langin3'] = 1 : $this->params['langin3'] = 0;
    return $this->result;
  }
  function validateFoto($pict, $item = null, $width = 0, $height = 0) {
    $foto = new Foto();
    $check = $foto->checkFoto($pict, $width, $height, $_FILES[$pict], $item);
    if ($check['error']) {
      $this->result['error'] = true;
      $this->result['alert'] = 'error';
      $this->result['status'] = '<strong>Не збережено!</strong>';
      $this->result[$value]['error'] = 'error';
      $this->result[$value]['status'] = $check['status'];
    }
    $this->result[$pict] = $check['ext'];
    return $this->result;
  }
  // *********** ITEM *************************

  // delete
  function delete($item = null) {
    if (!$item) {
      $item = new $this->entityName($this->params['id']);
    }
    $item->delete();
//    ActionController::addMsg('success', '', $this->conf['deleted_ok']);
    $this->result['status-kind'] = 'success';
    $this->result['status-title'] = $this->conf['deleted_ok'];
    $this->result['id'] = $item->getId();
    $this->result['c'] = mb_strtolower($this->entityName);
    return $this->result;
  }
  // mark delete
  function markDelete($item) {
    if ($item->getDel() == 1) {
      $set_del = 0;
      ActionController::addMessage('Відмітку на видалення знято');
      $this->result['status'] = 'Відмітку на видалення знято';
      LogsCollection::getInstance()->addlog(mb_strtolower($item->Name()), $item->getId(), 'unmark_del');
    } else {
      $set_del = 1;
      ActionController::addMessage('Запис відмічено на видалення');
      $this->result['status'] = 'Запис відмічено на видалення';
      LogsCollection::getInstance()->addlog(mb_strtolower($item->Name()), $item->getId(), 'mark_del');
    }
    $item->setDel($set_del);
    $this->result['mark'] = $set_del;
    $this->result['alert'] = 'success';
    $this->result['id'] = $item->getId();
    $this->result['c'] = mb_strtolower($item->Name());
    return $this->result;
  }
// reorder
  function reorderList() {
    $pageOrder = 1;
    DB::getInstance()->begin();
    foreach ( $this->params as $pageId ) {
      $page = new $this->entityName($pageId);
      $page->setOrder($pageOrder++);
    }
    DB::getInstance()->commit();
    return true;
  }
// Multiple (Banners)
  function updateBanners($item) {
    $url = $item->getUrlEntity();
    if ($url) {
      $data['name'] = $url->getName();
      $data['banner'] = $this->params['banner'.$url->getLang()];
      $url->update($data);
    } else {
      return false;
    }
  }
// Other
  function generatePass($len = 6) {
    $str = "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm0123456789";
    $pass = '';
    for ($i=1; $i<=$len; $i++) {
      $pass .= $str{rand(0,strlen($str)-1)};
    }
    return $pass;
  }

}

?>