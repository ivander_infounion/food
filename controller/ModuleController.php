<?php
/* (c) 2015 InfoUnion CMS v3.0, voloshanivsky@gmail.com */
class ModuleController extends CmsController {

  function __construct($params) {
    $this->entityName = 'Module';
    $params['ctrl'] = strtolower(substr(get_class($this), 0, -10));
    if (!parent::__construct($params)) {
      return;
    }
    if ($this->admin->getLogin() != 'root') {
      $this->displayError('access');
      return;
    }
  }

  function ListAction() {
    $this->view->assign('tablednd', 'list');
    $entCol = $this->entityName.'Collection';
    $col = new $entCol();
    $col = $col->getModules();
    $this->list_action($col);
  }

  function NewAction() {
    $this->view->assign('tablang', true);
    $this->new_action();
  }

  function EditAction() {
    $this->view->assign('tablang', true);
    $item = new $this->entityName($this->params['id']);
    $this->view->assign('langItems', $item->langItems);
    $this->edit_action(null, $item->getName($this->lang));
  }

}

class ModuleAjaxController extends CmsAjaxController {

  function __construct($params) {

    parent::__construct($params);
    $this->entityName = 'Module';
    $this->colName = $this->entityName.'Collection';

  }


  function checkValidate($item = null) {

    $require = array('controller');
    foreach ($this->params['lang'] as $lang) {
      $require[] = 'name'.$lang;
    }
    $this->validateFields($require, $item);
    return $this->result;

  }

  function AddAction() {

    $this->checkValidate();
    if ($this->result['error']) {
      return $this->result;
    }

    $col = new $this->colName();
    DB::getInstance()->begin();
    $item = $col->add($this->params);
    LogsCollection::getInstance()->addlog('admin', $item->getId(), 'add');
    DB::getInstance()->commit();

    $this->result['c'] = strtolower($this->entityName);
    $this->result['id'] = $item->getId();
    ActionController::addMsg('success', '', $this->conf['added_ok']);
    return $this->result;

  }

  function DeleteAction() {
    $this->result['status-kind'] = 'error';
    $this->result['status-title'] = 'Removing Module not allowed!';
    return $this->result;
    // return $this->delete();
  }

  function UpdateAction() {
    $item = new $this->entityName($this->params['id']);
    $this->checkValidate($item);
    if ($this->result['error']) {
      return $this->result;
    }

    DB::getInstance()->begin();
    $item->update($this->params);
    LogsCollection::getInstance()->addlog('admin', $item->getId(), 'update');
    DB::getInstance()->commit();
    $this->result['c'] = strtolower($this->entityName);
    $this->result['id'] = $item->getId();
    ActionController::addMsg('success', '', $this->conf['saved_ok']);
    return $this->result;

  }

  function ReorderlistAction() {
    return $this->reorderList();
  }

}

?>