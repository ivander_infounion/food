<?php
/* (c) 2015 InfoUnion CMS v3.0, voloshanivsky@gmail.com */
class TranslateController extends CmsController {

  function __construct($params) {
    $this->entityName = 'Translate';
    $params['ctrl'] = strtolower(substr(get_class($this), 0, -10));
    if (!parent::__construct($params)) {
      return;
    }
    // if ($this->admin->getLogin() != 'root') {
    //   $this->displayError('access');
    //   return;
    // }
  }

  function ListAction() {
    $entCol = $this->entityName.'Collection';
    $col = new $entCol();

    if (isset($this->params['order'])) {
      $order = $this->params['order'];
    } else {
      $order = 'translatehaslang_value COLLATE utf8_unicode_ci';
    }
    if (!isset($this->params['text'])) {
      $text = '';
    } else {
      $text = $this->params['text'];
    }
    $this->view->assign('findtext', $text);
    $count = $col->getByText($text)->count();
    $pager = $this->createPager(50, $count);
    $entCol = $this->entityName.'Collection';
    $col = new $entCol();
    $col = $col->getByText($text, "$order $pager");
    $this->list_action($col);
  }

  function NewAction() {
    $this->view->assign('tablang', true);
    $this->new_action();
  }

  function EditAction() {
    $item = new $this->entityName($this->params['id']);
    $this->view->assign('tablang', true);
    $this->view->assign('langItems', $item->langItems);
    $this->edit_action(null, $item->getName($this->lang));
  }

}

class TranslateAjaxController extends CmsAjaxController {

  function __construct($params) {

    parent::__construct($params);
    $this->entityName = 'Translate';
    $this->colName = $this->entityName.'Collection';

  }


  function checkValidate($item = null) {
    $require = array('name', 'value1');
    $this->validateFields($require, $item);
    return $this->result;
  }

  function AddAction() {

    $this->checkValidate();
    if ($this->result['error']) {
      return $this->result;
    }
    $col = new $this->colName();
    DB::getInstance()->begin();
    $item = $col->add($this->params);
    LogsCollection::getInstance()->addlog(strtolower($this->entityName), $item->getId(), 'add');
    DB::getInstance()->commit();

    $this->result['c'] = strtolower($this->entityName);
    $this->result['id'] = $item->getId();
    ActionController::addMsg('success', '', $this->conf['added_ok']);
    return $this->result;

  }

  function DeleteAction() {
    $item = new $this->entityName($this->params['id']);
    $info = $item->getName().', '.$item->getRu();
    LogsCollection::getInstance()->addlog(strtolower($this->entityName), $item->getId(), 'delete', $this->admin->getId(), $info);
    return $this->delete($item);
  }

  function UpdateAction() {
    $item = new $this->entityName($this->params['id']);

    $this->checkValidate($item);
    if ($this->result['error']) {
      return $this->result;
    }

    DB::getInstance()->begin();
    $item->update($this->params);
    LogsCollection::getInstance()->addlog(strtolower($this->entityName), $item->getId(), 'update');
    DB::getInstance()->commit();
    $this->result['c'] = strtolower($this->entityName);
    $this->result['id'] = $item->getId();
    ActionController::addMsg('success', '', $this->conf['saved_ok']);
    return $this->result;

  }

}

?>