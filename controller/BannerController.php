<?php
/* (c) 2015 InfoUnion CMS v3.0, voloshanivsky@gmail.com */
class BannerController extends CmsController {

  protected $section;
  function __construct($params) {
    $this->entityName = 'Banner';
    $params['ctrl'] = strtolower(substr(get_class($this), 0, -10));
    if (!parent::__construct($params)) {
      return;
    }
  }

  function ListAction() {
    $this->view->assign('tablednd', 'list');
    $entCol = $this->entityName.'Collection';
    $col = new $entCol();
    $col = $col->getByParams(array(), 'order DESC');
    $this->list_action($col, 'banner/list.tpl');
  }

  function NewAction() {
    $this->view->assign('tablang', true);
    $this->view->assign('editor', true);
    $this->view->assign('langItems', array(1=>true));
    $this->new_action();
  }

  function EditAction() {
    $item = new $this->entityName($this->params['id']);
    $this->view->assign('editor', true);
    $this->view->assign('tablang', true);
    $this->view->assign('langItems', $item->langItems);
    $this->edit_action(null, $item->getName($this->lang));
  }

}

class BannerAjaxController extends CmsAjaxController {

  function __construct($params) {

    parent::__construct($params);
    $this->entityName = 'Banner';
    $this->colName = $this->entityName.'Collection';

  }

  function ActiveAction() {
    return $this->activeAll();
  }

  function checkValidate($item = null) {

    $fileinfo = $this->getFileInfo('ext');
    if ($item) {
      if (!empty($fileinfo['ext'])) {
        $this->params['ext'] = $fileinfo['ext'];
        $this->params['filename'] = $fileinfo['filename'];
      } else {
        unset($this->params['ext']);
        unset($this->params['filename']);
      }
      $this->params['order'] = $item->getOrder();
    } else {
      $this->params['ext'] = $fileinfo['ext'];
      $this->params['filename'] = $fileinfo['filename'];
    }
    foreach ($this->params['lang'] as $l) {
      $require[] = 'name'.$l;
    }
    $this->validateFields($require, $item);
    return $this->result;

  }

  function AddAction() {
    $this->checkValidate();
    if ($this->result['error']) {
      return $this->result;
    }

    $types = array("");
    foreach ($types as $key => $value) {
      $pictbef[$value] = "";
    }

    $col = new $this->colName();
    DB::getInstance()->begin();
    $item = $col->add($this->params);
    $this->updatePictures($item, $pictbef, $types);
    LogsCollection::getInstance()->addlog(strtolower($this->entityName), $item->getId(), 'add');
    DB::getInstance()->commit();

    $this->result['c'] = strtolower($this->entityName);
    $this->result['id'] = $item->getId();
    ActionController::addMsg('success', '', $this->conf['added_ok']);
    return $this->result;

  }

  function updatePictures($item, $pictbef, $types = array("")) {
    foreach($types as $type){
      if ($this->params['ext'.$type] != '') {
        if ( !move_uploaded_file($_FILES['ext'.$type]['tmp_name'],$item->getRealPathPict($type))){
          ActionController::addMsg('error', '', $this->conf['upl_failed']);
          $item->setPict($pictbef[$type], $type);
        } else {
          $res_pict = Foto::img_cut($item->getRealPathPict($type), $item->getRealPathPreview($type), 
                                    $item->getPreviewSize('w','pict'.$type), $item->getPreviewSize('h','pict'.$type));
        }
      }
    }
  }

  function DeleteAction() {
    $item = new $this->entityName($this->params['id']);
    //delete or set null relations
    $info = $item->getName(1);
    LogsCollection::getInstance()->addlog(strtolower($this->entityName), $item->getId(), 'delete', $this->admin->getId(), $info);
    return $this->delete();
  }
  function DeleteimgAction() {
    $item = new $this->entityName($this->params['id']);
    if (file_exists($item->getRealPath())) {
      unlink($item->getRealPath());
    }
    $item->setExt('');
    $item->setFileName('');
    $this->result['pictname'] = 'ext';//$this->params['pictname'];
    $this->result['status-kind'] = 'success';
    $this->result['status-title'] = $this->conf['deleted_ok'];
    return $this->result;
  }

  function UpdateAction() {
    $item = new $this->entityName($this->params['id']);
    $this->checkValidate($item);
    if ($this->result['error']) {
      return $this->result;
    }
    $types = array("");
    foreach ($types as $key => $value) {
      $pictbef[$value] = $item->getPict();
    }
    $fnbef = $item->getFileName();
    DB::getInstance()->begin();
    $item->update($this->params);
    $this->updatePictures($item, $pictbef, $types);
    LogsCollection::getInstance()->addlog(strtolower($this->entityName), $item->getId(), 'update');
    DB::getInstance()->commit();
    $this->result['c'] = strtolower($this->entityName);
    $this->result['id'] = $item->getId();
    ActionController::addMsg('success', '', $this->conf['saved_ok']);
    return $this->result;

  }

  function ReorderlistAction() {
    return $this->reorderList($this->entityName);
  }
}

?>