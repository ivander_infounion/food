<?php
/* (c) 2015 Voloshanivsky, voloshanivsky@gmail.com */
class DefaultController extends ActionController {

  private $lang;
  private $langid;
  private $langurl;
  private $url;
  private $sections;
  private $active_menu;
  protected $conf;
  private $sett;
  private $bread;
  private $defparams;
  function __construct($params) {
    parent::__construct($params);

    session_start();
    $ip_debug = array('127.0.0.1');
    $client_ip = $_SERVER["REMOTE_ADDR"];
    if (in_array($client_ip, $ip_debug)) {
      $this->view->assign('debug', true);
    }
    $this->view->assign('lastmod', time());

    try {
      $lang = Lang::getRealId($this->params['lang']);
    } catch (Exception $e) {
      $lang_not_found = true;
      $this->params['lang'] = 'en';
      $lang = Lang::getRealId($this->params['lang']);
    }


    $lang = Lang::getRealId($this->params['lang']);
    $this->lang = new Lang($lang);
    $this->langid = $this->lang->getId();
    $this->view->assign('langid', $this->langid);
    $this->langurl = $this->lang->getUrl();
    $this->view->assign('langurl', $this->langurl);
    $this->view->assign('lang', $this->lang->getSname());

    $this->sett = new Sett(1);
    $this->view->assign('sett', $this->sett);

    $trans = new TranslateCollection();
    $this->conf = $trans->getTranslate($this->lang->getSname(), 'pub');
    $this->view->assign('conf', $this->conf);

    $this->translate = new Translatelist($this->lang->getSname(), 'pub', 'default');
    $this->view->assign('translate', $this->translate);

    $sections = new SectionCollection();
    $mainmenu = $sections->getByParams(array('lang'=>$this->langid, 'mainmenu'=>1, 'depth'=>0, 'active'=>1));
    $this->view->assign('mainmenu', $mainmenu);
    $sections = new SectionCollection();
    $footmenu = $sections->getByParams(array('lang'=>$this->langid, 'footmenu'=>1, 'active'=>1))->toArray();
    $this->view->assign('footmenu', $footmenu);

    $this->defparams = array('active'=>1);

    // ассайн коллекции на вьюшку "по новому" выглядит так
    // $this->assignCol('Autoyear', 'years');

    $this->view->assign('d',Settings::get('d'));


    if($lang_not_found){
      $this->displayError($this->translate->val('page_not_found'), $this->translate->val('lang_not_allowed'));
      $this->view->display('default/default.tpl');
      exit();
    }
    
  }

  function DefaultAction() {
    if (isset($this->params['url1'])) {
      $this->url = Url::getByNameLang($this->params['url1'], $this->langid);
      if ($this->url) {
        $this->checkUrl();
      } else {
        $this->notUrl();
      }
    } else {
      $this->displayIndex();
    }

    $this->view->display('default/default.tpl');
  }

  /**
   * Get Url item for current page
   *
   * @return Url object
   *
   */
  function getCurUrl() {
    $result = '';
    if (isset($this->params['url1'])) {
      $this->url = Url::getByNameLang($this->params['url1'], $this->langid);
    } else {
      $this->url = false;
    }
    if ($this->url) {
      $result = $this->url->getName().'.html';
    }

    return $result;
  }

  function notUrl() {
    $url = $this->params['url1'];

    if ($url == 'search') {
      $this->displaySearch($this->params['text']);
    }else{
      $this->displayError();
    }


  }

  /**
   * Displays error page (404 code) or message page (200 code) if first parameter included. 
   *
   * @param string $error Message for message page
   * 
   * @param string $desc  Description for message page
   *
   */
  function displayError($error = null, $desc = '') {
    if (!$error) {
      header("HTTP/1.0 404 Not Found");
      $error = $this->conf['page_not_found'];
    }
    $this->view->assign('error', $error);
    $this->view->assign('error_desc', $desc);
    $this->view->assign('fileTpl', 'default/error.tpl');
  }

  function checkUrl() {
    if ($this->url->getActive() == 0) {
      $this->displayError($this->conf['page_unavailable']);
      return;
    }
    $this->bread[0]['name'] = $this->conf['mainpage'];
    $this->bread[0]['url'] = $this->langurl;
    $ctrl = $this->url->getCtrl();
    $page = $this->url->getPage()->getParent();
    if (!$page) {
      $this->displayError();
      return;
    }

    $this->view->assign('url', $this->url);
    $this->view->assign('page', $page);
    $active_menu = array();
    if ($ctrl == 'section') {
      if ($page->getDepth() == 1) {
        $this->bread[1]['name'] = $page->getParent()->getName($this->langid);
        $this->bread[1]['url'] = $this->langurl.$page->getParent()->getUrl($this->langid).'.html';
        $this->bread[2]['name'] = $page->getName($this->langid);
        $this->bread[2]['url'] = '#';
        $active_menu[0] = $page->getParentId();
        $active_menu[1] = $page->getId();
      } else {
        $this->bread[1]['name'] = $page->getName($this->langid);
        $this->bread[1]['url'] = '#';
        $active_menu[0] = $page->getId();
      }
      $ctrl = $page->getCtrl();
      if ($ctrl == 'mainpage') {
        $this->displayIndex();
      } elseif ($ctrl == 'textpage') {
        $this->displayTextpage($page);
      } elseif ($ctrl == 'contact') {
        $this->displayContact($page);
      } elseif ($ctrl == 'goods') {
        $this->displayGoodsList($page);
      } else {
        $this->displayError($this->conf['is_work']);
      }
    } elseif (in_array($ctrl, array('article'))) {
      $section = $page->getSection();
      $this->view->assign('section',$section);
      if ($section->getDepth() == 1) {
        $this->bread[1]['name'] = $section->getParent()->getName($this->langid);
        $this->bread[1]['url'] = $this->langurl.$section->getParent()->getUrl($this->langid).'.html';
        $this->bread[2]['name'] = $section->getName($this->langid);
        $this->bread[2]['url'] = $this->langurl.$section->getUrl($this->langid).'.html';
        $this->bread[3]['name'] = $page->getName($this->langid);
        $this->bread[3]['url'] = '#';
        $active_menu[0] = $section->getParentId();
        $active_menu[1] = $section->getId();
      } else {
        $this->bread[1]['name'] = $section->getName($this->langid);
        $this->bread[1]['url'] = $this->langurl.$section->getUrl($this->langid).'.html';
        $this->bread[2]['name'] = $page->getName($this->langid);
        $this->bread[2]['url'] = '#';
        $active_menu[0] = $section->getId();
      } if ($ctrl == 'article') {
        $this->displayArticleDetal($page);
      } else {
        $this->displayError($this->conf['is_work']);
      }
    } elseif ($ctrl == 'goods') {
      $this->displayGoodsDetail($page);
    } else {
      $this->displayError($this->conf['is_work']);
    }
    $this->view->assign('active_menu', $active_menu);
    $this->view->assign('bread', $this->bread);
  }


  /**
   * Assignes latest articles in  $last_articles variable on view
   *
   * @param int $count Number of last articles
   * 
   */
  function assignArticles($count = 3) {
    $col = new ArticleCollection();
    $col = $col->getByParams(array('active'=>1, 'actual'=>1, 'lang'=>$this->langid), 'created DESC LIMIT '.$count);
    if ($col->count() > 0) {
      $this->view->assign('last_articles', $col);
      $sectnews = Section::getByCtrl('article');
      if ($sectnews) {
        $this->view->assign('url_articles', $sectnews->getUrl());
      }
    }
  }

  /**
   * Assignes special variables and query string to paginate items
   *
   * @param int $limit Number of items in one page
   * 
   * @param int $total Number of all items in list
   * 
   * @return string with sql limit for pagination
   * 
   */
  function createPager($limit, $total) {
    $countpages = floor($total/$limit - 0.001) + 1;
    $this->view->assign('countitems', $total);
    $this->view->assign('countpages', $countpages);
    if (isset($this->params['p'])) {
      $page = $this->params['p'];
    } else {
      $page = 1;
    }
    $this->view->assign('p', $page);
    $fromp = $page - 5;
    if ($fromp < 1) {
      $fromp = 1;
    }
    $to_p = $page+5;
    if ($to_p > $countpages) {
      $to_p = $countpages;
    }
    $this->view->assign('from_p', $fromp);
    $this->view->assign('to_p', $to_p);
    return " LIMIT ".($page-1)*$limit.", $limit";
  }

  /**
   * Assignes collection on view
   *
   * @param string $colname Collection classname without "Collection"
   * 
   * @param int $varname Name of variable on view with collection of items
   * 
   * @param array('parameter_name'=>'parameter_value') $params Parameters, that passes getByParams() of current collection
   * 
   * @param string $order Order that passes getByParams() of current collection
   * 
   */
  function assignCol($colname, $varname, $params = array(), $order = null){
    if(empty($params)){
      $params = $this->defparams;
    }
    if(strpos($colname, 'Has')!==false){
      $colname = $colname.'Collection';
    }else{
      $colname = ucfirst($colname).'Collection';
    }
    $col = new $colname();
    $col = $col->getByParams($params,$order);
    if($col->count()){
      $this->view->assign($varname, $col);
    }
  }

  function displayIndex() {
    $sett = new Sett(1);
    $section = new Section(1);
    $this->view->assign('active_menu', array('m0'=>1));
    $this->view->assign('index', 1);
    $this->view->assign('page', $section);
    $this->view->assign('mainpage', new Mainpagelang($this->langid));

    $this->view->assign('fileTpl','default/index.tpl');
  }

  function displayTextpage($section) {
    $this->view->assign('fileTpl', 'default/textpage.tpl');
  }

  function displayContact($page) {
    $this->view->assign('fileTpl', 'default/contacts.tpl');
  }
  function displayArticleList($page) {
    $params = array('active'=>1, 'actual'=>1);
    $col = new ArticleCollection();
    $count = $col->getByParams($params)->count();
    $pager = $this->createPager(6,$count);
    $col = new ArticleCollection();
    $col = $col->getByParams($params, 'created DESC '.$pager);
    $this->view->assign('list', $col);
    $this->view->assign('fileTpl', 'default/article-list.tpl');
  }
  function displayArticleDetal($page) {
    if (!$page->getActual()) {
      $this->displayError();
      return;
    }     
    $picts = $page->getFiles(1);
    $this->view->assign('gallery', true);
    $this->view->assign('pictures', $picts);
    $this->view->assign('fileTpl', 'default/article-detal.tpl');
  }

  function displayGoodsList($page) {
    $this->view->assign('page', $page);
    $this->view->assign('fileTpl', 'default/goods-list.tpl');
  }

  function displayGoodsDetail($page) {
    $this->view->assign('page', $page);
    $this->view->assign('fileTpl', 'default/goods-detail.tpl');
  }

  function displaySearch($text) {
    $findtext = addslashes(strip_tags(htmlspecialchars($text)));
    $params = $this->defparams;
    $params['search'] = $findtext;
    // $col = new CoverCollection();
    // $list = $col->getByParams($params);
    $this->view->assign('list', $list);
    $this->view->assign('fileTpl', 'default/search-list.tpl');
  }

}

class DefaultAjaxController extends AjaxActionController {
  private $d;
  public $conf;
  public $lang;
  private $google_captcha_secret;
  private $google_captcha_public;

  function __construct($params) {
    parent::__construct($params);
    $this->d = Settings::get('d');
    session_start();
    if (isset($this->params['page_language'])) {
      $this->lang = new Lang($this->params['page_language']);
      $trans = new TranslateCollection();
      $this->conf = $trans->getTranslate($this->lang->getSname());
    } else {
      throw new NotFoundException("Language error! 'page_language' variable is missing in form data.");
    }
  }



    // MAIL
    // $sett = new Sett(1);
    // $title = $email_title;
    // $body = file_get_contents("view/mail/infomail.html");
    // $body = str_replace(array('{TITLE}', '{CONTENT}', '{BASEURL}'),
    //                     array($title,$content_mail,Settings::get('defaulturl')),$body);
    // $mail = new Mail('UTF-8');
    // $mail->Subject($title);
    // $mail->From('info@promobiz.com.ua');
    // // $mail->To($email_address);
    // $mail->To('vakafonic@gmail.com');
    // $mail->Body($body, 'html');
    // $mail->Attach("images/logo.jpg","logo.jpg","image/jpg");  
    // $mail->Send();


  /**
   * Saving simle forms without file or special logic to "callback" table
   *
   * see example in example\callback\
   * 
   * example of input fields
   * 
   * use js validator if no special validation needed
   * 
   * Warning! to make it works use callback2array function from public.js before sending. Also see the right markup at the examples folder
   * 
   * $this->params['fields'] = array(   
   *                                  'name' => 'name', input tag name 
   *                                  'type' => 'input', //type of field , input or textarea allowed   
   *                                  'data' => 'Иван', // value of input tag   
   *                                  'fieldsname' => 'Имя', //name of field to show in edit form   
   *                                )
   *  
   */
   function SavecallbackAction(){
    $fields = $this->params['fields'];
    //validation in validate.js
    //saving to callback
    $cbcol = new CallbackCollection();
    $cbfcol = new CallbackfieldCollection();
    $cb = $cbcol->add( array( 'created' =>date("Y-m-d H:i", time()),
                        'status'=>0,
                        'formtype'=> $this->params['formtype']));
    foreach ($fields as $f) {
      if($f['name']){
        $cbfcol->add(array( 'name'=>$f['name'],
                            'type'=>$f['type'],
                            'data'=>$f['value'],
                            'fieldname'=>$f['fieldname'],
                            'callback'=>$cb->getId()
                            ));
      }
      if($f['name'] == 'name'){
        $username = $f['value'];
      }
    }
    $result['error']=false;
    $result['name']='Спасибо за заказ,'.$username.'!';
    $result['title']="Ваш заказ приянт в обработку!";
    $result['text']="Наш менеджер свяжется с Вами в ближашее время";
    $result['formtype']=$this->params['formtype'];
    return $result;
  }

}

?>
