<?php
class FilesController extends ActionController {
  // если файлы не общедоступные, то проверять доступность
  // или это админ или пользователь с доступом к файлам
  
//  function __construct($params) {
//    parent::__construct($params);
//    if ( !Admin::GetAuthenticatedInstance() ) {
//      $this->redirect('admin', 'login');
//    }   
//  }
  
  function DownloadAction() {
    //авторизация
    //проверка авторизации админа - это когда в админке посмотреть файл
    // if (!Admin::GetAuthenticatedInstance()) {
    //   $this->redirect('default', 'login');
    // }
    
    header ("Expires: Mon, 26 Jul 1990 05:00:00 GMT");
    header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header ("Cache-Control: no-cache, must-revalidate");
    header ("Pragma: no-cache");
    
    try {

      $file = Recordfile::getFile($this->params['ctrl'], $this->params['key']);

      $filePath = $file->getRealPath();

    } catch ( EntityException $e ) {
      throw new NotFoundException('File not found');
    }
//    header('Content-type: '.mime_content_type($price->getRealPathRetail()));
    header('Content-Disposition: attachment; filename="' . basename($file->getRealFileName()) . '";');
    header('Content-type: application/x-octet-stream');
    header('Content-Length: '.filesize($filePath));
    readfile($filePath);
  }
} 

class FilesAjaxController extends AjaxActionController {
} 
?>
