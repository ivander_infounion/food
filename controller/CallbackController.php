<?php
/* (c) 2015 InfoUnion CMS v3.0, voloshanivsky@gmail.com */
class CallbackController extends CmsController {

  function __construct($params) {
    $this->entityName = 'Callback';
    $params['ctrl'] = strtolower(substr(get_class($this), 0, -10));
    if (!parent::__construct($params)) {
      return;
    }
    if(!$params['formtype']){
      $this->displayError('Не указан тип формы заявок, обратитесь к разработчикам');
      return;
    }else{
      $this->view->assign('formtype', $params['formtype']);
    }

  }

  function ListAction() {
    $entCol = $this->entityName.'Collection';
    $params = array('formtype'=>$this->params['formtype']);
    $col = new $entCol();
    $count = $col->getByParams($params,'created DESC')->count();
    $pager = $this->createPager(30, $count);
    $col = new $entCol();
    $col = $col->getByParams($params,'created DESC');
    $this->view->assign('list', $col);
    $this->view->assign('fileTpl', 'callback/list.tpl');
    $this->view->display('admin/default.tpl');
  }

  function EditAction() {
    $item = new $this->entityName($this->params['id']);
    $this->view->assign('item', $item);
    $col = new WorkerCollection();
    $col = $col->getByParams(array());
    $this->view->assign('workers', $col);
    $this->view->assign('fileTpl', 'callback/edit.tpl');
    $this->view->display('admin/default.tpl');
  }

}

class CallbackAjaxController extends CmsAjaxController {

  function __construct($params) {

    parent::__construct($params);
    $this->entityName = 'Callback';
    $this->colName = $this->entityName.'Collection';

  }

  function ActiveAction() {
    return $this->active();
  }
  function RemovepictAction() {
    $entname = $this->entityName.'file';
    $item = new $entname($this->params);
    $item->delete();
    $this->result['id'] = $this->params;
    $this->result['status-kind'] = 'success';
    $this->result['status-title'] = $this->conf['deleted_ok'];
    return $this->result;
  }
  function DeleteimgAction() {
    $item = new $this->entityName($this->params['id']);
    $this->result['pictname'] = $this->params['pictname'];
    $this->params['pictname'] = substr($this->params['pictname'],4);
    if (file_exists($item->getRealPathPict($this->params['pictname']))) {
      unlink($item->getRealPathPict($this->params['pictname']));
    }
    $item->setPict('', $this->params['pictname']);
    $this->result['status-kind'] = 'success';
    $this->result['status-title'] = $this->conf['deleted_ok'];
    return $this->result;
  }

   function checkValidate($item = null) {
    $require = array('worker');
    $this->validateFields($require, $item);
    if ($item->getWorkerId()==0){
      $this->params['success'] = date("Y-m-d H:i:s");
    }
    return $this->result;

  }

  function DeleteAction() {
    $item = new $this->entityName($this->params['id']);
    $info = $item->getName(1);
    LogsCollection::getInstance()->addlog(strtolower($this->entityName), $item->getId(), 'delete', $this->admin->getId(), $info);
    return $this->delete();
  }
  function UpdateAction() {
    $item = new $this->entityName($this->params['id']);
    $this->checkValidate($item);
    if ($this->result['error']) {
      return $this->result;
    }
    DB::getInstance()->begin();
    $fields = $this->params;
    foreach ($fields as $fk => $f) {
      if (strpos($fk,'meta_')!== false){
        $fieldname = str_replace('meta_', '', $fk);
        $cbf = Callbackfield::getFieldMeta($item->getId(),$fieldname);
        $cbf->setData($f);
        unset($this->params[$fk]);
      }
    }
    $item->update($this->params);   
    LogsCollection::getInstance()->addlog(strtolower($this->entityName), $item->getId(), 'update');
    DB::getInstance()->commit();
    $this->result['c'] = strtolower($this->entityName);
    $this->result['id'] = $item->getId();
    ActionController::addMsg('success', '', $this->conf['saved_ok']);
    return $this->result;

  }

  function ReorderlistAction() {
    return $this->reorderList();
  }
}

?>