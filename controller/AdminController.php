<?php
/* (c) 2010-2015 voloshanivsky, voloshanivsky@gmail.com */
class AdminController extends ActionController {

  protected $conf;
  function __construct($params) {
    parent::__construct($params);
    $this->view->assign('d', Settings::get('d'));
    // STATUS
    if (isset($_SESSION['status'])) {
      $alert['kind'] = 'success';
      $alert['status'] = $_SESSION['status'];
      $this->view->assign('alert', $alert);
      unset($_SESSION['status']);
    }
    if (isset($_SESSION['error'])) {
      $alert['kind'] = 'error';
      $alert['status'] = $_SESSION['error'];
      $this->view->assign('alert', $alert);
      unset($_SESSION['error']);
    }
    $lang = Settings::get('deflang');
    $trans = new TranslateCollection();
    $this->conf = $trans->getTranslate($lang, 'cms');
    $this->view->assign('conf', $this->conf);

    $this->view->ctrl['name'] = 'admin';
    $this->view->ctrl['title'] = $this->conf['admin_header'];
    $this->view->current_url = 'admin';
    $this->view->assign('logo_start', Settings::get('logo_start'));
    $this->view->assign('current_year', date("Y"));
  }

  function DefaultAction() {
    $admin = Admin::getAuthenticatedInstance();
    if ($admin) {
      $this->redirect('cms', 'mainpage');
    }
    $this->view->action['name'] = '';
    $this->view->action['title'] = '';
    $this->view->display('admin/login.tpl');
  }

  function LoginAction() {
    # вывод формы логина
    $this->view->display('admin/login.tpl');
  }

  function LogoutAction() {
    Admin::unsetCookie();
    $admin = Admin::getAuthenticatedInstance();
    if ($admin) {
      LogsCollection::getInstance()->addlog('admin',$admin->getId(),'logout',$admin->getId());
      $admin->resetKey();
    }
    $this->redirect('', '');
  }

}

class AdminAjaxController extends AjaxActionController {

  function __construct($params) {
    parent::__construct($params);
    $trans = new TranslateCollection();
    $this->conf = $trans->getTranslate(Settings::get('deflang'));
  }

  function LoginAction() {

    $this->checkRequiredFields(array('login','pass'));
    if ($this->result['error'] == true) {
      return $this->result;
    }
    try {
      $admin = Admin::getByLogin($this->params['login']);
    } catch (Exception $e) {
      throw new NotFoundException($e);
    }
    $this->result['error'] = true;
    if (!$admin) {
      $this->result['login']['error'] = 'error';
      $this->result['login']['status'] = $this->conf['user_not_found'];
      return $this->result;
    }
    if (($admin->getActive() == 0) || ($admin->getDel() == 1)) {
      $this->result['login']['error'] = 'error';
      $this->result['login']['status'] = $this->conf['access_denied'];
      return $this->result;
    } elseif ($admin->getActive() == 2) {
      $this->result['login']['error'] = 'error';
      $this->result['login']['status'] = $this->conf['access_locked'];
      return $this->result;
    }
    $this->params['pass'] = md5($this->params['pass']);
    if ( $admin->getPass() == $this->params['pass']) {
      $this->result['error'] = false;
      $admin->setCookie($this->params['remember']);
      $admin->setWrongpass(0);
      LogsCollection::getInstance()->addlog('admin',$admin->getId(),'login',$admin->getId());
      return $this->result;
    } else {
      $admin->incWrongpass();
      $this->result['pass']['error'] = 'error';
      $this->result['pass']['status'] = $this->conf['wrong_pass'].'!';
      if ($admin->getActive() == 2) {
        $this->result['pass']['status'] .= '. '.$this->conf['five_times_wrong'];
      } else {
        $last = 5 - $admin->getWrongpass();
        if ($last == 1) {
          $this->result['pass']['status'] .= '. '.$this->conf['remaining_2']. ' 1 '.$this->conf['attempt_2'];
        } else {
          $this->result['pass']['status'] .= '. '.$this->conf['remaining']. ' '.$last.' '.$this->conf['attempt'];
        }
      }
      return $this->result;
    }
  }
}

?>
