<?php
/* (c) 2015 InfoUnion CMS v3.0, voloshanivsky@gmail.com */
class TextpageController extends CmsController {

  function __construct($params) {
    $this->entityName = 'Section';
    $params['ctrl'] = strtolower(substr(get_class($this), 0, -10));
    if (!parent::__construct($params)) {
      return;
    }
    $this->view->assign('menu_content', true);
    $this->insertBreadcrumbs(array('url'=>FrontController::MakeUrl('section','list'),'title'=>$this->conf['sitemap']));
  }

  function EditAction() {
    $this->view->assign('editor', true);
    $this->view->assign('tablang', true);
    $item = new $this->entityName($this->params['id']);
    $this->view->assign('langItems', $item->langItems);
    $this->view->current_section = $item->getData();
    $this->view->ctrl['title'] = $item->getName($this->lang);
    $col = new BannerCollection();
    $this->view->assign('banners', $col->getByParams());
    $isset_banners = array();
    foreach ($item->langItems as $langitem) {
      $url = $langitem->getUrlEntity();
      if ($url) {
        $isset_banners[$url->getLang()] = $url->getArrayIds('banner');
      }
    }
    $this->view->assign('isset_banners', $isset_banners);
    $this->edit_action();
//    $this->edit_action(null, $item->getName($this->lang));
  }

}

class TextpageAjaxController extends CmsAjaxController {

  function __construct($params) {

    parent::__construct($params);
    $this->entityName = 'Section';
    $this->colName = $this->entityName.'Collection';

  }

  function checkValidate($item = null) {

    $require = array();
    foreach ($this->params['lang'] as $l) {
      $require[] = 'name'.$l;
      $require[] = 'url'.$l;
    }
    $this->validateFields($require, $item);
    return $this->result;

  }

  function UpdateAction() {
    $item = new $this->entityName($this->params['id']);
    $this->checkValidate($item);
    if ($this->result['error']) {
      return $this->result;
    }

    DB::getInstance()->begin();
    $item->update($this->params);
    foreach ($item->langItems as $langitem) {
      $this->updateBanners($langitem);
    }

    LogsCollection::getInstance()->addlog('section', $item->getId(), 'update');
//    $this->updateGallery($item);
    DB::getInstance()->commit();
    $this->result['c'] = 'section';
    $this->result['id'] = $item->getId();
    ActionController::addMsg('success', '', $this->conf['saved_ok']);
    return $this->result;

  }

}

?>