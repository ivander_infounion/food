<?php
class TimelineController extends CmsController {

    protected $section;
    function __construct($params) {
        $this->entityName = 'Timeline';
        $params['ctrl'] = strtolower(substr(get_class($this), 0, -10));
        if (!parent::__construct($params)) {
            return;
        }
        $this->view->assign('menu_content', true);
    }

    function ListAction() {
        $entCol = $this->entityName.'Collection';
        $col = new $entCol();
        $col = $col->getByParams(array());

        $this->list_action($col);
    }

    function NewAction() {

        $foodset = new FoodsetCollection();
        $foodset = $foodset->getByParams();
        $this->view->assign('foodset', $foodset);
        $this->view->assign('datepick', array(0=>array('name'=>'created','time'=>false)));
        $this->new_action();
    }

    function EditAction() {
        $item = new $this->entityName($this->params['id']);

        $foodset = new FoodsetCollection();
        $foodset = $foodset->getByParams();
        $this->view->assign('foodset', $foodset);
        $this->view->assign('datepick', array(0=>array('name'=>'created','time'=>true)));

        $this->edit_action(null, $item->getCreated('d.m.y'));
    }

}

class TimelineAjaxController extends CmsAjaxController {

    function __construct($params) {

        parent::__construct($params);
        $this->entityName = 'Timeline';
        $this->colName = $this->entityName.'Collection';

    }

    function checkValidate($item = null) {
        $require = array('created');
        $this->validateFields($require, $item);
        $this->params['created'] = date("Y-m-d", strtotime($this->params['created']));
        return $this->result;
    }

    function AddAction() {
        $this->checkValidate();
        if ($this->result['error']) {
            return $this->result;
        }
        $col = new $this->colName();
        DB::getInstance()->begin();
        $item = $col->add($this->params);
        LogsCollection::getInstance()->addlog(strtolower($this->entityName), $item->getId(), 'add');
        DB::getInstance()->commit();

        $this->result['c'] = strtolower($this->entityName);
        $this->result['id'] = $item->getId();
        ActionController::addMsg('success', '', $this->conf['added_ok']);
        return $this->result;
    }

    function UpdateAction() {
        $item = new $this->entityName($this->params['id']);
        $this->checkValidate($item);
        if ($this->result['error']) {
            return $this->result;
        }
        DB::getInstance()->begin();
        $item->update($this->params);
        LogsCollection::getInstance()->addlog(strtolower($this->entityName), $item->getId(), 'update');
        DB::getInstance()->commit();
        $this->result['c'] = strtolower($this->entityName);
        $this->result['id'] = $item->getId();
        ActionController::addMsg('success', '', $this->conf['saved_ok']);
        return $this->result;
    }

    function DeleteAction() {
        $item = new $this->entityName($this->params['id']);
        //delete or set null relations
        $info = $item->getName(1);
        LogsCollection::getInstance()->addlog(strtolower($this->entityName), $item->getId(), 'delete', $this->admin->getId(), $info);
        return $this->delete();
    }



}

?>