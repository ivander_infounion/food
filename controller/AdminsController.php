<?php
/* (c) 2015 InfoUnion CMS v3.0, voloshanivsky@gmail.com */
class AdminsController extends CmsController {

  function __construct($params) {
    $this->entityName = 'Admin';
    $params['ctrl'] = strtolower(substr(get_class($this), 0, -10));
    if (!parent::__construct($params)) {
      return;
    }

  }

  function ListAction() {
    $entCol = $this->entityName.'Collection';
    $col = new $entCol();
    if ($this->admin->getLogin() == 'root') {
      $col = $col->getAllAdmins();
    } else {
      $col = $col->getAdmins();
    }

    $this->list_action($col, $fileTpl);
  }

  function NewAction() {
//    $this->view->assign('plugin_cropper', true);
    $this->view->assign('col_label', 4);
    $col = new LangCollection();
    $this->view->assign('langs', $col->getLangs());
    $act = Admin::$actives;
    $this->view->assign('acts', $act);
    $act = Admin::$skins;
    $this->view->assign('skins', $act);
    $this->new_action();
  }

  function EditAction() {
    $hideacc = false;
    if (!isset($this->params['id'])) {
      if (!$this->admin) {
        View::getInstance()->displayLoginForm();
        return false;
      }
      $item = $this->admin;
      $this->params['id'] = $item->getId();
      if ($item->getLogin() != 'root') {
        $hideacc = true;
      }
    } else {
      $item = new Admin($this->params['id']);
      if ($this->admin->getLogin() != 'root') {
        if ($item->getLogin() == 'root') {
          $this->displayError($this->conf['access_denied']);
          return;
        }
        if ($this->admin->getId() == $item->getId()) {
          $hideacc = true;
        }
      }
    }
    $this->view->assign('hideacc', $hideacc);
    $this->view->assign('col_label', 4);
    $col = new LangCollection();
    $this->view->assign('langs', $col->getLangs());
    $act = Admin::$actives;
    $this->view->assign('acts', $act);
    $act = Admin::$skins;
    $this->view->assign('skins', $act);
    $this->edit_action(null, $item->getName());
  }

}

class AdminsAjaxController extends CmsAjaxController {

  function __construct($params) {

    parent::__construct($params);
    $this->entityName = 'Admin';
    $this->colName = $this->entityName.'Collection';

  }
  function ActiveAction() {
      return $this->active();
  }

  function checkValidate($item = null) {

    $fileinfo = $this->getFileInfo('pict');

    if ($item) {
      if ($item->getId() == $this->admin->getId()) {// в будущем будут отличия редактирования своего и чужего пользователя
        $this->validateFields(array('name', 'login', 'email', 'lang'), $item);
      } else {
        $this->validateFields(array('name', 'login', 'email', 'lang'), $item);
      }
      if (!empty($fileinfo['ext'])) {
        $this->params['pict'] = $fileinfo['ext'];
      } else {
        unset($this->params['pict']);
      }
      $check = Admin::getByLogin($this->params['login']);
      if ($check) {
        if ($check->getId() != $item->getId()) {
          $this->setErrorStatus('login', $this->conf['exists_login']);
        }
      }
      $check = Admin::getByEmail($this->params['email']);
      if ($check) {
        if ($check->getId() != $item->getId()) {
          $this->setErrorStatus('email', $this->conf['exists_email']);
        }
      }
    } else {
      $this->validateFields(array('name', 'login', 'pass', 'email', 'lang'), $item);
      $this->params['pict'] = $fileinfo['ext'];
      $check = Admin::getByLogin($this->params['login']);
      if ($check) {
        $this->setErrorStatus('login', $this->conf['exists_login']);
      }
      $check = Admin::getByEmail($this->params['email']);
      if ($check) {
        $this->setErrorStatus('email', $this->conf['exists_email']);
      }
    }
    if (!empty($this->params['pass'])) {
      $this->params['pass'] = md5($this->params['pass']);
    }

    return $this->result;
  }

  function AddAction() {
    $this->checkValidate();
    if ($this->result['error']) {
      return $this->result;
    }

    $col = new $this->colName();
    DB::getInstance()->begin();
    $item = $col->add($this->params);
    if ($this->params['pict'] != '') {
      if ( !move_uploaded_file($_FILES['pict']['tmp_name'],$item->getRealPathPict())){
        ActionController::addMsg('error', '', $this->conf['upl_failed']);
        $item->setPict('');
      } else {
        $res_pict = Foto::img_cut($item->getRealPathPict(), $item->getRealPathPict(), 120, 120);
      }
    }
    LogsCollection::getInstance()->addlog(strtolower($this->entityName), $item->getId(), 'add');
    DB::getInstance()->commit();

    $this->result['c'] = 'admins';
    $this->result['id'] = $item->getId();
    ActionController::addMsg('success', '', $this->conf['added_ok']);
    return $this->result;

  }

  function DeleteAction() {
    $item = new $this->entityName($this->params['id']);
    if ($item->getLogin() == 'root') {
      $this->result['status-kind'] = 'error';
      $this->result['status-title'] = $this->conf['cant_del'];
      return $this->result;
    }
    return $this->delete($item);
  }

  function DeleteimgAction() {
    $item = new Admin($this->params['id']);
    if (file_exists($item->getRealPathPict())) {
      unlink($item->getRealPathPict());
    }
    $item->setPict('');
    $this->result['pictname'] = 'pict';//$this->params['pictname'];
    $this->result['status-kind'] = 'success';
    $this->result['status-title'] = $this->conf['deleted_ok'];
    return $this->result;
  }

  function MarkdelAction() {
    $item = new $this->entityName($this->params);
    return $this->markDelete($item);
  }

  function UpdateAction() {
    $item = new $this->entityName($this->params['id']);
    $this->checkValidate($item);
    if ($this->result['error']) {
      return $this->result;
    }
    $pictbef = $item->getPict();
    DB::getInstance()->begin();
    $item->update($this->params);
    if($this->params['active'] != 1 ) {
      $item->resetKey();
    }
    if ($this->params['pict'] != '') {
      if ( !move_uploaded_file($_FILES['pict']['tmp_name'],$item->getRealPathPict())){
        ActionController::addMsg('error', '', $this->conf['upl_failed']);
        $item->setPict($pictbef);
      } else {
        $res_pict = Foto::img_cut($item->getRealPathPict(), $item->getRealPathPict(), 120, 120);
      }
    }
    LogsCollection::getInstance()->addlog(strtolower($this->entityName), $item->getId(), 'update');
    DB::getInstance()->commit();
    $this->result['c'] = 'admins';
    $this->result['id'] = $item->getId();
    ActionController::addMsg('success', '', $this->conf['saved_ok']);
    return $this->result;

  }

}

?>