<?php
/* (c) 2015 InfoUnion CMS v3.0, voloshanivsky@gmail.com */
class ArticleController extends CmsController {

  protected $section;
  function __construct($params) {
    $this->entityName = 'Article';
    $params['ctrl'] = strtolower(substr(get_class($this), 0, -10));
    if (!parent::__construct($params)) {
      return;
    }
    if (isset($this->params['sid'])) {
      $this->section = new Section($this->params['sid']);
    } else {
      $this->displayError($this->conf['no_params'].' (section)');
      return;
    }
    $this->view->assign('section', $this->section);
    $this->view->assign('menu_content', true);
    $this->insertBreadcrumbs(array('url'=>FrontController::MakeUrl('section','list'),'title'=>$this->conf['sitemap']));
    $this->view->current_section = $this->section->getData();
    $this->view->ctrl['title'] = $this->section->getName($this->lang);
  }

  function ListAction() {
    $entCol = $this->entityName.'Collection';
    $col = new $entCol();
    $count = $col->getByParams(array('section'=>$this->section->getId()))->count();
    $pager = $this->createPager(30, $count);
    $col = new $entCol();
    $col = $col->getByParams(array('section'=>$this->section->getId()), "created DESC $pager");
    $this->list_action($col);
  }

  function NewAction() {
    $this->view->assign('datepick', array(0=>array('name'=>'created','time'=>true)));
    $this->view->assign('editor', true);
    $this->view->assign('tablang', true);
    $this->view->assign('langItems', array(1=>true));
    $this->new_action();
  }

  function EditAction() {
    $this->view->assign('datepick', array(0=>array('name'=>'created','time'=>true)));
    $this->view->assign('tablednd', 'file');
    $this->view->assign('editor', true);
    $this->view->assign('gallery', true);
    $this->view->assign('tablang', true);
    $item = new $this->entityName($this->params['id']);
    $this->view->assign('langItems', $item->langItems);
    $this->view->assign('pictures', $item->getFiles());
    $this->edit_action(null, $item->getName($this->lang));
  }

}

class ArticleAjaxController extends CmsAjaxController {

  function __construct($params) {

    parent::__construct($params);
    $this->entityName = 'Article';
    $this->colName = $this->entityName.'Collection';

  }

  function ActiveAction() {
    return $this->activeAll();
  }
  function ActivepictAction() {
    $this->entityName = $this->entityName.'file';
    $this->params = $this->params['id'];
    return $this->active();
  }
  function RemovepictAction() {
    $entname = $this->entityName.'file';
    $item = new $entname($this->params);
    $item->delete();
    $this->result['id'] = $this->params;
    $this->result['status-kind'] = 'success';
    $this->result['status-title'] = $this->conf['deleted_ok'];
    return $this->result;
  }

  function updatePictures($item, $pictbef, $types = array("")) {
    foreach($types as $type){
      if ($this->params['pict'.$type] != '') {
        if ( !move_uploaded_file($_FILES['pict'.$type]['tmp_name'],$item->getRealPathPict($type))){
          ActionController::addMsg('error', '', $this->conf['upl_failed']);
          $item->setPict($pictbef[$type], $type);
        } else {
          if($this->params['pictcrop']['pict'.$type]){
          //cutting like in plugin
          $res_pict = Foto::img_crop($item->getRealPathPict($type),
                                     $item->getRealPathPreview($type),
                                     $this->params['pictcrop']['pict'.$type]->x,
                                     $this->params['pictcrop']['pict'.$type]->y,
                                     $this->params['pictcrop']['pict'.$type]->width,
                                     $this->params['pictcrop']['pict'.$type]->height,
                                     $item->getPreviewSize('w','pict'.$type),
                                     $item->getPreviewSize('h','pict'.$type));
          // resizing to static size from model
          $res_pict = Foto::img_resize($item->getRealPathPreview($type),$item->getRealPathPreview($type),$item->getPreviewSize('w','pict'.$type),$item->getPreviewSize('h','pict'.$type));
          //watermark
          Foto::create_water($item->getRealPathPreview($type), 'img/watermark.png', 'center');
          }else{
              $res_pict = Foto::img_cut($item->getRealPathPict($type), $item->getRealPathPreview($type), $item->getPreviewSize('w','pict'.$type), $item->getPreviewSize('h','pict'.$type));
              Foto::create_water($item->getRealPathPreview($type), 'img/watermark.png', 'center');
          }
        }
      }else{
        if($this->params['pictcrop']['pict'.$type]){
          //cutting like in plugin
          $res_pict = Foto::img_crop($item->getRealPathPict($type),
                                     $item->getRealPathPreview($type),
                                     $this->params['pictcrop']['pict'.$type]->x,
                                     $this->params['pictcrop']['pict'.$type]->y,
                                     $this->params['pictcrop']['pict'.$type]->width,
                                     $this->params['pictcrop']['pict'.$type]->height,
                                     $item->getPreviewSize('w','pict'.$type),
                                     $item->getPreviewSize('h','pict'.$type));
          // resizing to static size from model
          $res_pict = Foto::img_resize($item->getRealPathPreview($type),$item->getRealPathPreview($type),$item->getPreviewSize('w','pict'.$type),$item->getPreviewSize('h','pict'.$type));

          Foto::create_water($item->getRealPathPreview($type), 'img/watermark.png', 'center');
        }
      }
    }
  }

  function checkValidate($item = null) {

    $types = array('');
    foreach($types as $type){
      if(!empty($this->params['pict'.$type.'_data'])){
        $this->params['pictcrop']['pict'.$type] = json_decode(str_replace("'", '"', $this->params['pict'.$type.'_data']));
      }
      $fileinfo = $this->getFileInfo('pict'.$type);
      if ($item) {
        if (!empty($fileinfo['ext'])) {
          $this->params['pict'.$type] = $fileinfo['ext'];
        } else {
          unset($this->params['pict'.$type]);
        }
      } else {
        $this->params['pict'.$type] = $fileinfo['ext'];
      }
    }
    $require = array('created');
    foreach ($this->params['lang'] as $l) {
      $require[] = 'name'.$l;
      $require[] = 'url'.$l;
    }
    $this->validateFields($require, $item);
    $this->params['created'] = date("Y-m-d H:i", strtotime($this->params['created']));
    return $this->result;

  }

  function AddAction() {
    $this->checkValidate();
    if ($this->result['error']) {
      return $this->result;
    }
    $types = array("");
    foreach ($types as $key => $value) {
      $pictbef[$value] = "";
    }

    $col = new $this->colName();
    DB::getInstance()->begin();
    $item = $col->add($this->params);

    $this->updatePictures($item, $pictbef, $types);
    LogsCollection::getInstance()->addlog(strtolower($this->entityName), $item->getId(), 'add');
    $this->updateGallery($item);
    DB::getInstance()->commit();

    $this->result['c'] = strtolower($this->entityName);
    $this->result['id'] = $item->getId();
    $this->result['sid'] = $item->getSectionId();
    ActionController::addMsg('success', '', $this->conf['added_ok']);
    return $this->result;

  }

  function DeleteAction() {
    $item = new $this->entityName($this->params['id']);
    //delete or set null relations
    $info = $item->getName(1);
    LogsCollection::getInstance()->addlog(strtolower($this->entityName), $item->getId(), 'delete', $this->admin->getId(), $info);
    return $this->delete();
  }
  function DeleteimgAction() {
    $item = new $this->entityName($this->params['id']);
    $this->result['pictname'] = $this->params['pictname'];
    $this->params['pictname'] = substr($this->params['pictname'],4);
    if (file_exists($item->getRealPathPict($this->params['pictname']))) {
      unlink($item->getRealPathPict($this->params['pictname']));
    }
    $item->setPict('', $this->params['pictname']);
    $this->result['status-kind'] = 'success';
    $this->result['status-title'] = $this->conf['deleted_ok'];
    return $this->result;
  }

  function UpdateAction() {
    $item = new $this->entityName($this->params['id']);
    $this->checkValidate($item);

    

    if ($this->result['error']) {
      return $this->result;
    }

    foreach ($types as $key => $value) {
      $pictbef[$value] = $item->getPict($value);  
    }

    DB::getInstance()->begin();
    $item->update($this->params);
    $types = array("");
    $this->updatePictures($item, $pictbef, $types);
    LogsCollection::getInstance()->addlog(strtolower($this->entityName), $item->getId(), 'update');
    $this->updateGallery($item);
    DB::getInstance()->commit();
    $this->result['c'] = strtolower($this->entityName);
    $this->result['id'] = $item->getId();
    ActionController::addMsg('success', '', $this->conf['saved_ok']);
    return $this->result;

  }

  function ReorderfileAction() {
    $this->entityName = 'Articlefile';
    return $this->reorderList();
  }
}

?>