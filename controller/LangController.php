<?php
/* (c) 2015 InfoUnion CMS v3.0, voloshanivsky@gmail.com */
class LangController extends CmsController {
  function __construct($params) {
    $this->entityName = 'Lang';
    $params['ctrl'] = strtolower(substr(get_class($this), 0, -10));
    if (!parent::__construct($params)) {
      return;
    }
  }

  function ListAction() {
    $this->view->assign('tablednd', 'list');
    $entCol = $this->entityName.'Collection';
    $col = new $entCol();
    $col = $col->getByParams(array(), "order");
    $this->list_action($col);
  }

  function NewAction() {
    $this->view->assign('tablang', true);
    $this->new_action();
  }  

  function EditAction() {
    $this->view->assign('tablang', true);
    $item = new $this->entityName($this->params['id']);
    $this->view->assign('langItems', $item->names);
    $this->edit_action(null, $item->getName($this->lang));
  }

}

class LangAjaxController extends CmsAjaxController {

  function __construct($params) {

    parent::__construct($params);
    $this->entityName = 'Lang';
    $this->colName = $this->entityName.'Collection';

  }

  function ActiveAction() {
    $this->entityName = substr($this->params['ctrl'],0,-7).'Haslang';
    $this->params = $this->params['id'];
    return $this->active();
  }

  function ActivelangAction() {
    return $this->active();
  }

  function checkValidate($item = null) {

    $require = array('sname');
    foreach ($this->params['lang'] as $l) {
      $require[] = 'name'.$l;
      $require[] = 'shortname'.$l;
    }
    $this->validateFields($require, $item);
    return $this->result;

  }

  function AddAction() {
    $this->checkValidate();
    if ($this->result['error']) {
      return $this->result;
    }

    $col = new $this->colName();
    DB::getInstance()->begin();
    $item = $col->add($this->params);
    LogsCollection::getInstance()->addlog(strtolower($this->entityName), $item->getId(), 'add');
    DB::getInstance()->commit();

    $this->result['c'] = strtolower($this->entityName);
    $this->result['id'] = $item->getId();
    ActionController::addMsg('success', '', $this->conf['added_ok']);
    return $this->result;

  }

  function UpdateAction() {
    $item = new $this->entityName($this->params['id']);
    $this->checkValidate($item);
    if ($this->result['error']) {
      return $this->result;
    }

    DB::getInstance()->begin();
    $item->update($this->params);
    LogsCollection::getInstance()->addlog(strtolower($this->entityName), $item->getId(), 'update');
    DB::getInstance()->commit();
    $this->result['c'] = strtolower($this->entityName);
    $this->result['id'] = $item->getId();
    ActionController::addMsg('success', '', $this->conf['saved_ok']);
    return $this->result;

  }

  function ReorderlistAction() {
    $pageOrder = 2;
    DB::getInstance()->begin();
    foreach ( $this->params as $pageId ) {
      $page = new $this->entityName($pageId);
      $page->setOrder($pageOrder++);
    }
    $lang_main = new Lang(1);
    $lang_main->setOrder(1);
    DB::getInstance()->commit();
    return true;
  }
}

?>