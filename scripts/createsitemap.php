<?php

  header('Content-type: text/html; charset=UTF-8');
  require '../autoload.php';
  $db = DB::getInstance();
  $dom = new DOMDocument("1.0","UTF-8");
  $root = $dom->createElement("urlset");
  $root->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
  $dom->appendChild($root);
  $def = Settings::get('defaulturl');

  $content = "<!DOCTYPE html>
<html>
<head>
  <title>Sitemap</title>
</head>
<body>
";

  $col = new UrlCollection();
  $col = $col->getCustomIterator("url_active=1");
  foreach ($col as $item) {
    $url = $dom->createElement("url");
    $langid = $item->getLang();
    $lang = new Lang($langid);
    $def = Settings::get('defaulturl');
    if ($langid != 1) {
      $def .= $lang->getSname().'/';
    }
    if ($item->getName() == 'index') {
      $link = $def;
      $priority = 1;
    } else {
      $ctrl = $item->getController();
      if (in_array($ctrl, array('section'))) {
        $priority = 0.7;
      } elseif (in_array($ctrl, array('seopage','category'))) {
        $priority = 0.7;
      } elseif (in_array($ctrl, array('goods'))) {
        $priority = 0.6;
      } elseif (in_array($ctrl, array('article'))) {
        $priority = 0.5;
      } else {
        $priority = 0;
        showLabelTime("end iteration ".$i." (skip ctrl)", $start);
        continue;
      }      
      $link = $def . $item->getName();
    }

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $link,
        CURLOPT_HEADER => true,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_NOBODY => true
    ));
    curl_exec($curl);
    $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    curl_close($curl);
    if ($code == 200) {    

      $page = $item->getPage();
      $loc = $dom->createElement("loc", $link . ".html");
      $content .= '<a href="' . $link .'.html">'.$page->getName().'</a><br/>';

      $url->appendChild($loc);
      if ($priority) {
        $P = $dom->createElement("priority",$priority);
        $url->appendChild($P);
      }      
      $root->appendChild($url);
    }
  }

  $dom->save("sitemap.xml");
// create sitemap.html
  $content .= "</body>
</html>";

    $filename = 'sitemap.html';
    if (!$file = fopen($filename, 'a')) {
      echo 'Не найден файл (sitemap.html)';
      exit;
    }
    ftruncate($file, 0);
    if (fwrite($file, stripslashes($content)) === false) {
      echo 'Ошибка записи файла (sitemap.html)';
      exit;
    }
    fclose($file);
    echo "Файл sitemap.html успешно создан <br>";

  echo 'File sitemap.xml updated';

?>