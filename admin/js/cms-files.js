function openImage(input) {
  var file = input.files[0];
  var fileReader = new FileReader();
  if (/^image\/\w+$/.test(file.type)) {
      fileReader.readAsDataURL(file);
      fileReader.onload = function () {
          var dataURL = fileReader.result;
          var img = $("#img-"+input.name);
          img[0].src = dataURL;
          img.removeClass('hide');
          var btn = $("#btn-clear-img-"+input.name);
          btn.removeClass('hide');
      };
  } else {
      alert("Please choose an image file.");
  }
}
function clearImage(name) {
  var img = $("#img-"+name);
  img.attr('src', '');
  img.addClass('hide');
  var input = $("#"+name);
  input.val('');
  var btn = $("#btn-clear-img-"+name);
  btn.addClass('hide');
  closeConfirm();
}
// **** GALLERY ****
function openImages(input, i, name) {
  var files = input.files;

  var blockImg = $("#block-new-"+name);
  var file = input.files[i];
  var fileReader = new FileReader();
  if (/^image\/\w+$/.test(file.type)) {

      fileReader.readAsDataURL(file);
      fileReader.onload = function () {

          var dataURL = fileReader.result;
          var img = $("<img/>", {
            'src': dataURL,
            'style': 'width:100px',
            'class': 'p-sm',
            'id': name+'new-'+i
          });
          blockImg.append(img);
          var btn = $("<a/>",{
            'class': 'btn btn-outline btn-success btn-xs pull-top m-l-xs',
            'id': 'btn-remove-'+name+'new-'+i,
            'onclick': "clearPict('"+name+"', '"+i+"')"
          });
          var icon = $("<i/>", {
            'class': 'fa fa-remove'
          });
          btn.append(icon);
          blockImg.append(btn);
          i++;
          if (i<files.length) {
            openImages(input, i, name);
          }
      };
  } else {
      alert("Please choose an image file.");
  }
}
function clearPict(name, i) {
  $("#btn-remove-"+name+"new-"+i).remove();
  $("#"+name+"new-"+i).remove();
}
// *** EDITOR ****
function sendFile(file, editor, welEditable) {
    data = new FormData();
    data.append("file", file);
    $.ajax({
        data: data,
        type: "POST",
        url: "/upload.php",
        cache: false,
        contentType: false,
        processData: false,
        scriptCharset: 'UTF-8',
        success: function(url) {
//          alert(url);
            editor.insertImage(welEditable, url);
        }
    });
}