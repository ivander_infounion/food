$(document).ready(function(){
  $("*[rel='tooltip']").tooltip();
  $('.i-checks').iCheck({
      checkboxClass: 'icheckbox_square-green',
      radioClass: 'iradio_square-green',
  });
  setTimeout(function(){$('.modal-body').hide();},500);

  
});

function defUrl() {
  return '/admin/';
//  return '/sunny/admin/';
}
function ShowLoad() {
  if ($('#boot_button').length > 0) {
    $('#boot_button').button('loading');
  }
}
function HideLoad() {
  if ($('#boot_button').length > 0) {
    $('#boot_button').button('reset');
  }
}
function showConfirm(c,a,params,msg) {
  if (c) {
    params = JSON.stringify(params);
    var func = "ajax_query('"+c+"','"+a+"',"+params+")";
  } else {
    var func = a+"('"+params+"')";
  }
  $("#modal-confirm .modal-header > h3").html(msg);
  $("#modal-confirm #btn-modal-confirm").attr('onclick',func);
  $("#modal-confirm").modal('show');
}
function closeConfirm() {
  $("#modal-confirm").modal('hide');
}
function ajax_query(c,a,data) {
  var query = new Array;
  ShowLoad();
  var editors = $(".summernote");
  for (i=0; i<editors.length; i++) {
    $("textarea[name='"+editors[i].id+"']").html($(".summernote").eq(i).code());
  }

  query['c'] = c;
  query['a'] = a;
  query['d'] = data;

  name_ajax = '/ajax.php';
//  name_ajax = '/sunny/ajax.php';

  JsHttpRequest.query(name_ajax,query,function(result, errors) {

      debug_layer = $('#debug');
      if (debug_layer && (errors || !result)) {

          var err = new Array;
          err['status-title'] = 'Error';
          err['status-msg'] = errors;
          err['status-kind'] = 'error';
          show_status(err);
          debug_layer.show();
          debug_layer.html("Error <br/><pre>"+errors+"</pre><i class='fa fa-remove pull-right cursor-pointer' onclick=" +'$("#debug").hide()'+"></i>");

      }
      if (typeof window['ajax_cb_'+result['a']] == 'function') {
        eval('ajax_cb_'+result['a']+'(result["r"])');
      } else {
        eval('ajax_cb_'+result['c']+'_'+result['a']+'(result["r"])');
      }

      HideLoad();
    },
    true
  );
}

function form2array(theForm) {
  var type;
  var arr = new Array;
  for(i=0; i<theForm.elements.length; i++){
    type = theForm.elements[i].type;
    if(type == "text" || type == "password" || type == "hidden" || type == "textarea" /*|| type == "button"*/ || type == "select-one"){
      arr[theForm.elements[i].name] = theForm.elements[i].value
    } else if(type == "checkbox" || type == "radio"){
      if ( theForm.elements[i].checked ) {
        arr[theForm.elements[i].name] = theForm.elements[i].value;
      } else {
        arr[theForm.elements[i].name] = 0;
      }
    } else if(type == "file") {
      arr[theForm.elements[i].name] = theForm.elements[i].files;
    }
  }
  return arr;
}

function ajax_callback(c,a,result) {
  alert('Recieved: ' + result);
}
// CMS v.3.0
function cancelForm() {
  window.location.reload();
}
function make_url(result) {
  res = defUrl() + result['c'] + '/' + result['a'] + '/';
  if (result['id']) {
    res += result['id'] + '/';
  }
  params = '';
  if (result['sid']) {
    if (params != '') {
      params += '&';
    }
    params += 'sid=' + result['sid'];
  }
  if (result['key']) {
    if (params != '') {
      params += '&';
    }
    params += 'key=' + result['key'];
  }
  if (params != '') {
    params = '?' + params;
  }
  return res + params;
}
function show_status(result) {
  toastr[result['status-kind']](result['status-msg'], result['status-title']);
}
function show_fields_error(result) {
  show_status(result);
  items = result['items'];
  for (i=0; i<items.length; i++) {
    item = items[i];
    if (result[item]['error'] == false) {
      $('#group-' + item).removeClass('has-error');
    } else {
      $('#group-' + item).addClass('has-'+result[item]['error']);//error,warning,success
    }
    $('#help-' + item).html(result[item]['status']);
  }
}
function delete_row(id) {
  $("#"+id).empty();
}

//******** AJAX CB **************
//------- item --------------------
function ajax_cb_active(result) {
  var active = $("#active-"+result['id']);
  var icon = $("#acticon-"+result['id']);
  var icon_big = $("#acticon-big-"+result['id']);
  var acttxt = $("#act-text-"+result['id']);
  if (result['active'] == 1) {
    active.attr('title', result['stop_public']);
    acttxt.html(result['stop_public']);
    icon.addClass('fa-eye');
    icon.removeClass('fa-eye-slash');
    icon_big.addClass('fa-check');
    icon_big.removeClass('fa-upload');
//    active.removeClass('text-danger');
  } else {
    active.attr('title', result['publish']);
    acttxt.html(result['publish']);
    icon.removeClass('fa-eye');
    icon.addClass('fa-eye-slash');
    icon_big.removeClass('fa-check');
    icon_big.addClass('fa-upload');
//    active.addClass('text-danger');
  }
}
function ajax_cb_add(result) {
  if (typeof result['debug'] !== "undefined"){
    console.log(result['debug']);
  }
  if (result['error'] == true) {
    show_fields_error(result);
  } else {
    if (typeof result['debug'] === "undefined"){
      result['a'] = 'edit';
      location.href = make_url(result);
    }
  }
}
function ajax_cb_delete(result) {
  closeConfirm();
  if (result['error'] == true) {
    show_status(result);
  } else {
    show_status(result);
    delete_row(result['c']+'_'+result['id']);
  }
}
function ajax_cb_update(result) {
  if (typeof result['debug'] !== "undefined"){
    console.log(result['debug']);
  }
  if (result['error'] == true) {
    show_fields_error(result);
  } else {
    if (typeof result['debug'] === "undefined"){
      window.location.reload();
    }
  }
}
function ajax_cb_reordertree(result) {
//  alert(result);
}
//******* FILE, IMAGE ****
function ajax_cb_deleteimg(result) {
  if (result['error'] == true) {
    show_status(result);
  } else {
    clearImage(result['pictname']);
  }
}
function ajax_cb_removepict(result) {
  closeConfirm();
  if (result['error'] == true) {
    show_status(result);
  } else {
//    $("#group-pict-"+result['id']).remove();
    window.location.reload();
//    clearImage(result['pictname']);
  }
}
function ajax_cb_removeban(result) {
  closeConfirm();
  if (result['error'] == true) {
    show_status(result);
  } else {
    $("#group-ban-"+result['id']).remove();
  }
}
function activePict(ctrl, id) {
  var p = new Array;
  p['id'] = id;
  ajax_query(ctrl, 'activepict', p);
}
function ajax_cb_activepict(result) {
  var active = $("#active-pict-"+result['id']);
  var icon = $("#acticon-pict-"+result['id']);
  if (result['active'] == 1) {
    active.attr('title', result['stop_public']);
    icon.addClass('fa-eye');
    icon.removeClass('fa-eye-slash');
  } else {
    active.attr('title', result['publish']);
    icon.removeClass('fa-eye');
    icon.addClass('fa-eye-slash');
  }
}
function ajax_cb_activeban(result) {
  var active = $("#active-ban-"+result['id']);
  var icon = $("#acticon-ban-"+result['id']);
  if (result['active'] == 1) {
    active.attr('title', result['stop_public']);
    icon.addClass('fa-eye');
    icon.removeClass('fa-eye-slash');
  } else {
    active.attr('title', result['publish']);
    icon.removeClass('fa-eye');
    icon.addClass('fa-eye-slash');
  }
}
//******* ADMIN **********
function ajax_cb_admin_login(result){
  if (result['error'] == false) {
    window.location.reload();
//    location.href = defUrl() + 'cms/mainpage/';
  } else {
    show_fields_error(result);
  }
}
function ajax_cb_cms_changelm(result) {
//change left menu
}

function ajax_cb_reorder(result){
//  alert(result);
}
function ajax_cb_reorderfile(result){
//  alert(result);
}
function ajax_cb_reorderlist(result){
//  alert(result);
}
//-------- lang -------------------
function ajax_cb_lang_active(result) {
  ajax_cb_active(result);
}
function ajax_cb_lang_activelang(result) {
  ajax_cb_active(result);
}
//-------- filter -----------------
function filterReload() {
  ajax_query('cms', 'filterreload', form2array(document.forms['form_filter']));
}
function ajax_cb_cms_filterreload(result) {
  window.location.reload();
}
function fillSelectList(name_list, value_list) {
  var list = $("#"+name_list);
  list.empty();
  for (i=0; i<value_list.length; i++) {
    opt = $("<option/>", {
      value: value_list[i]['id'],
      html: value_list[i]['name']
    })
    list.append(opt);
  }
}
//--------- seopage ----------
function ajax_cb_seopage_changefeature(result) {
  $(".feature_add > div").hide();
  $("#feat_add_select").empty();
  if (result['kind'] == '') {
    // $("#")
  } else {
    $("#feat_add_btn").show();
    if (result['kind'] == 'numeric') {
      $("#feat_add_numeric1").show();
      $("#feat_add_numeric2").show();
    } else {
      $("#feat_add_list").show();
      fillSelectList("feat_add_select",result['list']);
    }
  }
}
function ajax_cb_seopage_addfeature(result) {
  ajax_cb_update(result);
}
function ajax_cb_seopage_deletefeature(result) {
  closeConfirm();
  delete_row("group-seofeat"+result);
}
//----------- example reload chosen select ----
function ajax_cb_gallery_selectcat(result) {
  fillSelectList('cloth', result['list']);
  $("#cloth").chosen('destroy');
  $("#cloth").chosen(config[".chosen-select"]);
}

function ajax_cb_cover_addseries(result) {
  if (typeof result['debug'] !== "undefined"){
    console.log(result['debug']);
  }else{
    if (result['error'] == true) {
      show_fields_error(result);
    } else {
      result['a'] = 'seriaedit';
      location.href = make_url(result);
    }
  }
  
}
function ajax_cb_cover_updseries(result) {
  ajax_cb_update(result);
}
function ajax_cb_cover_deleteseries(result) {
  closeConfirm();
  if (result['error'] == true) {
    show_status(result);
  } else {
    window.location.reload();
  }
}
function ajax_cb_cover_activeseries(result) {
  var active = $("#active-series-"+result['id']);
  var icon = $("#acticon-series-"+result['id']);
  var acttxt = $("#act-text-"+result['id']);
  if (result['active'] == 1) {
    active.attr('title', result['stop_public']);
    acttxt.html(result['stop_public']);
    icon.addClass('fa-eye');
    icon.removeClass('fa-eye-slash');
  } else {
    active.attr('title', result['publish']);
    acttxt.html(result['publish']);
    icon.removeClass('fa-eye');
    icon.addClass('fa-eye-slash');
  }
}