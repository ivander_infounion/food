function defUrl() {
  return '/admin/';
//  return '/sunny/admin/';
}
function ajax_query(c,a,data) {
  var query = new Array;
  query['c'] = c;
  query['a'] = a;
  query['d'] = data;

  name_ajax = '/ajax.php';
//  name_ajax = '/sunny/ajax.php';

  JsHttpRequest.query(name_ajax,query,function(result, errors) {

      debug_layer = $('#debug');
      if (debug_layer && (errors || !result)) {

          debug_layer.show();
          debug_layer.html("Error <br/><pre>"+errors+"</pre>");

      }
      if (typeof window['ajax_cb_'+result['a']] == 'function') {
        eval('ajax_cb_'+result['a']+'(result["r"])');
      } else {
        eval('ajax_cb_'+result['c']+'_'+result['a']+'(result["r"])');
      }

    },
    true
  );
}

function form2array(theForm) {
  var type;
  var arr = new Array;
  for(i=0; i<theForm.elements.length; i++){
    type = theForm.elements[i].type;
    if(type == "text" || type == "password" || type == "hidden" || type == "textarea" /*|| type == "button"*/ || type == "select-one"){
      arr[theForm.elements[i].name] = theForm.elements[i].value
    } else if(type == "checkbox" || type == "radio"){
      if ( theForm.elements[i].checked ) {
        arr[theForm.elements[i].name] = theForm.elements[i].value;
      } else {
        arr[theForm.elements[i].name] = 0;
      }
    } else if(type == "file") {
      arr[theForm.elements[i].name] = theForm.elements[i].files;
    }
  }
  return arr;
}


function show_fields_error(result) {
  items = result['items'];
  for (i=0; i<items.length; i++) {
    item = items[i];
    if (result[item]['error'] == false) {
      $('#group-' + item).removeClass('has-error');
    } else {
      $('#group-' + item).addClass('has-'+result[item]['error']);//error,warning,success
    }
    $('#help-' + item).html(result[item]['status']);
  }
}
//******* ADMIN **********
function ajax_cb_admin_login(result){
  if (result['error'] == false) {
    window.location.reload();
//    location.href = defUrl() + 'cms/mainpage/';
  } else {
    show_fields_error(result);
  }
}
