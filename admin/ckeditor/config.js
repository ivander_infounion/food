/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	
	// disable deleting classes on elements
	config.allowedContent = true;

	//disable delating html tags that empty inside, <i class="hello"></i> as example
	config.protectedSource.push(/<i[^>]*><\/i>/g);

};
