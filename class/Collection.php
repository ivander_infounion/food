<?php
/* (c) 2007-2009 Denis Shushkin, me@den.kiev.ua */

abstract class Collection extends Model implements IteratorAggregate {
  private $dumptable;
  private $insertDefault = array();
  private $where = '1';
  private $groupBy = '';
  public $params = array();

  /**
  * Constructor for entity collection
  *
  * @param Entity|array List of parent entities to use for filtering
  * @return Collection
  */
  public function __construct($parent = NULL) {
    parent::__construct();
    $this->dumptable = "`{$this->table->getName()}`";
    if ( is_array($parent) ) {
      foreach ( $parent as $entity ) {
        $this->assignParentEntity($entity);
      }
    } elseif ( $parent ) {
      $this->assignParentEntity($parent);
    }
//    print_r('con-'.$this->dumptable.' = ');
  }

  public function addGroupBy($groupBy = null) {
    if ($groupBy) {
      return $this->groupBy = " GROUP BY $groupBy";
    } else {
      return $this->groupBy = " GROUP BY {$this->Name()}.{$this->Name()}_id";
    }
  }

  // Required definition of interface IteratorAggregate
  public function getIterator() {
    return new RowIterator("SELECT * FROM {$this->dumptable} WHERE {$this->where} {$this->groupBy} ORDER BY {$this->table->getPk()}", new RowFetchEntity($this));
  }

  public function getArrayIds(array $mtm) {
    $from = $this->dumptable;
    $where = '1';
    $manyToMany = $this->table->getManyToMany();
    foreach ($mtm as $ent => $value) {
      $assocTab = strtolower($manyToMany[$ent]);
      $from .= " NATURAL JOIN $assocTab";
      $where .= " AND {$ent}_id='$value'";
    }
    $iteration = new RowIterator("SELECT {$this->Name()}_id FROM $from WHERE $where GROUP BY {$this->table->getPk()} ORDER BY {$this->table->getPk()}", new RowFetchValue());
    return $iteration->toArray();
  }

  /**
  * Assign parent entity for filtering
  * @param Entity Parent entity
  */
  protected function assignParentEntity(Entity $entity) {
    if ( is_null($entity) ) {
      return;
    }

    $manyToMany = $this->table->getManyToMany();
//    echo get_class($this->table);
//    print_r($manyToMany);
    $key = $entity->table->getPk();
    $value = $entity->getId();

    if ( isset($manyToMany[$entity->table->getName()]) ) {
      $assocTable = Table::getByEntityName($manyToMany[$entity->table->getName()]);
      $this->addJoin($assocTable->getName());
    } else {
      $manyToOne = $this->table->getManyToOne();
      if ( !isset($manyToOne[$entity->table->getName()]) ) {
        throw new EntityException('Incorrect parent entity supplied');
      }
      $this->addInsertDefault($key, $value);
    }

    $this->addFilter("$key='$value'");
  }

  protected function addInsertDefault($key, $value) {
    $this->insertDefault[$key] = $value;
  }

  protected function addFilter($condition) {
    $this->where .= " AND $condition";
  }

  protected function addFilterByParams() {
    if (count($this->params)) {
      $manyToOne = $this->table->getManyToOne();
      $fields = $this->table->getNameFields();
      foreach ($this->params as $field => $value) {
        if (is_array($value)) {
          $value = implode("','", $value);
          $condition = "IN ('$value')";
        } else {
          $condition = "= '$value'";
        }
        if ( in_array($field, $manyToOne) ) {
          $this->addFilter("{$field}_id $condition");
        } elseif (in_array($field, $fields)) {
          $this->addFilter("{$this->Name()}_$field $condition");
        }
      }
    }
  }

  public function getByParams(array $params = array(), $order = null) {
    if (count($params)) {
      $this->params = array_merge($this->params, $params);
    }
    $this->addFilterByParams();
    return $this->getCustomIterator("", $order);
  }

  function getFetchAssoc($select, $where = '', $params = array(), $orderBy = NULL) {
    
    if ( $where ) {
      $where = " AND $where";
    }
    
    if (count($params)) {
      $this->params = array_merge($this->params, $params);
    }
    $this->addFilterByParams();

    $orderBy = $this->getOrderBy($orderBy);
    
    $values = new RowIterator("SELECT $select FROM {$this->dumptable} WHERE {$this->where} $where {$this->groupBy} ORDER BY $orderBy", new RowFetchAssoc());
    return $values->toArray();
  }  

  function getFetchValues($select, $where = '', $params = array(), $orderBy = NULL) {
    
    if ( $where ) {
      $where = " AND $where";
    }
    
    if (count($params)) {
      $this->params = array_merge($this->params, $params);
    }
    $this->addFilterByParams();
    
    $orderBy = $this->getOrderBy($orderBy);
     
    $values = new RowIterator("SELECT $select FROM {$this->dumptable} WHERE {$this->where} $where {$this->groupBy} ORDER BY $orderBy", new RowFetchValue());
    return $values->toArray();
  }

  protected function addJoin($tableName, $join = " NATURAL JOIN ", $condition = "") {
    return $this->dumptable .= $join . "`$tableName`" . $condition;
  }
  /**
  * Add new entity to the collection
  */
  function add(array $data = array()) {
    $insertData=array();
    foreach ($this->table->getFields() as $field) {
      $len = strlen($this->name);
      $key_data = substr($field,$len+1);
      if (isset($data[$key_data])) {
        $insertData[$field] = $data[$key_data];
      } else {

      }
    }
    foreach ( $this->table->getManyToOne() as $tableName ) {
      $field = $tableName . '_id';
      if (isset($data[$tableName])) {
        $insertData[$field] = $data[$tableName];
      }
    }
    $insertData = array_merge($insertData, $this->insertDefault);

    $addItem = $this->get($this->table->insert($insertData));
    // many to many
    foreach ($this->table->getManyToMany() as $ent => $assocTab) {
      if (isset($data[$ent])) {
        $insertDataMany = $data[$ent]; // reason[]
        $entColMany = $assocTab . "Collection";
//        require_once "model/$assocTab.php";
        $colMany = new $entColMany();
        foreach ($insertDataMany as $key => $value) {
          $colMany->add(array($this->Name() => $addItem->getId(), $ent => $value));
        }
      }
    }
    return $addItem;
  }

  function count() {
    return $this->getIterator()->count();
//    return $this->db->fetchSingle("SELECT COUNT(*) FROM {$this->dumptable} WHERE {$this->where}");
  }

  /**
  * Returns entity by ID
  * @return Entity
  */
  function get($id) {
    $name = $this->name;
    return new $name($id);
  }

  function getOrderBy($orderBy = NULL) {
    if ( $orderBy ) {
      $fields = $this->table->getNameFields();
      $mto = $this->table->getManyToOne();
      $pos_z = strpos($orderBy, ",");
      $pos_pr = strpos($orderBy, " ");
      if (($pos_z !== false) && ($pos_pr !== false)) {
        $pos_z = min($pos_pr, $pos_z);
        $order_field = substr($orderBy,0,$pos_z);
        $follow_order = substr($orderBy,$pos_z);
      } elseif ($pos_z !== false) {
        $order_field = substr($orderBy,0,$pos_z);
        $follow_order = substr($orderBy,$pos_z);
      } elseif ($pos_pr !== false) {
        $order_field = substr($orderBy,0,$pos_pr);
        $follow_order = substr($orderBy,$pos_pr);
      } else {
        $order_field = $orderBy;
        $follow_order = '';
      }
      if (in_array($order_field, $fields) || (in_array($order_field, $mto))) {
        $orderBy = $this->table->getRealFieldName($order_field) . $follow_order;
      } else {
//        $orderBy = $order_field . $follow_order;
      }
    } else {
      $orderBy = $this->table->getPk();
    }
    return $orderBy;
  }

  function getCustomIterator($where = '', $orderBy = NULL, IRowProcessStrategy $processStrategy = NULL) {
    if ( $orderBy ) {
      $fields = $this->table->getNameFields();
      $mto = $this->table->getManyToOne();
      $pos_z = strpos($orderBy, ",");
      $pos_pr = strpos($orderBy, " ");
      if (($pos_z !== false) && ($pos_pr !== false)) {
        $pos_z = min($pos_pr, $pos_z);
        $order_field = substr($orderBy,0,$pos_z);
        $follow_order = substr($orderBy,$pos_z);
      } elseif ($pos_z !== false) {
        $order_field = substr($orderBy,0,$pos_z);
        $follow_order = substr($orderBy,$pos_z);
      } elseif ($pos_pr !== false) {
        $order_field = substr($orderBy,0,$pos_pr);
        $follow_order = substr($orderBy,$pos_pr);
      } else {
        $order_field = $orderBy;
        $follow_order = '';
      }
      if (in_array($order_field, $fields) || (in_array($order_field, $mto))) {
        $orderBy = $this->table->getRealFieldName($order_field) . $follow_order;
      } else {
//        $orderBy = $order_field . $follow_order;
      }
    } else {
      $orderBy = $this->table->getPk();
    }
    if ( $where ) {
      $where = " AND $where";
    }
//    print_r('iter-'.$this->dumptable.' = ');
    return new RowIterator("SELECT {$this->Name()}.* FROM {$this->dumptable} WHERE {$this->where} $where {$this->groupBy} ORDER BY $orderBy", new RowFetchEntity($this), $processStrategy);
  }

  /**
  * @desc Delete entity from collection
  */
  function delete($id) {
    $id = intval($id);
    $this->table->delete("{$this->table->getPk()}='$id' AND {$this->where}");
  }

  function deleteByQuery($query, $ids = null) {
    $this->table->delete("{$query}");
  }

  /**
  * @desc Validates data before insertion
  */
  //abstract protected function ValidateInsert(array $data);
}

abstract class Collectionwithlang extends Collection {
//** voloshanivsky@gmail.com Multilang  *****
// дополнительные методы для коллекций с полями на нескольких языках
  private $tableLang;
  private $entColLang;

  public function __construct($parent = NULL) {
    parent::__construct($parent);
    $this->tableLang = strtolower($this->Name()) . 'haslang';
    $this->entColLang = strtolower($this->Name()) . 'HaslangCollection';
  }

  function add($data) {
    $item = parent::add($data);
    $colLang = new $this->entColLang();
    $fields = $colLang->table->getNameFields();
    $list_langs = new LangCollection();
    $list_langs = $list_langs->getFetchValues("lang_id");
    $dataUpdate = [];
    foreach ($list_langs as $lang) {
      $dataUpdate[$lang] = [];
      foreach ($fields as $field) {
        if (array_key_exists($field.$lang, $data)) {
          $dataUpdate[$lang][$field] = $data[$field.$lang];
        }
      }
    }
    $langItems = $item->getLangItems();
    foreach ($langItems as $langItem) {
      if (count($dataUpdate[$langItem->getLangId()]) > 0) {
        $langItem->update($dataUpdate[$langItem->getLangId()]);
      }
    }
    return $item;
  }

  function getCustomIteratorLang($where = '', $order = 'name', $lang = 1) {

    throw new Exception("Deprecated function, use function 'getByParams()'", 1);
    
  }

}

class Tree extends Collection {
  private $root;

  function __construct(Node $root = NULL) {
    parent::__construct();
    if ( $root && $root->name != $this->name ) {
      throw new EntityException('Root element given is not from this tree');
    }
    $this->root = $root;
    if ( $root ) {
      $this->addFilter("{$this->name}_left > {$root->getLeft()}");
      $this->addFilter("{$this->name}_right < {$root->getRight()}");
    }
  }

  /**
  * @param array $data
  * @return Node
  */
  function add(array $data = array()) {
    $this->db->begin();

    $fieldParent = $this->table->getRealFieldName('node_parent');
    $fieldRight = $this->table->getRealFieldName('node_right');
    $fieldLeft = $this->table->getRealFieldName('node_left');
    $fieldDepth = $this->table->getRealFieldName('node_depth');
    $tableName = "`{$this->table->getName()}`";

    $rootId = $this->root ? $this->root->getId() : NULL;
    $rootIdString = is_null($rootId) ? 'NULL' : $rootId;

    if ( $this->root ) {
      $res = $this->db->query("SELECT $fieldRight, $fieldDepth FROM $tableName WHERE {$this->table->getPk()}=$rootIdString FOR UPDATE");
    } else {
      $this->db->query("LOCK TABLES $tableName WRITE");
      $res = $this->db->query("SELECT MAX($fieldRight)+1, -1 FROM $tableName");
    }
    list ( $parentRight, $parentDepth ) = $res->fetch_row();
    if ( is_null($parentRight) ) {
      $parentRight = 1;
    }

    $this->db->query("UPDATE $tableName SET $fieldLeft=$fieldLeft+2 WHERE $fieldLeft>$parentRight");
    $this->db->query("UPDATE $tableName SET $fieldRight=$fieldRight+2 WHERE $fieldRight>=$parentRight");

    $data[$fieldParent] = $rootId;
    $data[$fieldDepth] = $parentDepth + 1;
    $data[$fieldLeft] = $parentRight;
    $data[$fieldRight] = $parentRight + 1;

    $node = parent::add($data);
    $this->db->query("UNLOCK TABLES");
    $this->db->commit();
    return $node;
  }

  function delete($id) {
    throw new EntityException('Not implemented yet');
  }

  function getIterator() {
    return parent::getCustomIterator('', 'node_left');//, new TreeProcessStrategy($this->root, $this->table->getRealFieldName('node_depth')));
  }
}

class TreeProcessStrategy implements IRowProcessStrategy {
  private $basedepth, $name;

  function __construct(Node $root = NULL, $fieldDepth = '') {
    $this->basedepth = $root ? $root->getDepth() : 0;
    $this->name = $fieldDepth;
  }

  function process(&$row) {
//    $row->data[$this->name] =
//    $row[$this->name] -= $this->basedepth;
  }
}

class CollectionException extends Exception {}

?>
