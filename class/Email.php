<?php
class Email {
  
  static function send_mime_mail($name_from, // имя отправителя
                          $email_from, // email отправителя
                          $name_to, // имя получателя
                          $email_to, // email получателя
                          $data_charset, // кодировка переданных данных
                          $send_charset, // кодировка письма
                          $subject, // тема письма
                          $body, // текст письма
                          $filepath = null, // прикрепленный файл
                          $filename = null // прикрепленный файл
                          ) {
    if ($name_to == '') {
      $to = $email_to;
    } else {
      $to = self::mime_header_encode($name_to, $data_charset, $send_charset)
                   . ' <' . $email_to . '>';
    }
    $subject = self::mime_header_encode($subject, $data_charset, $send_charset);
    $from =  self::mime_header_encode($name_from, $data_charset, $send_charset)
                       .' <' . $email_from . '>';
    if($data_charset != $send_charset) {
      $body = iconv($data_charset, $send_charset, $body);
    }
    $headers = "From: $from\n";
//    $headers .= "To: $email_to\n";
//    $headers .= "Subject: $subject\n";
//    $headers .= "X-Mailer: PHPMail Tool\n";
    $headers .= "Reply-To: $from\n";
    $headers .= "Return-path: $from\n";
    $headers .= "Mime-Version: 1.0\n";     
    
    
    if ($filepath) {
      $fp = fopen($filepath,"r"); 
      if (!$fp) { 
//        print "Файл $path не может быть прочитан"; 
        return false;
      } 
      $file = fread($fp, filesize($filepath)); 
      fclose($fp); 

      $boundary = "--".md5(uniqid(time())); // генерируем разделитель 
      $headers .="Content-Type: multipart/mixed; boundary=\"$boundary\"\n"; 

      $multipart .= "--$boundary\n"; 
      $multipart .= "Content-Type: text/html; charset=$send_charset\n"; 
      $multipart .= "Content-Transfer-Encoding: Quot-Printed\n\n"; 
      $multipart .= "$body\n\n"; 

      $message_part = "--$boundary\n"; 
      $message_part .= "Content-Type: application/octet-stream\n"; 
      $message_part .= "Content-Transfer-Encoding: base64\n"; 
      $message_part .= "Content-Disposition: attachment; filename = \"".$filename."\"\n\n"; 
      $message_part .= chunk_split(base64_encode($file))."\n"; 

      $body = $multipart . $message_part."--$boundary--\n"; 
      
    } else {
      $headers .= "Content-type: text/html; charset=$send_charset\n";
    }

    return mail($to, $subject, $body, $headers);
//    return mail($to, $subject, $body, $headers, "-f".$email_from);
  }

  function mime_header_encode($str, $data_charset, $send_charset) {
    if($data_charset != $send_charset) {
      $str = iconv($data_charset, $send_charset, $str);
    }
    return '=?' . $send_charset . '?B?' . base64_encode($str) . '?=';
  }  
}
?>