<?php
/* (c) 2009 Denis Shushkin, me@den.kiev.ua */

class Field {
  public $name, $type, $null, $default, $autoincrement;
  
  public function __construct($name, $type, $null = false, $default = NULL, $autoincrement = false) {
    $this->name = $name;
    $this->type = $type;
    $this->null = $null;
    $this->default = $default;
    $this->autoincrement = $autoincrement;
  }
  
  public function getSqlColumnName(Table $model) {
    return "{$model->getName()}_{$this->name}";
  }
  
  private function getSqlNull() {
    return $this->null ? "NULL" : "NOT NULL";
  }
  
  private function getSqlDefault() {
    return is_null($this->default) ? "" : "DEFAULT '{$this->default}'";
  }
  
  private function getSqlAutoincrement() {
    return $this->autoincrement ? "auto_increment" : "";
  }
  
  function getSql(Table $model) {
    return "{$this->getSqlColumnName($model)} {$this->type} {$this->getSqlNull()} {$this->getSqlDefault()} {$this->getSqlAutoincrement()}";
  }
}

?>
