<?php
/* (c) 2009 Denis Shushkin, me@den.kiev.ua */

abstract class Table {
  /**
  * @var DB
  */
  private $db;
  protected $name;

  private $manyToOne = array();
  private $foreignKeys = array(), $fields = array(), $indexes = array();

  protected function __construct() {
    $this->db = DB::getInstance();
  }

  final public function getFields() {
    /**
    * @var field
    */
    $field;
    $fields=array();
    foreach ($this->fields as $field) {
      $fields[] = $field->getSqlColumnName($this);
    }
    return $fields;
  }

  final public function getNameFields() {
    /**
    * @var field
    */
    $field;
    $fields=array();
    foreach ($this->fields as $field) {
      $fields[] = $field->name;
    }
    return $fields;
  }

  final public function getManyToOne() {
    return $this->manyToOne;
  }
  final public function getAllFields() {
    return array_merge($this->getNameFields(), $this->getManyToOne());
  }

  final public function getName() {
    return $this->name;
  }

  final public function delete($where) {
    $this->db->query("DELETE FROM `{$this->name}` WHERE $where");
  }

  function prepData(array &$data) {
    $this->db->prepareData($data);
    $keys = array_keys($data);
    foreach ($this->fields as $field) {
      if ((in_array($field->name,$keys)) && ((strtolower($field->type) == 'varchar')||(strtolower($field->type) == 'text')) ){
        $data[$field->name] = stripslashes($data[$fielad->name]);
      }
    }
  }

  final public function insert(array $data) {
//    print_r($data);
    $this->db->prepareData($data);
//    $this->prepData($data);
    $fields = join(",", array_keys($data));
    $values = join(",", array_values($data));
    $this->db->query("INSERT INTO `{$this->name}` ($fields) VALUES ($values)");
    return $this->db->insert_id;
  }

  final public function update($where, array $data) {
    $this->db->prepareData($data);
//    $this->prepData($data);
    $update = '';
    $fields = $this->getFields();
    $mto = array();
    foreach ( $this->manyToOne as $item) {
      $mto[] = $item.'_id';
    }
    foreach ( $data as $field => $value ) {
      if ((in_array($field, $fields))||(in_array($field,$mto))) {
        $update .= ", $field=$value";
      }
    }
    $update = substr($update, 2);
    $this->db->query("UPDATE `{$this->name}` SET $update WHERE $where");
    //return $this->affected_rows;
  }

  protected function addField(Field $field) {
    $this->fields[] = $field;
    if ( is_null($field->default) && !$field->null ) {
      $this->requiredFields[] = $field;
    }
  }

  protected function addFields(array $fields) {
    foreach ( $fields as $fieldName => $fieldType ) {
      $this->fields[] = new Field($fieldName, $fieldType);
    }
  }

  protected function addIndex($fields) {
    $this->indexes[] = $fields;
  }

  private function getSqlForeignKey($tableName, $onDelete = 'CASCADE', $onUpdate = 'RESTRICT') {
    $fieldName = "{$tableName}_id";
    return "CONSTRAINT `fk_{$this->name}_$tableName`
        FOREIGN KEY (`$fieldName` )
        REFERENCES `$tableName` (`$fieldName` )
        ON DELETE $onDelete
        ON UPDATE $onUpdate";
  }

  private function getSqlFields() {
    $sql = '';
//    "{$this->pk} INT NOT NULL auto_increment";

    # fields
    foreach ( $this->fields as $field ) {
      $sql .= "{$field->getSql($this)},\n";
    }

    # references
    foreach ( $this->manyToOne as $tableName ) {
      $sql .= "{$tableName}_id INT NOT NULL,\n";
    }

    return $sql;
  }

  private function getSqlIndexes() {
    $indexId = 0;
    $sql = '';
    foreach ( $this->indexes as $index ) {
      ++$indexId;
      if ( is_array($index) ) {
        foreach ( $index as &$field ) {
          $field = $this->getRealFieldName($field);
        }
        $index = join(',', $index);
      } else {
        $index = $this->getRealFieldName($index);;
      }
      $sql .= "\n
        INDEX ak_$indexId ($index),";
    }

    return $sql;
  }

  abstract protected function getSqlPrimaryKey();

  public function createTable() {
    /**
    * @var Entity
    */
    $parent = NULL;

    /**
    * @var Field
    */
    $field = NULL;

    /**
    * @var Table
    */
    $table = NULL;

    $sql = "CREATE TABLE IF NOT EXISTS `{$this->name}` (\n";
    $sql .= $this->getSqlFields();
    $sql .= $this->getSqlIndexes();
    $sql .= $this->getSqlPrimaryKey();

    # references
    foreach ( $this->manyToOne as $tableName ) {
      $sql .= ",\n" . $this->getSqlForeignKey($tableName);
    }

    # end
    $sql .= "\n) ENGINE=InnoDB\n\n";

    $this->db->query($sql);
//    echo ($sql);
}

  public function dropTable() {
    $this->db->query("DROP TABLE IF EXISTS `{$this->name}`");
  }

  private function isCreated() {
    $res = $this->db->query("SHOW TABLES LIKE '{$this->table}'");
    return $res->num_rows > 0;
  }

  /**
  * Add parent entity
  */
  protected function addManyToOne($entityName) {
    $this->manyToOne[$entityName] = $entityName;
  }

  /**
  * @param string $entityName
  * @return Table
  */
  static public function getByEntityName($entityName) {
//    require_once $_SERVER['DOCUMENT_ROOT'] . Settings::get('d') . "model/$entityName.php";
    $className = "{$entityName}Table";
    return new $className;
  }
}

abstract class EntityTable extends Table {
  private $pk;

  private $manyToMany = array();
  private $manyToManyOwning = array();

  protected function __construct() {
    parent::__construct();
    $this->name = strtolower(substr(get_class($this), 0, -5));
    $this->pk = "{$this->getName()}_id";
    $this->addField(new Field('id', 'int', false, NULL, true));
  }

  public function createTable() {
    parent::createTable();
    foreach ( $this->manyToManyOwning as $entityName => $assocTable ) {
      $table = Table::getByEntityName($assocTable);
      $table->createTable();
    }
  }

  final public function getManyToMany() {
    return $this->manyToMany;
  }

  final public function getPk() {
    return $this->pk;
  }

  final public function getRealFieldName($fieldName) {
    if (in_array($fieldName,$this->getManyToOne())) {
      return $fieldName.'_id';
    }
    return "{$this->getName()}_$fieldName";
  }

  protected function getSqlPrimaryKey() {
    return "PRIMARY KEY ({$this->pk})";
  }

  /**
  * May be used in future for automatic model generation
  *
  * @param mixed $name
  */
  protected function addOneToMany($name) {}

  /**
  * Add many-to-many relationship with another entity
  *
  * @param string Entity name
  * @param bool Whether we own the relationship
  */
  protected function addManyToMany($entityName, $isOwning = true) {
    if ( $isOwning ) {
      $assocTable = "{$this->getName()}Has{$entityName}";
      $this->manyToManyOwning[$entityName] = $assocTable;
    } else {
      $assocTable = "{$entityName}Has{$this->getName()}";
    }
    $this->manyToMany[$entityName] = $assocTable;
  }
}

abstract class AssociativeTable extends EntityTable {
  private $keys;

  public function __construct() {
    parent::__construct();
    $entities = explode('Has', substr(get_class($this), 0, -5));
    $this->name = strtolower(join('has', $entities));
    foreach ( $entities as &$entityName ) {
      $entityName = strtolower($entityName);
      $this->addManyToOne($entityName);
      $this->keys[] = "{$entityName}_id";
    }
  }

//  final protected function getSqlPrimaryKey() {
//    $keys = join(',', $this->keys);
//    return "PRIMARY KEY ($keys)";
//  }
}

?>