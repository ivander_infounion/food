<?php
/* (c) 2007-2009 Denis Shushkin, me@den.kiev.ua */

abstract class Model {
  /**
  * @var DB
  */
  protected $db;
  protected $name;
  /**
  * @var EntityTable
  */
  protected $table;
  
  protected function __construct() {
    $this->db = DB::getInstance();
    $this->name = preg_replace('/(Collection)$/', '', get_class($this));
//    $table = strtolower(preg_replace('/(?<!^)(?=[A-Z])/', self::ENTITY_SEPARATOR, $this->name));
    $this->table = Table::getByEntityName($this->name);
  }
  
  final public function getEntityName() {
    return $this->name;
  }
  final public function Name() {
    return strtolower($this->name);
  }

}

?>
