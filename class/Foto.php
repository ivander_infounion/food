<?php
/* (c) 2009 Sanya Vol., shv@ukr.net */

class Foto {

  function checkFoto($picture, $width, $height, $_atrfile, $obj = null) {
    $result = array('error' => false, 'ext' => '', 'status' => '');
    if ($_atrfile['name'] == '') {
      $result['ext'] = '';
      $result['fname'] = '';
      $result['status'] = 'пустое имя';
    } else {
      $fileName = $_atrfile['name'];
      $check = $this->checkImage($_atrfile['tmp_name'],$width,$height);
      if ($check == 1) {
        $pos = strrpos($fileName,'.');
        $result['ext'] = substr($fileName,$pos+1);
        $result['fname'] = substr($fileName,0,$pos);
        if ($obj) {
          $file = $obj->getRealPathPict($picture);
//          if ($picture == 'sm') {
//            $file = $obj->getRealPathSm();
//          } elseif ($picture == 'big') {
//            $file = $obj->getRealPathBig();
//          }
          if (file_exists($file)) {
            unlink($file);
          }
        }
      } else {
        $result['ext'] = '';
        $result['fname'] = '';
        $result['status'] = $check;
        $result['error'] = true;
      }
    }

    return $result;
  }

  function checkImage($fileName, $width, $height) {
    if (($width == 0)&&($height == 0)) {
      return 1;
    }
    str_replace(' ', '%20', $fileName);
    if (!is_file($fileName)) return 'Неверное имя файла';
    $atr = getimagesize($fileName);
    if ((($atr[0] >= $width-1)&&($atr[0] <= $width+1)) && (($atr[1] >= $height-1)&&($atr[1] <= $height+1))) {
      return 1;
    } else {
      return 'Неверный формат картинки';
    }
  }

  /***********************************************************************************
  Функция img_resize(): генерация thumbnails
  Параметры:
    $src             - имя исходного файла
    $dest            - имя генерируемого файла
    $width, $height  - ширина и высота генерируемого изображения, в пикселях
  Необязательные параметры:
    $rgb             - цвет фона, по умолчанию - белый
    $quality         - качество генерируемого JPEG, по умолчанию - максимальное (100)
  ***********************************************************************************/
  static public function img_resize($src, $dest, $width, $height, $rgb=0xFFFFFF, $quality=95)
  {
    if (!file_exists($src)) return array('error'=>true, 'status'=>'File not exists!');

    $size = getimagesize($src);

    if ($size === false) return array('error'=>true, 'status'=>'Picture size FALSE!');

    // Определяем исходный формат по MIME-информации, предоставленной
    // функцией getimagesize, и выбираем соответствующую формату
    // imagecreatefrom-функцию.
    $format = strtolower(substr($size['mime'], strpos($size['mime'], '/')+1));
    $icfunc = "imagecreatefrom" . $format;

    $x_ratio = $width / $size[0];
    $y_ratio = $height / $size[1];

    $ratio       = min($x_ratio, $y_ratio);
    $use_x_ratio = ($x_ratio == $ratio);

    $new_width   = $use_x_ratio  ? $width  : floor($size[0] * $ratio);
    $new_height  = !$use_x_ratio ? $height : floor($size[1] * $ratio);
    $new_left    = $use_x_ratio  ? 0 : floor(($width - $new_width) / 2);
    $new_top     = !$use_x_ratio ? 0 : floor(($height - $new_height) / 2);

    if (!function_exists($icfunc)) {
      array('error'=>true, 'status'=>'Not exists function '.$icfunc.'!');
    } else {
      if (!($isrc = $icfunc($src))) {
        array('error'=>true, 'status'=>'why?? '.$icfunc.'!');
      }
    }
  //  if (!$isrc) {
  //    return array('error'=>true, 'status'=>'No img '.$src.'!');
  //  }
    if (!function_exists('imagecreatetruecolor')) {
      array('error'=>true, 'status'=>'why11?? imagecreatetruecolor!');
    }
    if (!($idest = imagecreatetruecolor($width, $height))) {
      array('error'=>true, 'status'=>'why?? imagecreatetruecolor!');
    }

    imagefill($idest, 0, 0, $rgb);
    imagecopyresampled($idest, $isrc, $new_left, $new_top, 0, 0,
      $new_width, $new_height, $size[0], $size[1]);

    imagejpeg($idest, $dest, $quality);

    imagedestroy($isrc);
    imagedestroy($idest);

    return array('error'=>false, 'status'=>'OK');

  }

  static public function img_cut($src, $dest, $width, $height, $rgb=0xFFFFFF, $quality=95)
  {
    if (!file_exists($src)) return array('error'=>true, 'status'=>'File not exists!');

    $size = getimagesize($src);

    if ($size === false) return array('error'=>true, 'status'=>'Picture size FALSE!');

    // Определяем исходный формат по MIME-информации, предоставленной
    // функцией getimagesize, и выбираем соответствующую формату
    // imagecreatefrom-функцию.
    $format = strtolower(substr($size['mime'], strpos($size['mime'], '/')+1));
    $icfunc = "imagecreatefrom" . $format;

    $x_ratio = $width / $size[0];
    $y_ratio = $height / $size[1];

    $ratio       = max($x_ratio, $y_ratio);
    $use_x_ratio = ($x_ratio == $ratio);

    $new_width   = floor($size[0] * $ratio);
    $new_height  = floor($size[1] * $ratio);
//    $new_left    = $use_x_ratio  ? 0 : floor(($width - $new_width) / 2);
//    $new_top     = !$use_x_ratio ? 0 : floor(($height - $new_height) / 2);

    if (!function_exists($icfunc)) {
      array('error'=>true, 'status'=>'Not exists function '.$icfunc.'!');
    } else {
      if (!($isrc = $icfunc($src))) {
        array('error'=>true, 'status'=>'why?? '.$icfunc.'!');
      }
    }

    if (!function_exists('imagecreatetruecolor')) {
      array('error'=>true, 'status'=>'why11?? imagecreatetruecolor!');
    }
    if (!($idest = imagecreatetruecolor($new_width, $new_height))) {
      array('error'=>true, 'status'=>'why?? imagecreatetruecolor!');
    }

    imagefill($idest, 0, 0, $rgb);
    imagecopyresampled($idest, $isrc, 0, 0, 0, 0,
      $new_width, $new_height, $size[0], $size[1]);

    if (($new_width > $width+1)) {
      $idest2 = imagecreatetruecolor($width, $height);
      $left = floor(($new_width - $width) / 2);
      imagecopyresized($idest2, $idest, 0, 0, $left, 0, $width, $height, $width, $height);
      imagejpeg($idest2, $dest, $quality);
      imagedestroy($idest2);
    } elseif ($new_height > $height+1) {
      $idest2 = imagecreatetruecolor($width, $height);
      $top = floor(($new_height - $height) / 2);
      imagecopyresized($idest2, $idest, 0, 0, 0, $top, $width, $height, $width, $height);
      imagejpeg($idest2, $dest, $quality);
      imagedestroy($idest2);
    } else {
      imagejpeg($idest, $dest, $quality);
    }

    imagedestroy($isrc);
    imagedestroy($idest);

    return array('error'=>false, 'status'=>'OK');

  }

 static public function img_crop($src, $dest, $x_start, $y_start, $width, $height, $width_needed, $height_needed, $rgb=0xFFFFFF, $quality=95)
  {
    if (!file_exists($src)) return array('error'=>true, 'status'=>'File not exists!');

    $size = getimagesize($src);

    if ($size === false) return array('error'=>true, 'status'=>'Picture size FALSE!');

    // Определяем исходный формат по MIME-информации, предоставленной
    // функцией getimagesize, и выбираем соответствующую формату
    // imagecreatefrom-функцию.
    $format = strtolower(substr($size['mime'], strpos($size['mime'], '/')+1));
    $icfunc = "imagecreatefrom" . $format;

    if (!function_exists($icfunc)) {
      array('error'=>true, 'status'=>'Not exists function '.$icfunc.'!');
    } else {
      if (!($isrc = $icfunc($src))) {
        array('error'=>true, 'status'=>'why?? '.$icfunc.'!');
      }
    }

    if (!function_exists('imagecreatetruecolor')) {
      array('error'=>true, 'status'=>'why11?? imagecreatetruecolor!');
    }
    if (!($idest = imagecreatetruecolor($width, $height))) {
      array('error'=>true, 'status'=>'why?? imagecreatetruecolor!');
    }

    imagefill($idest, 0, 0, $rgb);

    $x_start_original = $x_start;
    $y_start_original = $y_start;
    if($x_start_original < 0 || $y_start_original < 0){
      if($x_start_original < 0){
        $place_x = abs($x_start);
        $x_start = 0;
      }else{
        $place_x = 0;
      }
      if($y_start_original < 0){
        $place_y = abs($y_start);
        $y_start = 0;
      }else{
        $place_y = 0;
      }
    }
    if($width <= $size[0]){
      $src_w = $size[0]-abs($x_start_original);

    }
    if($height <= $size[1]){
      $src_h = $size[1]-abs($y_start_original);
    }

    if(($place_x+$size[0])<$width){
      $src_w = $size[0];
    }
    if(($place_y+$size[1])<$height){
      $src_h = $size[1];
    }

    imagecopyresampled($idest, $isrc,  $place_x, $place_y, $x_start, $y_start,
      $src_w, $src_h, $src_w, $src_h);
    
    imagejpeg($idest, $dest, $quality);

    imagedestroy($isrc);
    imagedestroy($idest);

    return array('error'=>false, 'status'=>'OK');
  }

  static public function create_water($src, $src_logo = "img/watermark.png", $place = 'center')
  {
    $src_water = $src_logo;
//    $src_water = Settings::get('d') . $src_logo;
    if (!file_exists($src)) return array('error'=>true, 'status'=>'File not exists!');
    if (!file_exists($src_water)) return array('error'=>true, 'status'=>'File watermark not exists!');

    $size = getimagesize($src);

    if ($size === false) return array('error'=>true, 'status'=>'Picture size FALSE!');

//    if ($size[0] <= 500) {
//      $src_water = "images/water-sm.png";
//      $minus_px = 30;
//    } else {
//      $src_water = "images/water-big.png";
//      $minus_px = 100;
//    }
    $minus_px = 100;


    $size_water = getimagesize($src_water);

    if($size_water[0] > $size[0]) {
      $newwater_width = round($size[0] * 70 / 100);
      $use_x_ratio = $newwater_width / $size_water[0];
      $newwater_height = round($size_water[1] * ($newwater_width / $size_water[0]));

      Foto::img_resize_png($src_water, 'img/watermark.png', $newwater_width, $newwater_height);

      $src_water = 'img/watermark.png';
    }

    $size_water = getimagesize($src_water);
    $water = imagecreatefrompng($src_water);
    // Определяем исходный формат по MIME-информации, предоставленной
    // функцией getimagesize, и выбираем соответствующую формату
    // imagecreatefrom-функцию.
    $format = strtolower(substr($size['mime'], strpos($size['mime'], '/')+1));
    $icfunc = "imagecreatefrom" . $format;

    $isrc = $icfunc($src);

    if ($format == 'png') {

      $image = imagecreatetruecolor($size[0], $size[1]);
      $color = imagecolorallocatealpha($image, 0, 0, 0, 127);
      imagealphablending($image, false);
      imagefilledrectangle($image, 0, 0, $size[0], $size[1], $color);
      imagesavealpha($image, true);
      imagealphablending($image, true);

      imagecopyresampled($image, $isrc, 0, 0, 0, 0, $size[0], $size[1], $size[0], $size[1]);

      if ($place == 'center') {
        imagecopyresampled($image, $water, round(($size[0] - $size_water[0])/2,0), round(($size[1] - $size_water[1])/2,0), 0, 0, $size_water[0], $size_water[1], $size_water[0], $size_water[1]);
      } elseif ($place == 'center-bottom') {
        imagecopyresampled($image, $water, round(($size[0] - $size_water[0])/2,0), round(($size[1] - $size_water[1] - $size[1]*0.05),0), 0, 0, $size_water[0], $size_water[1], $size_water[0], $size_water[1]);
      } elseif($place == 'center-top') {
        imagecopyresampled($image, $water, round(($size[0] - $size_water[0])/2,0), round(($size_water[1] + $size[1]*0.05),0), 0, 0, $size_water[0], $size_water[1], $size_water[0], $size_water[1]);
      } else {
        imagecopyresampled($image, $water, round(($size[0] - $size_water[0] - 20),0), round(($size[1] - $size_water[1] - 70),0), 0, 0, $size_water[0], $size_water[1], $size_water[0], $size_water[1]);
      }

      imagepng($image, $src, 1);

      imagedestroy($image);
      imagedestroy($water);
    } else {

      if ($place == 'center') {
        imagecopy($isrc, $water, round(($size[0] - $size_water[0])/2,0), round(($size[1] - $size_water[1])/2,0), 0, 0, $size_water[0], $size_water[1]);
      } elseif ($place == 'center-bottom') {
        imagecopy($isrc, $water, round(($size[0] - $size_water[0])/2,0), round(($size[1] - $size_water[1] - $minus_px),0), 0, 0, $size_water[0], $size_water[1]);
      } elseif($place == 'center-top') {
          imagecopy($isrc, $water, round(($size[0] - $size_water[0]),0), round(($size[1] - $size_water[1]),0), 0, 0, $size_water[0], $size_water[1]);
      } else {
        imagecopy($isrc, $water, round(($size[0] - $size_water[0] - 20),0), round(($size[1] - $size_water[1] - 70),0), 0, 0, $size_water[0], $size_water[1]);
      }

      $_ifunc = "image" . $format;
      $_ifunc($isrc, $src);

      imagedestroy($water);
      imagedestroy($isrc);
    }

    return array('error'=>false, 'status'=>'OK');

  }


  static public function img_resize_png($src, $dest, $width, $height, $rgb=0xFFFFFF, $quality=95)
  {
    if (!file_exists($src)) return array('error'=>true, 'status'=>'File not exists!');

    $size = getimagesize($src);

    if ($size === false) return array('error'=>true, 'status'=>'Picture size FALSE!');

    // Определяем исходный формат по MIME-информации, предоставленной
    // функцией getimagesize, и выбираем соответствующую формату
    // imagecreatefrom-функцию.
    $format = strtolower(substr($size['mime'], strpos($size['mime'], '/')+1));
    $icfunc = "imagecreatefrom" . $format;

    $x_ratio = $width / $size[0];
    $y_ratio = $height / $size[1];

    $ratio       = min($x_ratio, $y_ratio);
    $use_x_ratio = ($x_ratio == $ratio);

    $new_width   = $use_x_ratio  ? $width  : floor($size[0] * $ratio);
    $new_height  = !$use_x_ratio ? $height : floor($size[1] * $ratio);
    $new_left    = $use_x_ratio  ? 0 : floor(($width - $new_width) / 2);
    $new_top     = !$use_x_ratio ? 0 : floor(($height - $new_height) / 2);

    if (!function_exists($icfunc)) {
      array('error'=>true, 'status'=>'Not exists function '.$icfunc.'!');
    } else {
      if (!($isrc = $icfunc($src))) {
        array('error'=>true, 'status'=>'why?? '.$icfunc.'!');
      }
    }
    imagealphablending($isrc, false);
    imagesavealpha($isrc, true);    

    $image = imagecreatetruecolor($width, $height);
    $color = imagecolorallocatealpha($image, 0, 0, 0, 127);
    imagealphablending($image, false);
    imagefilledrectangle($image, 0, 0, $width, $height, $color);
    imagesavealpha($image, true);
    imagealphablending($image, true);

    imagecopyresampled($image, $isrc, $new_left, $new_top, 0, 0,
      $new_width, $new_height, $size[0], $size[1]);

    imagepng($image, $dest, 1);

    imagedestroy($isrc);
    imagedestroy($image);

    return array('error'=>false, 'status'=>'OK');

  }

}

?>
