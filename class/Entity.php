<?php
/* (c) 2007-2009 Denis Shushkin, me@den.kiev.ua */

abstract class Entity extends Model {
  protected $id;
  protected $data;
  protected $loaded;
  private $where;

  /**
  * @param int Entity ID
  */
  public function __construct($id) {
    parent::__construct();
    if ( is_null($id) ) {
      $this->id = NULL;
      return;
    }
    $this->id = intval($id);
    $this->data = array();
    $this->loaded = false;
    $this->where = "{$this->table->getPk()}='{$this->id}'";
  }

  final public function getId() {
    return $this->id;
  }

  function delete() {
    $id = $this->id;
    $this->table->delete("{$this->table->getPk()}='{$this->id}'");
    return $id;
  }

  function getArrayIds($entity) {
    $colEnt = ucfirst($entity) . 'Collection';
    $col = new $colEnt();
    return $col->getArrayIds(array($this->Name() => $this->getId()));
  }

  function getDateTime($field = 'created', $date = null, $value = null) {
    if ($field) {
      $_value = $this->getField($field);
    } else {
      $_value = $value;
    }
    if (is_null($_value)) {
      return '';
    }
    if ($date) {
      if ($date == 'sqldate') {
        return substr($_value, 0, 10);
      }
      if ($date == 'date') {
        return date('d.m.Y', strtotime($_value));
      } elseif ($date == 'datetime') {
        return date('d.m.Y H:i', strtotime($_value));
      } elseif ($date == 'java') {
        return date('M,d,Y,H:i:s', strtotime($_value));
      } elseif ($date == 'dM') {
        $months = Settings::get('months');
        $d = date('j', strtotime($_value));
        $m = $months[Settings::get('deflang')][date('n', strtotime($_value))];
        return $d.' '.$m;
      } elseif ($date == 'datelong') {
        $months = Settings::get('months');
        $d = date('j', strtotime($_value));
        $y = date('Y', strtotime($_value));
        $m = $months[Settings::get('deflang')][date('n', strtotime($_value))];
        return $d.' '.$m.' '.$y;
      } else {
        return date($date, strtotime($_value));
      }
    }
    return $_value;
  }
  final function getField($field) {
    return $this->getRealField($this->table->getRealFieldName($field));
  }

  final protected function getParentEntity($entityName) {
    $entity = ucfirst($entityName);
    return new $entity($this->getRealField("{$entityName}_id"));
  }

  final protected function setField($field, $value) {
    $this->setFields(array($field => $value));
  }

  final protected function setParentEntity(Entity $entity) {
    $this->setRealField($entity->table->getPk(), $entity->getId());
  }

  static function getSingle($field, $value, $entity) {
    $db = DB::getInstance();
    $value = $db->real_escape_string($value);
    $entity = strtolower($entity);
    $id = $db->fetchSingle("SELECT {$entity}_id FROM $entity WHERE {$entity}_$field='$value'");
    if ($id) {
      $entity = ucfirst($entity);
      return new $entity($id);
    } else {
      return false;
    }
  }

  function getData($manyToMany = null) {
    $data = $this->db->fetchRowAssoc("SELECT * FROM `{$this->table->getName()}` WHERE {$this->where}");
    $res = array();
    $fields = $this->table->getNameFields();
    foreach ($fields as $field) {
      $res[ $field ] = $data[$this->table->getName().'_'.$field];
    }
    $fields = $this->table->getManyToOne();
    foreach ($fields as $field) {
      $res[ $field ] = $data[$field.'_id'];
    }
    if ($manyToMany) {
      $mtm = $this->table->getManyToMany();
      foreach ($mtm as $entity => $assocTab) {
        $assocCol = $assocTab . 'Collection';
        $col_mtm = new $assocCol();
        $col_mtm->params[ $this->table->getName() ] = $this->getId();
        $mtm_items = $col_mtm->getByParams();
        $data_array = array();
        foreach ($mtm_items as $mtmkey => $mtm_item) {
          $data_array[] = $mtm_item->getData();
        }
        $res[ $entity ] = $data_array;
      }
    }
    return $res;
  }

  final protected function loadData() {
    if ( $this->loaded ) {
      return;
    } elseif ( $this->id ) {
      try {
        $this->data = $this->db->fetchRowAssoc("SELECT * FROM `{$this->table->getName()}` WHERE {$this->where}");
      } catch ( Exception $e ) {
        throw new EntityException("Entity not found (id={$this->id})");
      }
      $this->loaded = true;
      return;
    } else {
      throw new EntityException("Data fetch attempt for non-stored entity");
    }
  }

//  final private function getRealField($field) {
  function getRealField($field) {
    $this->loadData();
//    if ( isset($this->data[$field]) ) {
    if ( key_exists($field, $this->data) ) {
      return $this->data[$field];
    } else {
//      print_r($this->data);
//      var_dump($this->id);
//      if (is_null($this->data[$field])) {
//        return null;
//      } else {
        throw new EntityException('Attempt to fetch non-existing field');
//      }
    }
  }

  final protected function setRealField($field, $value) {
    $this->setRealFields(array($field => $value));
  }

  final private function setRealFields(array $data) {
    if ( $this->id ) {
      $this->table->update($this->where, $data);
      if ( $this->loaded ) {
        foreach ( $data as $key => $value ) {
          $this->data[$key] = $value;
        }
      }
    } else {
      throw new EntityException("Data update attempt for non-stored entity");
    }
  }

  final protected function setFields(array $data) {
    unset($data['id']);
    if ((isset($data['pass']))&&($data['pass'] == '')) {
      unset($data['pass']);
    }
    $realData = array();
    foreach ( $data as $key => $value ) {
      $realData[$this->table->getRealFieldName($key)] = $value;
    }
    // many to many
    $mtm = $this->table->getManyToMany();
    foreach ($mtm as $ent => $assocTab) {
      if (isset($data[$ent])) {
        $insertDataMany = $data[$ent];
        $entCol = ucfirst($ent) . "Collection";
        $col = new $entCol();
        $issetDataMany = $col->getArrayIds(array($this->Name() => $this->getId()));
        $entColMany = $assocTab . "Collection";
        $colMany = new $entColMany();
        foreach ($insertDataMany as $key => $value) {
          if ( !in_array($value, $issetDataMany) ) {
            $colMany->add(array($this->Name() => $this->getId(), $ent => $value));
          }
        }
        foreach ($issetDataMany as $key => $value) {
          if ( !in_array($value, $insertDataMany) ) {
            // добавляем в эту функцию массив айди, для страниц с урл нужно по айди найти урл и удалить его
            $colMany->deleteByQuery("{$this->Name()}_id='{$this->getId()}' AND {$ent}_id='$value'", array($this->Name()=>$this->getId(), $ent=>$value));
          }
        }
      }
    }
    //
    $this->setRealFields($realData);
  }
  function update($data) {
    $this->setFields($data);
    return true;
  }

  static function getByIds($entity, array $search) {
    $db = DB::getInstance();
    $where = '1';
    foreach ($search as $key => $value) {
      $where .= " AND {$key}_id=$value";
    }
    $ent = strtolower($entity);
    $id = $db->fetchSingle("SELECT {$ent}_id FROM $ent WHERE $where");
    if ($id) {
      return new $entity($id);
    } else {
      return false;
    }
  }

}

abstract class Entitywithlang extends Entity {
/** Voloshanivsky, 09.2014 Multilang NEW version  *****
*   Voloshanivsky, 03.2017 Изменения для работы с любым количеством языковых версий
*   дополнительные методы для сущностей с полями на нескольких языках
*/
  protected $list_lang;
  public $langItems;
  function __construct($id) {
    parent::__construct($id);
    $collang = new LangCollection();
    $this->list_lang = $collang->getCustomIterator("");
    $col = $this->getLangItems();
    foreach ($col as $li) {
      $this->langItems[ $li->getLangId() ] = $li;
    }
  }
  /**
  * Получить $entity($parent) для языка $lang
  * @param ID entity
  * @param ID Lang 
  * @param Entity name
  */
  static function getItemByEntityLang($parent, $lang, $entity) {
      $entity = strtolower($entity);
      $db = DB::getInstance();
      $parent = intval($parent);
      $lang = intval($lang);
      $id = $db->fetchSingle("SELECT ".$entity."haslang_id FROM ".$entity."haslang WHERE ".$entity."_id='$parent' AND lang_id='$lang'");
      if ($id) {
        $ent = ucfirst($entity).'Haslang';
        return new $ent($id);
      } else {
        return false;
      }
  }

  /**
  * Для каких языках существует объект
  */
  function getLangItems() {
    $entityCol = strtolower($this->getEntityName()).'HaslangCollection';
    $col = new $entityCol();
    return $col->getCustomIterator($this->Name().'_id='.$this->getId());
  }
  /**
  * Кол-во подключенных языков для объекта
  */
  function getCountLang() {
    return count($this->langItems);
  }

  function getItemLangId($lang = 1) {
    if (isset($this->langItems[ $lang ])) {
      return $this->langItems[ $lang ]->getId();
    }
    return false;
  }

  function getItemLang($lang = 1) {
    if (isset($this->langItems[ $lang ])) {
      return $this->langItems[ $lang ];
    }
    return false;
  }
  function issetItemLang($lang = 1) {
    return isset($this->langItems[ $lang ]);
  }

  /**
  * defaultfield: если пустое поле field то выводим поле defaultfield
  * (это больше частный случай вместо title выводить name)
  */
  function getFieldLang($field, $lang = 1, $defaultfield = null) {
    if (!is_int($lang)) {
      $lang = Lang::getRealId($lang);
    }
    if ($this->issetItemLang( $lang )) {
      $itemLang = $this->getItemLang( $lang );
      if (($itemLang->getField($field) == '') && $defaultfield) {
        //это конешно все частный случай, но так проще - если нет тайтл то берем название
        return $itemLang->getField($defaultfield);
      }
      return $itemLang->getField($field);
    } else {
      return '';
    }
  }

  function setFieldLang($field, $value, $lang = 1) {
    if ($this->issetItemLang( $lang )) {
      $itemLang = $this->getItemLang($lang);
      return $itemLang->setField($field, $value);
    } else {
      return false;
    }
  }

  function delete() {
    $items = $this->getLangItems();
    foreach ($items as $lang) {
      $lang->delete();
    }
    return parent::delete();
  }

  function update($data) {
    $this->setFields($data);
    $entTabLang = $this->Name().'HaslangTable';
    $tablang = new $entTabLang();
    $fields = $tablang->getNameFields();
    $list_langs = new LangCollection();
    $list_langs = $list_langs->getFetchValues("lang_id");
    $dataUpdate = [];
    foreach ($list_langs as $lang) {    
      $dataUpdate[$lang] = [];
      foreach ($fields as $field) {
        if (array_key_exists($field.$lang, $data)) {
          $dataUpdate[$lang][$field] = $data[$field.$lang];
        }
      }
    }
    $langItems = $this->getLangItems();
    foreach ($langItems as $langItem) {
      if (count($dataUpdate[$langItem->getLangId()]) > 0) {
        $langItem->update($dataUpdate[$langItem->getLangId()]);
      }
    }
    return $this;

  }
//************************************************************
}

class Node extends Entity {
  /**
  * @return Tree
  */
  function getChildren() {
    $class = "{$this->name}Collection";
    return new $class($this);
  }

  function getDepth() {
    return $this->getField('node_depth');
  }

  function getLeft() {
    return $this->getField('node_left');
  }

  function getParentNode() {
    $class = $this->name;
    return new $class($this->getField('node_parent'));
  }

  function getRight() {
    return $this->getField('node_right');
  }
}

class EntityException extends Exception {}

?>