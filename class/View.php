<?php
/* (c) 2007-2008 Denis Shushkin, me@den.kiev.ua */

class View {
  /**
  * @var Smarty
  */
  protected $smarty = NULL;
  static protected $instance = NULL;
  var $header = false, $footer = false, $display_compl = false;
  var $ctrl = array(), $action = array(), $current_menu = array(), $current_section = array(), $breadcrumbs;

  /**
  * @desc Get instance of class
  * @return View
  */
  final static function getInstance() {
    if (self::$instance == NULL) {
      self::$instance = new self();
    }
    return self::$instance;
  }
  final private function __clone() {}

  protected function __construct() {
    require_once 'smarty/Smarty.class.php';
    $settings = Settings::get('Smarty');
    $this->smarty = new Smarty();
    $this->smarty->template_dir = $settings['templates'];
    $this->smarty->compile_dir  = $settings['compile'];
    $this->smarty->cache_dir    = $settings['cache'];
    $this->smarty->config_dir   = $settings['config'];
//    $this->smarty->security     = false;
  }

  /**
  * @desc assigns values to template variables
  * @param array|string $tpl_var the template variable name(s)
  * @param mixed $value the value to assign
  */
  function assign($tpl_var, $value = null) {
    $this->smarty->assign($tpl_var, $value);
  }

  function del_assing($tpl_var) {
    $this->smarty->clearAssign($tpl_var);
  }

  function displayError() {
    $this->assign('d', Settings::get('d'));
    $this->assign('fileTpl', 'admin/error.tpl');
    $this->display('admin/default.tpl');
  }

  function displayLoginForm() {
    $this->assign('d', Settings::get('d'));
    $trans = new TranslateCollection();
    $trans = $trans->getTranslate(Settings::get('deflang'), 'cms');
    $this->assign('conf', $trans);
    $this->assign('logo_start', Settings::get('logo_start'));
    $this->display('admin/login.tpl');
    $this->display_compl = true;
  }

  function display($template_name) {
    if ($this->display_compl) {
      return;
    }
    if ( $this->header ) {
      $this->smarty->display($this->header);
    }
    $this->assign('ctrl', $this->ctrl);
    $this->assign('action', $this->action);
    $this->assign('current_menu', $this->current_menu);
    $this->assign('current_section', $this->current_section);
    if (is_array($this->breadcrumbs)) {
      ksort($this->breadcrumbs);
      $this->assign('breadcrumbs', $this->breadcrumbs);
    }
    $this->smarty->display($template_name);
//    $this->display_compl = true;
    if ( $this->footer ) {
      $this->smarty->display($this->footer);
    }
  }
}

?>
