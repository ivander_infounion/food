<?php
/* (c) 2007-2009 Denis Shushkin, me@den.kiev.ua */

abstract class AbstractFrontController {
  private $processing;

  final private function __clone() {}
  protected function __construct() {}

  /**
  * @desc Получает имя класса из имени контроллера
  */
  abstract protected function GetControllerClassName($controller);

  /**
  * @desc Определяет из параметров запроса имя контроллера, имя действия и остальные переданные браузером параметры ($_REQUEST)
  */
  protected function ParseRequest() {
    $controller = (isset($_REQUEST['c'])?$_REQUEST['c']:'');
    $action = (isset($_REQUEST['a'])?$_REQUEST['a']:'');
//    $action = $_REQUEST['a'];
    unset($_REQUEST['c']);
    unset($_REQUEST['a']);
    return array($controller, $action, $_REQUEST);
  }

  /**
  * @desc Обрабатывает поступивший запрос
  */
  protected function Process() {
    if ( $this->processing ) {
      throw new Exception("AbstractFrontController::process() called twice");
    }
    $this->processing = true;

    header('Content-type: text/html; charset='.Settings::get('encoding'));

    list($controller, $action, $params) = $this->ParseRequest();

    # проверяем допустимость имён контроллера и действия
    if ( preg_match('/[^A-Za-z0-9_\-]/', $controller . $action) || $controller == '' || $action == '' ) {
      throw new NotFoundException("Invalid arguments");
    }

    $class = $this->GetControllerClassName($controller);
    # проверяем существование контроллера
    if ( !class_exists($class) || !in_array("AbstractActionController", class_parents($class)) ) {
      throw new NotFoundException("Controller $controller not found.", 100);
    }

    # новый экземпляр контроллера действий (action controller), передаём конструкторы параметры
    $object = new $class($params);
    # имя метода
    $method = $action . 'Action';

    # проверяем существование такого действия в контроллере
    if ( method_exists($object, $method) ) {
      # запускаем действие
//      echo $controller.'<br>'.$action;
      return array('c'=>$controller,'a'=>$action,'r'=>$object->$method());
    } else {
      throw new NotFoundException("Action $controller/$action is not defined", 100);
    }
  }
}

/**
* @desc Фронт-контроллер для обычных (синхронных) запросов
*/
class FrontController extends AbstractFrontController {
  static private $instance;

  /**
  * @return FrontController
  */
  static function GetInstance() {
    if ( !isset(self::$instance) ) {
      self::$instance = new self;
    }
    return self::$instance;
  }

  protected function GetControllerClassName($controller) {
    return $controller . 'Controller';
  }

  static function MakeUrl($controller, $action, $params = array()) {
    if ( isset($params['a']) || isset($params['c']) ) {
      throw new Exception("Cannot use params 'a' or 'c'");
    }
    if ( preg_match('/[^A-Za-z0-9_\-]/', $controller . $action) ) {
      throw new Exception("Invalid arguments");
    }
//    $query = $controller ? 'c=' . urlencode($controller) . '&' : '';
//    $query .= $action ? 'a=' . urlencode($action) . '&' : '';
    $query = $controller ? urlencode($controller) . '/' : '';
    $query .= $action ? urlencode($action) . '/' : '';
//    if ( $query || count($params) ) {
//      $query = "?$query";
//    }
    if ( count($params) ) {
      $query = "$query?";
    }
    return Settings::get('baseurl') . strtolower("$query") . http_build_query($params);
  }

  function ParseRequest() {
    list($controller, $action, $params) = parent::ParseRequest();
    if ( $controller == '' ) {
      $controller = 'Default';
    }
    if ( $action == '' ) {
      $action = 'Default';
    }
    return array($controller, $action, $params);
  }

  function Process() {
    try {
      parent::Process();
    } catch ( NotFoundException $e ) {
      if ($e->getCode()) {
        View::getInstance()->assign('error', $e);
        if ($e->getCode() == 100) {
//          under construct
        } elseif ($e->getCode() == 404) {
//          not found
        }
        View::getInstance()->displayError();
      } else {
        echo $e;
      }
//      header("HTTP/1.0 404 Not Found");
//      View::getInstance()->display('404.tpl');
    }
  }
}

/**
* @desc Фронт-контроллер для AJAX (асинхроннных) запросов
*/
class AjaxFrontController extends AbstractFrontController {
  static private $instance;

  /**
  * @return AjaxFrontController
  */
  static function GetInstance() {
    if ( !isset(self::$instance) ) {
      self::$instance = new self;
    }
    return self::$instance;
  }

  protected function __construct() {
    new JsHttpRequest(Settings::get('encoding'));
  }

  protected function GetControllerClassName($controller) {
    return $controller . 'AjaxController';
  }

  protected function ParseRequest() {
    $result = parent::ParseRequest();
    if ( isset($result[2]['d']) )
      $result[2] = $result[2]['d'];
    return $result;
  }


  function Process() {
    try {
      $GLOBALS['_RESULT'] = parent::Process();
    } catch ( NotFoundException $e ) {
      # действия, когда что-то не найдено во время ajax-запроса
      echo $e->__toString();
    }
  }
}

/**
* @desc Абстрактный контроллер действий
*/
abstract class AbstractActionController {
  protected $params;

  function __construct($params) {
    $this->params = $params;
    session_start();
  }
  /**
  * @desc Транслит
  */
  function translit($content){
    $transA=array('А'=>'a','Б'=>'b','В'=>'v','Г'=>'g','Ґ'=>'g','Д'=>'d','Е'=>'e','Є'=>'e','Ё'=>'io','Ж'=>'zh','З'=>'z','И'=>'y','І'=>'i','Й'=>'y','Ї'=>'i','К'=>'k','Л'=>'l','М'=>'m','Н'=>'n','О'=>'o','П'=>'p','Р'=>'r','С'=>'s','Т'=>'t','У'=>'u','Ў'=>'u','Ф'=>'f','Х'=>'h','Ц'=>'ts','Ч'=>'ch','Ш'=>'sh','Щ'=>'shch','Ъ'=>'','Ы'=>'y','Ь'=>'','Э'=>'e','Ю'=>'iu','Я'=>'ia');
    $transB=array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','ґ'=>'g','д'=>'d','е'=>'e','ё'=>'io','є'=>'e','ж'=>'zh','з'=>'z','и'=>'y','і'=>'i','й'=>'y','ї'=>'i','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ў'=>'u','ф'=>'f','х'=>'h','ц'=>'ts','ч'=>'ch','ш'=>'sh','щ'=>'shch','ъ'=>'','ы'=>'y','ь'=>'','э'=>'e','ю'=>'iu','я'=>'ia','&quot;'=>'','&amp;'=>'','µ'=>'u','№'=>'');
//    $content=trim(strip_tags_smart($content));
    $content=strtr($content,$transA);
    $content=strtr($content,$transB);
    $content=preg_replace("/\s+/ums","-",$content);
    $content=preg_replace('/[\-]+/ui','-',$content);
    $content=preg_replace('/[\.]+/u','_',$content);
    $content=preg_replace("/[^a-z0-9\_\-\.]+/umi","",$content);
    $content=str_replace("/[_]+/u","_",$content);
    return $content;
  }
}

abstract class ActionController extends AbstractActionController {
  /**
  * @var View
  */
  protected $view;
  public $access;

  function __construct($params) {
    parent::__construct($params);
    $this->view = View::getInstance();
  }

  /**
  * @desc Перенаправить браузер на URL, который соответствует данному контроллеру и действию
  */
  function Redirect($controller = '', $action = '', $params = array()) {
    $this->RedirectToUrl(FrontController::MakeUrl($controller, $action, $params));
  }

  /**
  * @desc Перенаправить браузер на заданный URL
  */
  function RedirectToUrl($url) {
    header("Location: $url");
    exit;
  }

  static function addMsg($kind = 'success', $msg = '', $title = '') {
    if (isset($_SESSION['toastr'])) {
      $cnt = count($_SESSION['toastr']);
    } else {
      $cnt = 0;
    }
    $_SESSION['toastr'][$cnt]['kind'] = $kind;
    $_SESSION['toastr'][$cnt]['msg'] = $msg;
    $_SESSION['toastr'][$cnt]['title'] = $title;
  }

  function getAdminAccess() {
    $this->admin = Admin::getAuthenticatedInstance();
    if ( !$this->admin ) {
      View::getInstance()->displayLoginForm();
      return false;
//      $this->redirect('admin', '');
    } else {
      if (!isset($_COOKIE[Admin::COOKIE_R])) {
        $this->admin->setCookie();
      }
    }
    $this->access = $this->admin->getAccess();
    $this->view->assign('admin', $this->admin);
    $this->view->assign('access', $this->access);
    $this->view->assign('menu', $this->admin->getMenu());
    return $this->admin;
  }
  function UTF8toCP1251($str){ // by SiMM, $table from http://ru.wikipedia.org/wiki/CP1251
    static $table = array("\xD0\x81" => "\xA8", // Ё
                          "\xD1\x91" => "\xB8", // ё
                          // украинские символы
                          "\xD0\x8E" => "\xA1", // Ў (У)
                          "\xD1\x9E" => "\xA2", // ў (у)
                          "\xD0\x84" => "\xAA", // Є (Э)
                          "\xD0\x87" => "\xAF", // Ї (I..)
                          "\xD0\x86" => "\xB2", // I (I)
                          "\xD1\x96" => "\xB3", // i (i)
                          "\xD1\x94" => "\xBA", // є (э)
                          "\xD1\x97" => "\xBF", // ї (i..)
                          // чувашские символы
                          "\xD3\x90" => "\x8C", // &#1232; (А)
                          "\xD3\x96" => "\x8D", // &#1238; (Е)
                          "\xD2\xAA" => "\x8E", // &#1194; (С)
                          "\xD3\xB2" => "\x8F", // &#1266; (У)
                          "\xD3\x91" => "\x9C", // &#1233; (а)
                          "\xD3\x97" => "\x9D", // &#1239; (е)
                          "\xD2\xAB" => "\x9E", // &#1195; (с)
                          "\xD3\xB3" => "\x9F", // &#1267; (у)
                         );
    $str = preg_replace('#([\xD0-\xD1])([\x80-\xBF])#se',
                        'isset($table["$0"]) ? $table["$0"] :
                         chr(ord("$2")+("$1" == "\xD0" ? 0x30 : 0x70))
                        ',
                        $str
                       );
    $str = str_replace("i", "і", $str);
    $str = str_replace("I", "І", $str);
    return $str;
  }
}

abstract class AjaxActionController extends AbstractActionController {
  # пока тут ничего не требуется, позже можно добавить методы, специфичные для аджаксовых контроллеров действий
  public $result;
  public $lang;
  public $conf;

  function __construct($params) {
    parent::__construct($params);
    $this->result['error'] = false;
    $this->result['status-kind'] = '';
    $this->result['status-title'] = '';
    $this->result['status-msg'] = '';
  }

  function setRequiredFields(array $fields) {
    $this->result['items'] = array();
    $i = 0;
    foreach ($fields as $key) {
      $this->result[$key]['error'] = false;
      $this->result[$key]['status'] = '';
      $this->result['items'][$i] = $key;
      $i++;
    }
  }
  function checkRequiredFields(array $fields = array()) {
    if (count($fields)) {
      $this->setRequiredFields($fields);
    }
    foreach ($this->result['items'] as $field) {
      if (empty($this->params[$field])) {
        $this->result['error'] = true;
        $this->result[$field]['error'] = 'error';
        $this->result[$field]['status'] = $this->conf['req_field'];
      }
    }
  }

  function getAdminAccess() {
    $this->admin = Admin::getAuthenticatedInstance();
    if ( !$this->admin ) {
      throw new NotFoundException($this->conf['error_authorization']);
    } else {

    }
    return $this->admin;
  }

}

/**
* @desc Исключение кидается когда что-либо не найдено - контроллер, действие, какая-то сущность (например, категория с данным id)
*/
class NotFoundException extends Exception {

  public function __construct($message, $code = 0) {
//    ActionController::addMsg('error', $message, 'Error!');
    parent::__construct($message, $code);
  }
  public function __toString() {
    return $this->getMessage();
  }
}

?>