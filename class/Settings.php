<?php

final class Settings {
  private static $instance;
  private $params;

  function __construct() {
    for ($i=0; $i < 4; $i++) {
      $path = str_repeat('../', $i); 
      if (file_exists($path.'conf.php')) {
        require $path.'conf.php';
        break;
      }
    }
  }
  private function __clone() {}

  /**
  * @return Settings
  */
  static function getInstance() {
    if ( !isset(self::$instance) ) {
      self::$instance = new self;
    }
//    echo 'Settings::GetInstance()';
    return self::$instance;
  }
  static function get($name, $key = null) {
    if ( !isset($this) || !$this instanceOf self ) {
      return self::getInstance()->Get2($name, $key);
    }
    if ( isset($this->params[$name]) ) {
      if ($key) {
        return $this->params[$name][$key];
      }
      return $this->params[$name];
    } else {
      throw new Exception("Undefined setting: $name");
    }
  }
  /**
  * Получение значения параметра конфигурации
  *
  */
  function get2($name, $key) {
    if ( !isset($this) || !$this instanceOf self ) {
      return self::getInstance()->get2($name, $key);
    }
    if ( isset($this->params[$name]) ) {
      if ($key) {
        return $this->params[$name][$key];
      }
      return $this->params[$name];
    } else {
      throw new Exception("Undefined setting: $name");
    }
  }

  /** 
  *  транслит почти ISO 9 
  *  https://ru.wikipedia.org/wiki/ISO_9
  */
    public static function translit($content){
    $transA=array('А'=>'a','Б'=>'b','В'=>'v','Г'=>'g','Ґ'=>'g','Д'=>'d','Е'=>'e','Є'=>'ye','Ё'=>'yo','Ж'=>'zh','З'=>'z','И'=>'i','І'=>'i','Й'=>'j','Ї'=>'yi','К'=>'k','Л'=>'l','М'=>'m','Н'=>'n','О'=>'o','П'=>'p','Р'=>'r','С'=>'s','Т'=>'t','У'=>'u','Ў'=>'u','Ф'=>'f','Х'=>'x','Ц'=>'cz','Ч'=>'ch','Ш'=>'sh','Щ'=>'shh','Ъ'=>'','Ы'=>'y','Ь'=>'','Э'=>'e','Ю'=>'yu','Я'=>'ya');
    $transB=array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','ґ'=>'g','д'=>'d','е'=>'e','є'=>'ye','ё'=>'yo','ж'=>'zh','з'=>'z','и'=>'i','і'=>'i','й'=>'j','ї'=>'yi','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ў'=>'u','ф'=>'f','х'=>'x','ц'=>'cz','ч'=>'ch','ш'=>'sh','щ'=>'shh','ъ'=>'','ы'=>'y','ь'=>'','э'=>'e','ю'=>'yu','я'=>'ya','&quot;'=>'','&amp;'=>'','µ'=>'u','№'=>''," "=> "-", "."=> "-", "/"=> "-", "-"=>"-", "*"=>"-",")"=>"-","("=>"-","]"=>"-","["=>"-","%"=>"-", "#"=>"-","+"=>"-", ","=>"-", "?"=>"-", "&"=>"", "|"=>"-", "="=>"-", "ё"=>"e","<"=>"-", ">"=>"-", "№"=>"-", "!"=>"-","«"=>"-","»"=>"-");
    $content=strtr($content,$transA);
    $content=strtr($content,$transB);
    $content=preg_replace("/\s+/ums","-",$content);
    $content=preg_replace('/[\-]+/ui','-',$content);
    $content=preg_replace('/[\.]+/u','_',$content);
    $content=preg_replace("/[^a-z0-9\_\-\.]+/umi","",$content);
    $content=str_replace("/[_]+/u","_",$content);
    $content=str_replace("/[-]+/u","-",$content);
    $content=strtolower($content);
    $content=rtrim($content,'-');
    return $content;
  }

}

?>