<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>{$ctrl.title}</h2>
        {include file="admin/include/breadcrumbs.tpl"}
    </div>
    <div class="col-sm-4">
        <div class="title-action">
          {makelink c=$ctrl.name a=new class="btn btn-primary" icon="file" text=$conf.create}
          {makelink c=$ctrl.name a=list class="btn btn-primary" icon="list" text=$conf.list}
        </div>
    </div>
</div>
<div class="row m-t">
    <div class="col-lg-12">
      <div class="panel">
        {element type="lang-tabs"}

        <div class="panel-body">
        <form onsubmit="return false" class="form-horizontal" enctype="multipart/form-data">
          <input type="hidden" name="id" value="{$item->getId()}">
          <div class="tab-content">
              {element type="lang-check-page"}
              {foreach from=$pub_langs item=plang}
                {assign var=plid value=$plang->getId()}
                {assign var=plname value=$plang->getSname()}
                  <div id="tab-{$plname}" lang="{$plname}" class="tab-pane">
                    {element type="input" label=$conf.name name="name{$plid}" value=$item->getName($plid) require=1}
                    {element type="input" label=$conf.short_name name="shortname{$plid}" value=$item->getShortName($plid) require=1}
                  </div>
              {/foreach}
              
              <div id="tab-all" lang="all" class="tab-pane">
                {element type="input" label="URL" name="sname" value=$item->getSname() require=1}
              </div>
              
              {element type="dashed"}
              
              {element type="group_button" btn_cancel=true btn_action="update" btn_title=$conf.update f2a=false}
          </div>
        </form>
        </div>

      </div>
    </div>
</div>
