<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>{$ctrl.title}</h2>
        {include file="admin/include/breadcrumbs.tpl"}
    </div>
    <div class="col-sm-4">
        <div class="title-action">
          {makelink c=$ctrl.name a=new class="btn btn-primary" icon="file" text=$conf.create}
        </div>
    </div>
</div>
<div class="row m-t">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
        <div class="ibox-content">

            <table class="table table-hover" id="table-dnd">
                <thead>
                <tr class="nodrop nodrag">
                    <th>#</th>
                    <th>URL</th>
                    <th>{$conf.name}</th>
                    <th>{$conf.short_name}</th>
                    <th class="three-btn"></th>
                </tr>
                </thead>
                <tbody>
                {foreach from=$list item=item name=foritem}
                <tr id="{$ctrl.name}_{$item->getId()}" lang="{$item->getId()}">
                    <td>{$smarty.foreach.foritem.iteration}</td>
                    <td>
                      {makelink c=$ctrl.name a=edit id=$item->getId() text=$item->getSname()}
                    </td>
                    <td>
                      {makelink c=$ctrl.name a=edit id=$item->getId() text=$item->getName($lang_content)}
                    </td>
                    <td>
                      {makelink c=$ctrl.name a=edit id=$item->getId() text=$item->getShortname($lang_content)}
                    </td>
                    <td>
                        {if $item->getActive() == 0}
                            <a class="pull-right m-r" onclick='ajax_query("{$ctrl.name}", "activelang", "{$item->getId()}")' id="active-{$item->getId()}" rel="tooltip" data-toggle="tooltip" data-placement="top" title="{$translate->val('publish')}"><i class="fa fa-eye-slash" id="acticon-{$item->getId()}"></i></a>
                        {else}
                            <a class="pull-right m-r" onclick='ajax_query("{$ctrl.name}", "activelang", "{$item->getId()}")' id="active-{$item->getId()}" rel="tooltip" data-toggle="tooltip" data-placement="top" title="{$translate->val('stop_public')}"><i class="fa fa-eye" id="acticon-{$item->getId()}"></i></a>
                        {/if}                    
                      {makelink c=$ctrl.name a=edit id=$item->getId() class="pull-right m-r" icon="pencil" alt=$conf.edit tooltip="top"}
                    </td>
                </tr>
                {/foreach}
                </tbody>
            </table>
        </div>
    </div>
  </div>
</div>
