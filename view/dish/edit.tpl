<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>{$ctrl.title}</h2>
        {include file="admin/include/breadcrumbs.tpl"}
    </div>
    <div class="col-sm-4">
        <div class="title-action">
          {makelink c=$ctrl.name a=new class="btn btn-primary" icon="file" text=$conf.create}
          {makelink c=$ctrl.name a=list class="btn btn-primary" icon="list" text=$conf.list}
        </div>
    </div>
</div>
<div class="row m-t">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
            <form onsubmit="return false" class="form-horizontal">
              <input type="hidden" name="id" id="id" value="{$item->getId()}">
              
              {element type="input" label="Название" name="name" value=$item->getName() require=1}
              {element type="input" label="Цена" name="price"  value=$item->getPrice()}
              {element type="select" label="Категория" name="category" value=$item->getCategoryId() list=$cats require=1}
              {element type="textarea" label="Описание" name="desc"  value=$item->getDesc()}
              {element type="input" label="Вес" name="weight"  value=$item->getWeight()}

              {element type="group_button" btn_cancel=true btn_action="update" btn_title=$conf.update f2a=true}
            </form>
            </div>
        </div>
    </div>
</div>

