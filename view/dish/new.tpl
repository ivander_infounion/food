<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>{$ctrl.title}</h2>
        {include file="admin/include/breadcrumbs.tpl"}
    </div>
    <div class="col-sm-4">
        <div class="title-action">
          {makelink c=$ctrl.name a=list class="btn btn-primary" icon="list" text=$conf.list}
        </div>
    </div>
</div>
<div class="row m-t">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
            <form onsubmit="return false" class="form-horizontal">
              
              {element type="input" label="Название" name="name" require=1}
              {element type="input" label="Цена" name="price"}
              {element type="select" label="Категория" name="category" list=$cats require=1}
              {element type="textarea" label="Описание" name="desc"}
              {element type="input" label="Вес" name="weight"}

              {element type="group_button" btn_cancel=true btn_action="add" btn_title=$conf.add f2a=true}
            </form>
            </div>
        </div>
    </div>
</div>

