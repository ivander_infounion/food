<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>{$ctrl.title}</h2>
        {include file="admin/include/breadcrumbs.tpl"}
    </div>
    <div class="col-sm-4">
        <div class="title-action">
          {makelink c=$ctrl.name a=new class="btn btn-primary" icon="file" text=$conf.create}
        </div>
    </div>
</div>
<div class="row m-t">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <div class="row">
                <div class="col-sm-4 m-b-xs">
                </div>
                <div class="col-sm-4 m-b-xs">

                </div>
                <div class="col-sm-4">
                <form onsubmit="return false">
                    <div class="input-group">
                      <input type="text" placeholder="{$conf.search}" id="find" value="{$findtext}" class="input-sm form-control">
                      <span class="input-group-btn">
                        <button type="submit" class="btn btn-sm btn-primary" onclick="searchTranslate($('#find').val())">{$conf.find}</button>
                      </span>
                    </div>
                </form>
                </div>
            </div>
<script type="text/javascript">
function searchTranslate(text) {
  window.location.href = "{$d}admin/translate/list/?text="+text;
}
</script>
            <table class="table table-hover dataTable">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{$conf.group}</th>
                    <th class="sorting" onclick="">{$conf.key}</th>
                    <th>{$entlang->getName($lang_content)}</th>
                    <th class="two-btn"></th>
                </tr>
                </thead>
                <tbody>
                {foreach from=$list item=item name=foritem}
                <tr id="{$ctrl.name}_{$item->getId()}">
                    <td>{$smarty.foreach.foritem.iteration}</td>
                    <td>
                      {makelink c=$ctrl.name a=edit id=$item->getId() text=$item->getGroup()}
                    </td>
                    <td>
                      {makelink c=$ctrl.name a=edit id=$item->getId() text=$item->getName()}
                    </td>
                    <td>
                      {makelink c=$ctrl.name a=edit id=$item->getId() text=$item->getValue($entlang->getId())}
                    </td>
                    <td>
                      {makelink c=$ctrl.name a=edit id=$item->getId() class="pull-right m-r" icon="pencil" alt=$conf.edit tooltip="top"}
                    </td>
                </tr>
                {/foreach}
                </tbody>
            </table>
            {element type="pager" }
        </div>
    </div>
  </div>
</div>
