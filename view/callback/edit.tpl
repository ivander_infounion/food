<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>{$ctrl.title}</h2>
        {include file="admin/include/breadcrumbs.tpl"}
    </div>
    <div class="col-sm-4">
        <div class="title-action">
          {makelink c=$ctrl.name a=list  formtype=$formtype class="btn btn-primary" icon="list" text=$conf.list}
        </div>
    </div>
</div>
<div class="row m-t">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
            <form onsubmit="return false" class="form-horizontal" enctype="multipart/form-data">
              <input type="hidden" name="id" value="{$item->getId()}">
              <input type="hidden" name="status" value="0">
              {* {$item->printMetaFields()} *}
              {foreach from=$item->getMetaFieldsArray() item=field key=fname name=metafields}
                {if $field.type = 'input'}
                  {element type='input' label=$field.fieldname value=$field.data name="meta_{$field.name}"}
                {elseif $field.type = 'textarea'}
                  {element type='textarea' label=$field.fieldname value=$field.data name="meta_{$field.name}"}
                {/if}
              {/foreach}
              {element type='static-text' label="Заявка принята" value=$item->getCreated('d.m.y H:i')}
              {element type='select' name="worker" label="Обработанно сотрудником" list=$workers value=$item->getWorkerId() empty_opt=true}
              {if $item->getWorkerId()!=0}
                {element type='static-text' label="Заявка обработанна" value=$item->getSuccess('d.m.y H:i')}
              {/if}
              {element type="group_button" btn_cancel=true btn_action="update" btn_title=$conf.update f2a=false}
            </form>
            </div>
        </div>
    </div>
</div>

