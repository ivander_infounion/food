{if $countpages > 1}
  <div class="row">
    <div class="col-sm-12">
      <div class="btn-group pull-right m-r">
      {if $currentpage == 1}
        {makelink c=$ctrl.name a=$action.name class="btn btn-white disabled" icon="chevron-left" p=1 sid=$sid cid=$cid}
      {else}
        {makelink c=$ctrl.name a=$action.name class="btn btn-white" icon="chevron-left" p=($currentpage-1) sid=$sid cid=$cid}
      {/if}
      {section name=pager start=1 loop=$countpages+1}
        {if $currentpage == $smarty.section.pager.index}{assign var=btnclass value="btn btn-white  active"}
        {else}{assign var=btnclass value="btn btn-white"}{/if}
        {makelink c=$ctrl.name a=$action.name class=$btnclass text=$smarty.section.pager.index p=$smarty.section.pager.index sid=$sid cid=$cid}
      {/section}
      {if $currentpage == $countpages}
        {makelink c=$ctrl.name a=$action.name class="btn btn-white disabled" icon="chevron-right" p=$currentpage sid=$sid cid=$cid}
      {else}
        {makelink c=$ctrl.name a=$action.name class="btn btn-white" icon="chevron-right" p=($currentpage+1) sid=$sid cid=$cid}
      {/if}

      </div>
    </div>
  </div>
{/if}