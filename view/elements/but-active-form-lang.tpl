<div class="form-group" id="group-active">
  <div class="col-sm-12">
  {if $obj}
    {if $obj->getActive() == 0}
    <a class="btn btn-white btn-sm pull-right {$class}" onclick="ajax_query('lang', 'active', {'{'}id:'{$obj->getId()}', ctrl:'{$obj->Name()}' })" id="active-{$obj->getId()}" rel="tooltip" data-toggle="tooltip" data-placement="top" >
      <i class="fa fa-upload" id="acticon-big-{$obj->getId()}"></i> <span id="act-text-{$obj->getId()}">{$translate->val('publish')}</span>
    </a>
    {else}
    <a class="btn btn-white btn-sm pull-right {$class}" onclick="ajax_query('lang', 'active', {'{'}id:'{$obj->getId()}', ctrl:'{$obj->Name()}' })" id="active-{$obj->getId()}" rel="tooltip" data-toggle="tooltip" data-placement="top" >
      <i class="fa fa-check" id="acticon-big-{$obj->getId()}"></i> <span id="act-text-{$obj->getId()}">{$translate->val('stop_public')}</span>
    </a>
    {/if}
  {/if}
  </div>
</div>
