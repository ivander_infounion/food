<div class="form-group" id="group-{$name}">
  <label class="col-sm-{$col_label} control-label">{$label}</label>
  <div class="col-sm-{12-$col_label}">
    <p class="form-control-static">{$value}</p>
  </div>
</div>