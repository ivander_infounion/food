<div class="form-group" id="group-{$name}">
  <label class="col-sm-{$col_label} control-label">{if $require}* {/if}{$label}</label>
  <div class="col-sm-{12-$col_label}">
    {if $addon_first}
    <div class="input-group">
      <span class="input-group-addon">{$addon_first}</span>
    {/if}
    <textarea rows="{if $size}{$size}{else}5{/if}" class="form-control" name="{$name}" id="{$name}">{$value}</textarea>
    {if $addon_first}</div>{/if}
    <span class="help-block m-b-none" id="help-{$name}">{$help}</span>
  </div>
</div>