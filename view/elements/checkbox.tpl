<div class="form-group">
    <label class="col-sm-{$col_label} control-label">{$label}</label>
    <div class="col-sm-{12-$col_label}">
      <label class="checkbox i-checks">
        <input type="checkbox" class="cmscheckbox" value="1" id="{$name}" name="{$name}"{if $value==1} checked="checked"{/if}>
        <i></i> {$inplabel}
      </label>
    </div>
</div>
