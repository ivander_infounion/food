<div class="form-group" id="group-{$name}">
  <label class="col-sm-{$col_label} control-label">{if $require}* {/if}{$label}</label>
  <div class="col-sm-{12-$col_label}">
    <div class="row">
      <div class="col-md-3">
        <div class="input-group date" id="datepicker-{$name}" data-date="{$value}" data-date-format="dd.mm.yyyy">
          <input type="text" class="form-control" name="{$name}" id="{$name}"{if $time} value="{$value|date_format:'d.m.Y H:i'}"{else} value="{$value|date_format:'d.m.Y'}"{/if} style="min-width:140px;">
          <span class="input-group-addon cursor-pointer" onclick="$('#{$name}').datetimepicker('show');"><i class="fa fa-calendar"></i></span>
        </div>
        <span class="help-block m-b-none" id="help-{$name}">{$help}</span>
      </div>
    </div>
  </div>
</div>
