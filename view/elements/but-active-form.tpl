<div class="form-group" id="group-active">
  <div class="col-sm-12">
    {if $obj->getActive() == 0}
    <a class="btn btn-white btn-sm pull-right {$class}" onclick='ajax_query("{$ctrl.name}", "active", "{$obj->getId()}")' id="active-{$obj->getId()}" rel="tooltip" data-toggle="tooltip" data-placement="top" >
      <i class="fa fa-upload" id="acticon-big-{$obj->getId()}"></i> <span id="act-text-{$obj->getId()}">{$translate->val('publish')}</span>
    </a>
    {else}
    <a class="btn btn-white btn-sm pull-right {$class}" onclick='ajax_query("{$ctrl.name}", "active", "{$obj->getId()}")' id="active-{$obj->getId()}" rel="tooltip" data-toggle="tooltip" data-placement="top" >
      <i class="fa fa-check" id="acticon-big-{$obj->getId()}"></i> <span id="act-text-{$obj->getId()}">{$translate->val('stop_public')}</span>
    </a>
    {/if}
  </div>
</div>
