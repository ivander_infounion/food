{assign var=checkbox_names value=$names|explode:', '}
{assign var=checkbox_labels value=$labels|explode:', '}
{assign var=number_of_elements value={$checkbox_names|@count} }
{assign var=width value=10/$number_of_elements}
{assign var=width value=$width|string_format:"%d"}
{assign var=width value=2}

<div class="form-group" id="group-{$name}">
    <label class="col-sm-2 control-label">{$label}</label>
    <div class="col-sm-10">
    {foreach from=$checkbox_names item=name name=cbn}
  	{assign var=iter value=$smarty.foreach.cbn.index} 
	    <div class="col-sm-{$width}">
	    	<label class="col-sm-8 control-label">{$checkbox_labels.$iter}</label>
	    	<div class="col-sm-4">
		      <label class="checkbox i-checks">
						{assign var=check value={getvalue var=check name=$name item=$item truevalue=1 returntrue="checked='checked'"} }
		        <input type="checkbox" class="cmscheckbox" value="1" id="{$name}" name="{$name}" {$check}>
		        <i></i> {$inplabel}
		      </label>
	    	</div>
	    </div>
    {/foreach}
    
    </div>
    
</div>
