<div class="form-group" id="group-{$name}">
  <label class="col-sm-{$col_label} control-label">{if $require}* {/if}{$label}</label>
  <div class="col-sm-{12-$col_label}">
  {if $size}
    <div class="row">
      <div class="col-md-{$size}">
  {/if}
        <select class="form-control" name="{$name}" id="{$name}">
          {if $empty_opt}
            <option value="0"></option>
          {/if}
          {foreach from=$list key=key item=listitem}
            <option value="{$key}"{if $key==$value} selected="selected"{/if}>{$listitem.$lang}</option>
          {/foreach}
        </select>
        <span class="help-block m-b-none" id="help-{$name}">{$help}</span>
  {if $size}
      </div>
    </div>
  {/if}
  </div>
</div>