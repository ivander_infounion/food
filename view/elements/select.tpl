<div class="form-group" id="group-{$name}">
  <label class="col-sm-{$col_label} control-label">{if $require}* {/if}{$label}</label>
  <div class="col-sm-{12-$col_label}">
  {if $size}
    <div class="row">
      <div class="col-md-{$size}">
  {/if}
        <select class="form-control{if $chosen} chosen-select{/if}" name="{$name}" id="{$name}"{if $onchange} onchange="{$onchange}"{/if}>
          {if $empty_opt}
            <option value="0"></option>
          {/if}
          {foreach from=$list item=listitem}
            <option value="{$listitem->getId()}"{if $listitem->getId()==$value} selected="selected"{/if}>{$listitem->getName($lang_content)}</option>
          {/foreach}
        </select>
        <span class="help-block m-b-none" id="help-{$name}">{$help}</span>
  {if $size}
      </div>
    </div>
  {/if}
  </div>
</div>