<div class="form-group" id="group-{$name}">
  <label class="col-sm-{$col_label} control-label">{if $require}* {/if}{$label}</label>
  <div class="col-sm-{12-$col_label}">
      {assign var=file value=''}
      {if $obj}
        {if $obj->getPict($pictname) != ''}
          {assign var=file value=$obj->getRealPathPict($pictname)}
        {/if}
      {/if}

      <img src="{$d}{$file}" id="img-{$name}" style="width:150px;" class="m-b{if $file==''} hide{/if}" alt="">
  {if $file==''}
      <a class="btn btn-outline btn-success btn-sm pull-top hide" id="btn-clear-img-{$name}" onclick='showConfirm(false,"clearImage","{$name}","{$translate->val('remove')} {$translate->val('pict')}?")' rel="tooltip" data-toggle="tooltip" data-placement="right" title="{$translate->val('remove')} {$translate->val('pict')|strtolower}"><i class="fa fa-remove"></i></a>
  {else}
      <a class="btn btn-outline btn-success btn-sm pull-top" id="btn-clear-img-{$name}" onclick="showConfirm('{$ctrl.name}','deleteimg',{'{'}id:'{$obj->getId()}',pictname:'{$name}'},'{$translate->val('remove')} {$translate->val('pict')}?')" rel="tooltip" data-toggle="tooltip" data-placement="right" title="{$translate->val('remove')} {$translate->val('pict')|strtolower}"><i class="fa fa-remove"></i></a>
  {/if}
    <div>
      <label title="{$translate->val('choose_file')}" for="{$name}" class="btn btn-primary btn-sm">
        <input type="file" class="hide" name="{$name}" id="{$name}" onchange="openImage(this)" accept="image/*">
        {$translate->val('choose')} {$translate->val('pict')|strtolower}
      </label>
    </div>

        <span class="help-block m-b-none" id="help-{$name}">{$help}</span>
  </div>
</div>
