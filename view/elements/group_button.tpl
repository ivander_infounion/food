<div class="form-group">
    <div class="col-sm-12 col-sm-offset-{$col_label}">
      {if $btn_cancel}
        <button class="btn btn-white" type="button" onclick="cancelForm()">{$translate->val('cancel')}</button>
      {/if}
      {if $f2a}
        <button class="btn btn-primary" type="submit" onclick="ajax_query('{$ctrl.name}','{$btn_action}',form2array(this.form))">{$btn_title}</button>
      {else}
        <button class="btn btn-primary" type="submit" onclick="ajax_query('{$ctrl.name}','{$btn_action}',this.form)">{$btn_title}</button>
      {/if}
    </div>
</div>
