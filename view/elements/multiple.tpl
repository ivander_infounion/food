<div class="form-group" id="group-{$name}">
  <label class="col-sm-{$col_label} control-label">{if $require}* {/if}{$label}</label>
  <div class="col-sm-{12-$col_label}">
  {if $size}
    <div class="row">
      <div class="col-md-{$size}">
  {/if}
        <select name="{$name}[]" id="{$name}" class="chosen-select" multiple style="width:350px;" tabindex="4" data-placeholder="{$translate->val('choose')}">
          {if $empty_opt}
            <option value="0"></option>
          {/if}
          {foreach from=$list item=listitem}
            {assign var=selvalue value=$listitem->getId()|in_array:$select_vals}
            <option value="{$listitem->getId()}"{if $selvalue} selected="selected"{/if}>{$listitem->getName($lang_content)}</option>
          {/foreach}
        </select>
        <span class="help-block m-b-none" id="help-{$name}">{$help}</span>
  {if $size}
      </div>
    </div>
  {/if}
  </div>
</div>