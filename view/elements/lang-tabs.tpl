<div class="panel-heading">
    <div class="panel-options">
        <ul class="nav nav-tabs">
          {foreach from=$langs item=ltab name=forltab}
            {assign var=ltabid value=$ltab->getId()}
            <li id="tab-lang-{$ltab->getId()}"{if $smarty.foreach.forltab.iteration==1} class="active"{/if}{if !$langItems.$ltabid} style="display:none;"{/if}>
              <a data-toggle="tab" href="#tab-{$ltab->getId()}" onclick="tabLang('{$ltab->getSname()}')" tab-lang="{$ltab->getSName()}">{$ltab->getFlag()} {$ltab->getName($lang)}</a>
            </li>
          {/foreach}
        </ul>
    </div>
</div>
<script>
  function tabLang(lang) {
      $(".tab-pane[lang!='all']").removeClass('active');
      $(".tab-pane[lang='all']").addClass('active');
      $(".tab-pane[lang='"+lang+"']").addClass('active');
  }
</script>