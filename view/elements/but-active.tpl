{if $obj->getActive() == 0}
<a class="{$class}" onclick='ajax_query("{$ctrl.name}", "active", "{$obj->getId()}")' id="active-{$obj->getId()}" rel="tooltip" data-toggle="tooltip" data-placement="top" title="{$translate->val('publish')}">
  <i class="fa fa-eye-slash" id="acticon-{$obj->getId()}"></i>
</a>
{else}
<a class="{$class}" onclick='ajax_query("{$ctrl.name}", "active", "{$obj->getId()}")' id="active-{$obj->getId()}" rel="tooltip" data-toggle="tooltip" data-placement="top" title="{$translate->val('stop_public')}">
  <i class="fa fa-eye" id="acticon-{$obj->getId()}"></i>
</a>
{/if}
