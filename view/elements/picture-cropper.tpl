{if $previewtype}
  {assign var="pictname_alt" value=$previewtype}
{/if}

<div class="form-group" id="group-{$name}">
  <label class="col-sm-{$col_label} control-label">{if $require}* {/if}{$label}
    <br>
    <label title="{$translate->val('edit')}" class="btn btn-primary btn-sm" onclick="$('.modal-body-{$name}').slideToggle()">
      {$translate->val('edit')}
    </label>
    {if $obj}
      {if $obj->getPict($pictname) != ''}
        <br>
        <label title="{$translate->val('remove')} {$translate->val('pict')}?" style="margin:4px" class="btn btn-primary btn-sm" id="btn-clear-img-{$name}" onclick="showConfirm('{$ctrl.name}','deleteimg',{'{'}id:'{$obj->getId()}',pictname:'{$name}'},'{$translate->val('remove')} {$translate->val('pict')}?')">
            {$translate->val('remove')} {$translate->val('pict')}?
        </label>
      {/if}
    {/if}
  </label>
  <div class="col-sm-{12-$col_label}">
      {assign var=file value=''}
      {if $obj}
        {if $obj->getPict($pictname) != ''}
          {assign var=file value=$obj->getRealPathPict($pictname)}
        {/if}
      {/if}
      <div class="row">
        <div class="container col-md-12" id="crop-avatar_{$name}">
          <div class="col-sm-3">
            <div class="old_preview">
              {if $obj->getPict($pictname) != ''}
               <img src="{$d}{$obj->getRealPathPreview()}">
              {else}
                <span>Фото не загружено</span>
              {/if}
            </div>
          </div>
          <div class="modal-body modal-body-{$name} col-sm-10">
            <div class="avatar-body">
              {if $obj}
                {if $obj->getPict($pictname) != ''}
                  <!-- Upload image and data -->
                  <div class="avatar-upload avatar-upload_{$name}">
                    <input type="hidden" class="avatar-src avatar-src_{$name}" name="avatar_src">
                    <input type="hidden" class="avatar-data avatar-data_{$name}" name="avatar_data">
                    <label for="avatarInput">Загрузить фото</label>
                    <input type="file" class="avatar-input avatar-input_{$name}" id="avatarInput_{$name}" name="{$name}">
                  </div>
                  <!-- Crop and preview -->
                  <div class="row">
                    <div class="col-md-9">
                      <div class="avatar-wrapper avatar-wrapper_{$name}"></div>
                    </div>
                    <div class="col-md-3">
                      Новая фотография
                      <div class="avatar-preview avatar-preview_{$name} preview-lg"></div>
                      Оригинал фотографии
                      <div class=" preview-lg original_preview avatar-view_{$name}">
                        <img src="{$d}{$obj->getRealPathPict()}">
                      </div>
                    </div>
                  </div>
                  <div class="row avatar-btns_{$name} avatar-btns">
                    <label title="{$translate->val('crop_file')}" for="{$name}_data" class="btn btn-primary btn-sm avatar-save_{$name} ">
                      <input type="hidden" name="{$name}_data" id="{$name}_data" value=''>
                      {$translate->val('crop_file')}
                    </label>
                  </div>
                {else}
                    <label for="avatarInput">Загрузить картинку</label>
                    <input type="file" class="avatar-input avatar-input_{$name}" id="avatarInput_{$name}" name="{$name}">
                {/if}
              {else}
                  <label for="avatarInput">Загрузить картинку</label>
                  <input type="file" class="avatar-input avatar-input_{$name}" id="avatarInput_{$name}" name="{$name}">
              {/if}
            </div>
          </div>
        </div>
      </div>   
    </div>
  </div>



  
<script type="text/javascript">

var ratio = {if $obj}{$obj->getPreviewSize('w',$name)}/{$obj->getPreviewSize('h',$name)}{/if};


$(document).ready(function(){

  function CropAvatar($element) {

    this.$avatarView = $('.avatar-view_'+"{$name}");
    this.$avatar = this.$avatarView.find('img');
    this.$avatarModal = $('#avatar-modal_'+"{$name}");
    this.$avatarForm = $('.avatar-form_'+"{$name}");
    this.$avatarUpload = $('.avatar-upload_'+"{$name}");
    this.$avatarSrc = $('.avatar-src_'+"{$name}");
    this.$avatarData = $('.avatar-data_'+"{$name}");
    this.$avatarInput = $('#avatarInput_'+"{$name}");
    this.$avatarSave = $('.avatar-save_'+"{$name}");
    this.$avatarBtns = $('.avatar-btns_'+"{$name}");
    this.$avatarWrapper = $('.avatar-wrapper_'+"{$name}");
    this.$avatarPreview = $('.avatar-preview_'+"{$name}");
    this.init();
  }

  CropAvatar.prototype = {
    constructor: CropAvatar,
    support: {
      fileList: !!$('<input type="file">').prop('files'),
      blobURLs: !!window.URL && URL.createObjectURL,
      formData: !!window.FormData
    },

    init: function () {
      this.support.datauri = this.support.fileList && this.support.blobURLs;
      this.initModal();
      this.addListener();
      this.click();
    },

    addListener: function () {

      $('.avatar-view_'+"{$name}").on('click', $.proxy(this.click, this));
      $('#avatarInput_'+'{$name}').on('change',$.proxy(this.change, this));
      $('.avatar-save_'+'{$name}').on('click', $.proxy(this.submit, this));
    },

    initModal: function () {
      this.$avatarModal.modal({
        show: false
      });
    },

    initPreview: function () {
      var url = this.$avatar.attr('src');
      this.$avatarPreview.html('<img src="' + url + '">');
    },

    click: function () {
      this.initPreview();
      this.url = this.$avatar.attr('src');
      this.startCropper();
    },

    change: function () {
      var files;
      var file;
      if (this.support.datauri) {
        files = this.$avatarInput.prop('files');

        if (files.length > 0) {
          file = files[0];

          if (this.isImageFile(file)) {
            if (this.url) {
              URL.revokeObjectURL(this.url); // Revoke the old one
            }

            this.url = URL.createObjectURL(file);
            this.startCropper();
          }
        }
      } else {
        file = this.$avatarInput.val();

        if (this.isImageFile(file)) {
          this.syncUpload();
        }
      }
    },

    submit: function (e) {
      let data = this.$img.cropper("getData");
      let inp_id = this.$avatarSave.attr('for');
      data = JSON.stringify(data).replace(/"/g, "'");
      $('#'+inp_id).val(data);
      $('button[type="submit"]').click()
    },

    rotate: function (e) {
      var data;
      if (this.active) {
        data = $(e.target).data();

        if (data.method) {
          this.$img.cropper(data.method, data.option);
        }
      }
    },

    isImageFile: function (file) {
      if (file.type) {
        return /^image\/\w+$/.test(file.type);
      } else {
        return /\.(jpg|jpeg|png|gif)$/.test(file);
      }
    },

    startCropper: function () {
      var _this = this;

      if (this.active) {
        this.$img.cropper('replace', this.url);
      } else {
        this.$img = $('<img src="' + this.url + '">');
        this.$avatarWrapper.empty().html(this.$img);
        this.$img.cropper({
          viewMode: 0,
          dragMode: 'move',
          autoCropArea: 0.65,
          restore: false,
          guides: false,
          highlight: false,
          cropBoxMovable: false,
          cropBoxResizable: false,
          aspectRatio: ratio,
          preview: this.$avatarPreview.selector,
          crop: function (e) {
            var json = [
                  '{ "x":' + e.x,
                  '"y":' + e.y,
                  '"height":' + e.height,
                  '"width":' + e.width,
                  '"rotate":' + e.rotate + '}'
                ].join();

            _this.$avatarData.val(json);
          }
        });

        this.active = true;
      }

      this.$avatarModal.one('hidden.bs.modal', function () {
        _this.$avatarPreview.empty();
        _this.stopCropper();
      });
    },

    stopCropper: function () {
      if (this.active) {
        this.$img.cropper('destroy');
        this.$img.remove();
        this.active = false;
      }
    },

    

    syncUpload: function () {
      this.$avatarSave.click();
    },

    submitStart: function () {
      this.$loading.fadeIn();
    },

    submitDone: function (data) {
      console.log(data);

      if ($.isPlainObject(data) && data.state === 200) {
        if (data.result) {
          this.url = data.result;

          if (this.support.datauri || this.uploaded) {
            this.uploaded = false;
            this.cropDone();
          } else {
            this.uploaded = true;
            this.$avatarSrc.val(this.url);
            this.startCropper();
          }

          this.$avatarInput.val('');
        } else if (data.message) {
          this.alert(data.message);
        }
      } else {
        this.alert('Failed to response');
      }
    },

    submitFail: function (msg) {
      this.alert(msg);
    },

    submitEnd: function () {
      this.$loading.fadeOut();
    },

    alert: function (msg) {
      var $alert = [
            '<div class="alert alert-danger avatar-alert alert-dismissable">',
              '<button type="button" class="close" data-dismiss="alert">&times;</button>',
              msg,
            '</div>'
          ].join('');

      this.$avatarUpload.after($alert);
    }
  };

var ca = new CropAvatar($('#crop-avatar_{$name}'));
});


</script>
