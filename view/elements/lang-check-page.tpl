{if $lcheck_pub_langs->count() > 1}
<div class="form-group">
    <label class="col-sm-{$col_label} control-label">{$translate->val('lang_ver')}</label>
    <div class="col-sm-{12-$col_label-3}">
    {foreach from=$lcheck_pub_langs item=lcheck name=forlcheck}
    {if $lcheck->getId() > 1}
      {assign var=lcheckid value=$lcheck->getId()}
      <label class="checkbox-inline i-checks">
        <input type="checkbox" class="showlang" data-id="{$lcheckid}" value="1" id="showlang{$lcheckid}" name="showlang{$lcheckid}"{if $langItems.$lcheckid} checked="checked"{/if}>
        <i></i> {$lcheck->getName($lang)}
      </label>
    {/if}
    {/foreach}
    </div>
    {if $langItems && $btn_active}
    <div class="col-sm-3">
      {foreach from=$langs item=ltab name=forltab}
        {assign var=plid value=$ltab->getId()}
        {assign var=plname value=$ltab->getSname()}
          <div id="tab-{$plname}" lang="{$plname}" class="tab-pane">
            {element type="but-active-form-lang" actlang=1 obj=$langItems.$plid}
          </div>
      {/foreach}
    </div>
    {/if}
</div>
{/if}
<select name="lang[]" id="lang" multiple="multiple" class="hide">
{foreach from=$lcheck_pub_langs item=lcheck}
  {assign var=lcheckid value=$lcheck->getId()}
  <option value="{$lcheckid}"{if $langItems.$lcheckid} selected="selected"{/if}></option>
{/foreach}
</select>
