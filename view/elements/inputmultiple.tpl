{assign var=input_names value=$names|explode:', '}
{assign var=input_labels value=$labels|explode:', '}
{assign var=number_of_elements value={$input_names|@count} }
{assign var=width value=10/$number_of_elements}
{assign var=width value=$width|string_format:"%d"}
<div class="form-group" id="group-{$name}-1">
  <div class="col-sm-2"></div>
  {foreach from=$input_names item=name2 name=multiple_input2}
    {assign var=iter value=$smarty.foreach.multiple_input2.index} 
    <label class="col-sm-{$width} control-label" style="text-align:left">{$input_labels.$iter}</label>
  {/foreach}
</div>
<div class="form-group" id="group-{$name}-2">
  
  <label class="col-sm-2 control-label">{$label}</label>
  {foreach from=$input_names item=name1 name=multiple_input1}
  {assign var=iter value=$smarty.foreach.multiple_input1.index} 
  <div class="col-sm-{$width}">
    <div class="row">
      <div class="col-md-12">
        <input type="text" class="form-control" name="{$name1|lower}" id="{$name1|lower}" value="{getvalue name=$name1 item=$item returnfalse=$returnfalse}" placeholder={$input_labels.$iter}>
      </div>
    </div>
  </div>
  {/foreach}

</div>