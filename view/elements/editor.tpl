<div class="form-group" id="group-{$name}">
  <label class="col-sm-{$col_label} control-label">{if $require}* {/if}{$label}</label>
  <div class="col-sm-{12-$col_label}">
{if $editor && $editor_kind == 'summernote'}
    <div class="summernote" id="{$name}">
      {$value}
    </div>
    <textarea class="hide" name="{$name}"></textarea>
{elseif $editor && $editor_kind == 'ckeditor'}
    <textarea id="{$name}" name="{$name}" class="editor" rows="7">{$value}</textarea>
    <script>
      var roxyFileman = '/admin/fileman/index.html';
      CKEDITOR.replace( '{$name}',{'{'} language: '{$lang}',
                                filebrowserBrowseUrl:roxyFileman,
                                filebrowserUploadUrl:roxyFileman,
                                filebrowserImageBrowseUrl:roxyFileman+'?type=image',
                                filebrowserImageUploadUrl:roxyFileman+'?type=image',
                                skin:'bootstrapck'
//                                skin:'office2013',
      });
    </script>
{/if}
  </div>
</div>