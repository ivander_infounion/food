<div class="panel panel-default">
    <div class="panel-heading">
      <i class="fa fa-photo"></i>
      {$translate->val('gallery')}
    </div>
    <div class="panel-body">
<table id="table-dnd" width="100%">
  <tbody>
    {foreach from=$pictures item=pict name=forpict}
  <tr id="picture_{$pict->getId()}" lang="{$pict->getId()}">
    <td>
      {assign var=pictid value=$pict->getId()}
      <div class="form-group" id="group-pict-{$pict->getId()}">
        <label class="col-sm-{$col_label} control-label">{$translate->val('pict')} {$smarty.foreach.forpict.iteration}</label>
        <div class="col-sm-3">
        {foreach from=$pub_langs item=plang}
          {assign var=plid value=$plang->getId()}
          {assign var=plname value=$plang->getSname()}
          <div class="input-group m-b-xs">
            <span class="input-group-addon">{$flags.$plname}</span>
            <input type="text" class="form-control input-sm" name="file-name{$plname}-{$pictid}" id="file-name{$plname}-{$pictid}" value="{$pict->getName($plid)}">
          </div>
        {/foreach}
        </div>
        <div class="col-sm-{9-$col_label}">
        <a href="{$d}{$pict->getRealPath()}" title="{$pict->getName($lang_content)}" data-gallery>
          <img src="{$d}{$pict->getRealPathPreview()}" height="100px" alt="">
        </a>
      {if $pict->getActive() == 0}
        <a class="btn btn-outline btn-success btn-xs pull-top m-l-sm" id="active-pict-{$pictid}" onclick="activePict('{$ctrl.name}','{$pictid}')" rel="tooltip" data-toggle="tooltip" data-placement="top" title="{$translate->val('publish')}"><i class="fa fa-eye-slash" id="acticon-pict-{$pictid}"></i></a>
      {else}
        <a class="btn btn-outline btn-success btn-xs pull-top m-l-sm" id="active-pict-{$pictid}" onclick="activePict('{$ctrl.name}','{$pictid}')" rel="tooltip" data-toggle="tooltip" data-placement="top" title="{$translate->val('stop_public')}"><i class="fa fa-eye" id="acticon-pict-{$pictid}"></i></a>
      {/if}
        <a class="btn btn-outline btn-success btn-xs pull-top m-l-xs" id="btn-remove-pict-{$pictid}" onclick='showConfirm("{$ctrl.name}","removepict","{$pictid}","{$translate->val('remove')} {$translate->val('pict')|strtolower}?")' rel="tooltip" data-toggle="tooltip" data-placement="top" title="{$translate->val('remove')} {$translate->val('pict')|strtolower}"><i class="fa fa-remove"></i></a>
        </div>
      </div>
    </td>
    <td></td>
  </tr>
    {/foreach}
  </tbody>
</table>
    {if $pictures}
      <div id="blueimp-gallery" class="blueimp-gallery" >
          <div class="slides"></div>
          <h3 class="title"></h3>
          <a class="prev">‹</a>
          <a class="next">›</a>
          <a class="close">×</a>
          <a class="play-pause"></a>
          <ol class="indicator"></ol>
      </div>
    {/if}
      <div id="block-new-pict"></div>
      <div class="form-group" id="group-pict-new">
        <label class="col-sm-{$col_label} control-label"></label>
        <div class="col-sm-{12-$col_label}" id="block-images">

            <label title="{$translate->val('choose_file')}" for="files" class="btn btn-primary btn-sm">
              <input type="file" class="hide" name="files[]" id="files" multiple onchange="openImages(this, 0, 'pict')" accept="image/*">
              {$translate->val('choose')} {$translate->val('picts')|strtolower}
            </label>
            <label class="checkbox inline m-l-lg"><input type="checkbox" name="create_water" id="create_water" value="1" checked="checked">{$translate->val('create_water')}</label>
        </div>
      </div>
    </div>
</div>