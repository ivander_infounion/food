<div class="form-group" id="group-{$name}">
  <label class="col-sm-{$col_label} control-label">{if $require}* {/if}{$label}</label>
  <div class="col-sm-{12-$col_label}">
  {if $size}
    <div class="row">
      <div class="col-md-{$size}">
        <input type="text" class="form-control" name="{$name}" id="{$name}" value="{$value|escape}" placeholder="{$placeholder}">
        <span class="help-block m-b-none" id="help-{$name}">{$help}</span>
      </div>
    </div>
  {else}
    <input type="text" class="form-control" name="{$name}" id="{$name}" value="{$value|escape}" placeholder="{$placeholder}">
    <span class="help-block m-b-none" id="help-{$name}">{$help}</span>
  {/if}
  </div>
</div>