<a onclick="showConfirm('{$ctrl.name}', 'delete', {'{'}id:'{$obj->getId()}'}, '{$translate->val('remove_record')}')" rel="tooltip" data-toggle="tooltip" data-placement="top" title="{$translate->val('remove')}" class="{$class}">
  <i class="fa fa-remove"></i>
</a>