<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>{$ctrl.title}</h2>
        {include file="admin/include/breadcrumbs.tpl"}
    </div>
    <div class="col-sm-4">
        <div class="title-action">
          {makelink c=$ctrl.name a=list class="btn btn-primary" icon="list" text=$translate->val('list')}
        </div>
    </div>
</div>
<div class="row m-t">
    <div class="col-lg-12">
        
        <div class="panel">
          {element type="lang-tabs"}
          <div class="panel-body">
            <form onsubmit="return false" class="form-horizontal" enctype="multipart/form-data">
              <div class="tab-content">
              {element type="lang-check-page"}
              {foreach from=$pub_langs item=plang}
                {assign var=plid value=$plang->getId()}
                {assign var=plname value=$plang->getSname()}
                  <div id="tab-{$plname}" lang="{$plname}" class="tab-pane">
                    {element type="input" label=$translate->val('name') name="name{$plid}" require=1}
                    {element type="input" label="Название кнопки" name="namebtn{$plid}" }
                    {element type="editor" label=$translate->val('content') name="content{$plid}" }
                  </div>
              {/foreach}
                  <div id="tab-all" lang="all" class="tab-pane">
                    {element type="input" label=$translate->val('link') name="link" value=""}
                    {element type="picture" label="{$translate->val('img')}" name="ext"}
                  </div>
                  {element type="dashed"}
                  {element type="group_button" btn_cancel=true btn_action="add" btn_title=$translate->val('update') f2a=false}
              </div>
            </form>
            </div>
        </div>
    </div>
</div>

