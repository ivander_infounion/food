<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>{$ctrl.title}</h2>
        {include file="admin/include/breadcrumbs.tpl"}
    </div>
    <div class="col-sm-4">
        <div class="title-action">
          {makelink c=$ctrl.name a=new class="btn btn-primary" icon="file" text=$translate->val('create')}
        </div>
    </div>
</div>
<div class="row m-t">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
        <div class="ibox-content">

            <table class="table table-hover" id="table-dnd">
                <thead>
                <tr class="nodrop nodrag">
                    <th>#</th>
                    <th>{$translate->val('name')}</th>
                    <th class="three-btn"></th>
                </tr>
                </thead>
                <tbody>
                {foreach from=$list item=item name=foritem}
                <tr id="{$ctrl.name}_{$item->getId()}" lang="{$item->getId()}">
                    <td>{$smarty.foreach.foritem.iteration}</td>
                    <td>
                      {makelink c=$ctrl.name a=edit id=$item->getId() text=$item->getName($lang_content)}
                    </td>
                    <td>
                      {element type="but-delete" obj=$item class="pull-right"}
                      {element type="but-active" obj=$item class="pull-right m-r"}
                      {makelink c=$ctrl.name a=edit id=$item->getId() class="pull-right m-r" icon="pencil" alt=$translate->val('edit') tooltip="top"}
                    </td>
                </tr>
                {/foreach}
                </tbody>
            </table>
            {element type="pager" }
        </div>
    </div>
  </div>
</div>
