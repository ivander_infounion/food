<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>{$ctrl.title}</h2>
        {include file="admin/include/breadcrumbs.tpl"}
    </div>
    <div class="col-sm-4">
        <div class="title-action">
          {makelink c=$ctrl.name a=list class="btn btn-primary" icon="list" text=$conf.list}
        </div>
    </div>
</div>
<form onsubmit="return false" class="form-horizontal" enctype="multipart/form-data">
<div class="row m-t">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
              <div class="row">
                <div class="col-sm-6 b-r">
                  <h4 class="m-t-none m-b">{$conf.pers_data}</h4>
                  {element type="input" label=$conf.name_person name="name" value="" require=1}
                  {element type="input" label="Email" name="email" value="" require=1}
                  {element type="select" label=$conf.lang name="lang" list=$langs value=$entlang->getId() require=1}
                  {element type="select-array" label=$conf.theme name="skin" list=$skins value="" require=1}
                  {element type="picture" label=$conf.foto name="pict"}
                </div>
                <div class="col-sm-6">
                  <h4 class="m-t-none m-b">{$conf.params_acc}</h4>
                  {element type="input" label=$conf.login name="login" value="" require=1}
                  {element type="input" label=$conf.password name="pass" value="" require=1}
                  {element type="select-array" label=$conf.access name="active" list=$acts value=0 require=1}
                </div>
              </div>

              <div class="row">
                {element type="dashed"}
                  <div class="col-sm-6">
                    {element type="group_button" btn_cancel=true btn_action="add" btn_title=$conf.add f2a=false}
                  </div>
                  <div class="col-sm-6">
                  </div>
              </div>
            </div>
        </div>
    </div>
</div>
</form>

