<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>{$ctrl.title}</h2>
        {include file="admin/include/breadcrumbs.tpl"}
    </div>
    <div class="col-sm-4">
        <div class="title-action">
          {makelink c=$ctrl.name a=new class="btn btn-primary" icon="file" text=$conf.create}
          {makelink c=$ctrl.name a=list class="btn btn-primary" icon="list" text=$conf.list}
        </div>
    </div>
</div>
<form onsubmit="return false" class="form-horizontal" enctype="multipart/form-data">
<input type="hidden" name="id" value="{$item->getId()}">
<div class="row m-t">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
              <div class="row">
                <div class="col-sm-6 b-r">
                  <h4 class="m-t-none m-b">{$conf.pers_data}</h4>
                  {element type="input" label=$conf.name_person name="name" value=$item->getName() require=1}
                  {element type="input" label="Email" name="email" value=$item->getEmail() require=1}
                  {element type="select" label=$conf.lang name="lang" list=$langs value=$item->getLangId() require=1}
                  {element type="select-array" label=$conf.theme name="skin" list=$skins value=$item->getSkin() require=1}
                  {element type="picture" label=$conf.foto name="pict" obj=$item}
                </div>
                <div class="col-sm-6">
                  <h4 class="m-t-none m-b">{$conf.params_acc}</h4>
                  {element type="input" label=$conf.login name="login" value=$item->getLogin() require=1}
                  {element type="input" label=$conf.password name="pass" value=""}
                {if !$hideacc}
                  {element type="select-array" label=$conf.access name="active" list=$acts value=$item->getActive() require=1}
                {/if}
                </div>
              </div>

              <div class="row">
                {element type="dashed"}
                  <div class="col-sm-6">
                    {element type="group_button" btn_cancel=true btn_action="update" btn_title=$conf.update f2a=false}
                  </div>
                  <div class="col-sm-6">
                  </div>
              </div>
            </div>
        </div>
    </div>
</div>
</form>

