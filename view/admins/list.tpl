<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>{$ctrl.title}</h2>
        {include file="admin/include/breadcrumbs.tpl"}
    </div>
    <div class="col-sm-4">
        <div class="title-action">
          {makelink c=$ctrl.name a=new class="btn btn-primary" icon="file" text=$translate->val('create')}
        </div>
    </div>
</div>
<div class="row m-t">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
        <div class="ibox-content">

            <table class="table table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{$conf.name_person}</th>
                    <th>Email</th>
                    <th>{$conf.login}</th>
                    <th>{$conf.access}</th>
                    <th class="two-btn"></th>
                </tr>
                </thead>
                <tbody>
                {foreach from=$list item=item name=foritem}
                <tr id="admin_{$item->getId()}">
                    <td>{$smarty.foreach.foritem.iteration}</td>
                    <td>
                      {makelink c=$ctrl.name a=edit id=$item->getId() text=$item->getName($lang)}
                    </td>
                    <td>
                      {makelink c=$ctrl.name a=edit id=$item->getId() text=$item->getEmail()}
                    </td>
                    <td>
                      {makelink c=$ctrl.name a=edit id=$item->getId() text=$item->getLogin()}
                    </td>
                    <td>
                      {makelink c=$ctrl.name a=edit id=$item->getId() text=$item->getActiveName($lang)}
                    </td>
                    <td>
                      {element type="but-delete" obj=$item class="pull-right"}

                      {if $item->getActive() == 1}
                        <a class="pull-right m-r" onclick='ajax_query("{$ctrl.name}", "active", "{$item->getId()}")' id="active-{$item->getId()}" rel="tooltip" data-toggle="tooltip" data-placement="top" title="{$conf.stop_public}">
                          <i class="fa fa-eye" id="acticon-{$item->getId()}"></i>
                        </a>
                        {else}
                        <a class="pull-right m-r" onclick='ajax_query("{$ctrl.name}", "active", "{$item->getId()}")' id="active-{$item->getId()}" rel="tooltip" data-toggle="tooltip" data-placement="top" title="{$conf.publish}">
                          <i class="fa fa-eye-slash" id="acticon-{$item->getId()}"></i>
                        </a>
                        {/if}

                        
                      {makelink c=$ctrl.name a=edit id=$item->getId() class="pull-right m-r" icon="pencil" alt=$conf.edit tooltip="top"}
                    </td>
                </tr>
                {/foreach}
                </tbody>
            </table>
        </div>
    </div>
  </div>
</div>
