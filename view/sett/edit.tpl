<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>{$ctrl.title}</h2>
        {include file="admin/include/breadcrumbs.tpl"}
    </div>
    <div class="col-sm-4">
        <div class="title-action">
        </div>
    </div>
</div>
<div class="row m-t">
    <div class="col-lg-12">
      <div class="panel">
        {element type="lang-tabs"}

        <div class="panel-body">
        <form onsubmit="return false" class="form-horizontal" enctype="multipart/form-data">
          <input type="hidden" name="id" value="{$item->getId()}">

          <div class="tab-content">
              <div id="tab-all" lang="all" class="tab-pane">

                {element type="lang-check-page"}
                {element type="input" label="Email" name="email" value=$item->getEmail() require=1}
                {element type="input" label=$conf.phone name="phone" value=$item->getPhone()}
              </div>
          {foreach from=$pub_langs item=plang}
            {assign var=plid value=$plang->getId()}
            {assign var=plname value=$plang->getSname()}
              

              <div id="tab-{$plname}" lang="{$plname}" class="tab-pane">
                
                {element type="input" label="Адрес" name="address{$plid}" value=$item->getAddress($plid)}
                {element type="input" label="Copyright" name="copyright{$plid}" value=$item->getCopyright($plid)}
                

                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h5 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeo{$plname}">{$conf.fields_seo}</a>
                            </h5>
                        </div>
                        <div id="collapseSeo{$plname}" class="panel-collapse collapse">
                            <div class="panel-body">
                          {element type="input" label="Title {$conf.default}" name="title{$plid}" value=$item->getTitle($plid)}
                          {element type="input" label="Keywords {$conf.default}" name="keyw{$plid}" value=$item->getKeyw($plid)}
                          {element type="input" label="Keywords {$conf.in_header} {$conf.default}" name="hkeyw{$plid}" value=$item->getHKeyw($plid)}
                          {element type="input" label="Keywords {$conf.in_footer} {$conf.default}" name="fkeyw{$plid}" value=$item->getFKeyw($plid)}
                          {element type="textarea" label="Description {$conf.default}" size=3 name="descrip{$plid}" value=$item->getDescrip($plid)}
                            </div>
                        </div>
                    </div>
                </div>

              </div>
          {/foreach}

              </div>
              <div id="tab-all" lang="all" class="tab-pane">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h5 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseSoc">{$conf.social}</a>
                            </h5>
                        </div>
                        <div id="collapseSoc" class="panel-collapse collapse">
                            <div class="panel-body">
                              {element type="input" label="Facebook" name="facebook" value=$item->getFacebook()}
                              {element type="input" label="Twitter" name="twitter" value=$item->getTwitter()}
                              {element type="input" label="Google" name="google" value=$item->getGoogle()}
                              {element type="input" label="Youtube" name="youtube" value=$item->getYoutube()}
                              {element type="input" label="LinkedIn" name="linkedin" value=$item->getLinkedin()}
                              {element type="input" label="VK" name="vk" value=$item->getVk()}
                              {element type="input" label="Pinterest" name="pinterest" value=$item->getPinterest()}
                              {element type="input" label="Yelp" name="yelp" value=$item->getYelp()}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h5 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseCount">{$conf.codes_counters}</a>
                            </h5>
                        </div>
                        <div id="collapseCount" class="panel-collapse collapse">
                            <div class="panel-body">
                          {element type="textarea" label="{$conf.counters} {$conf.on_the_main}" size=3 name="countersmain" value=$item->getCountersMain()}
                          {element type="textarea" label="{$conf.counters} /head/" size=3 name="countershead" value=$item->getCountersHead()}
                          {element type="textarea" label="{$conf.counters} /body/" size=3 name="counters" value=$item->getCounters()}
                          {element type="textarea" label="Robots.txt" size=3 name="robots" value=$robots_txt}
                            </div>
                        </div>
                    </div>
                </div>

                {element type="dashed"}
                {element type="group_button" btn_cancel=true btn_action="update" btn_title=$conf.update f2a=false}
              </div>
          </div>
        </form>
        </div>

      </div>
</div>
