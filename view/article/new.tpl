<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>{$ctrl.title}</h2>
        {include file="admin/include/breadcrumbs.tpl"}
    </div>
    <div class="col-sm-4">
        <div class="title-action">
          {makelink c=$ctrl.name a=list sid=$section->getId() class="btn btn-primary" icon="list" text=$conf.list}
        </div>
    </div>
</div>
<div class="row m-t">
    <div class="col-lg-12">
      <div class="panel">
        {element type="lang-tabs"}

        <div class="panel-body">
        <form onsubmit="return false" class="form-horizontal" enctype="multipart/form-data">
          <input type="hidden" name="section" value="{$section->getId()}">
          <div class="tab-content">
          {foreach from=$pub_langs item=plang}
            {assign var=plid value=$plang->getId()}
            {assign var=plname value=$plang->getSname()}
              <div id="tab-{$plname}" lang="{$plname}" class="tab-pane">
              {if $plid == 1}
                {element type="lang-check-page"}
              {/if}
                {element type="input" label=$conf.name name="name{$plid}" value="" require=1}
                {element type="input" label="Url" name="url{$plid}" value="" require=1}
              </div>
          {/foreach}
              <div id="tab-all" lang="all" class="tab-pane">
                {element type="date" label=$conf.date name="created" value=$defdate time=true require=1}
                {element type="picture" label=$conf.foto name="pict"}
                {element type="input" label="Ссылка на источник" name="link" value=""}
              </div>
          {foreach from=$pub_langs item=plang}
            {assign var=plid value=$plang->getId()}
            {assign var=plname value=$plang->getSname()}
              <div id="tab-{$plname}" lang="{$plname}" class="tab-pane">
                {element type="input" label="Автор/источник" name="author{$plid}" value=""}
                {element type="editor" label=$conf.short_desc name="preview{$plid}" value=""}
                {element type="editor" label=$conf.description name="content{$plid}" value=""}
              </div>
          {/foreach}

              {element type="gallery"}

              {include file="admin/include/seo-page-new.tpl"}

              {element type="dashed"}

              {element type="group_button" btn_cancel=true btn_action="add" btn_title=$conf.add f2a=false}

          </div>
        </form>
        </div>

      </div>
    </div>
</div>
