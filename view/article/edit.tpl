<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>{$ctrl.title}</h2>
        {include file="admin/include/breadcrumbs.tpl"}
    </div>
    <div class="col-sm-4">
        <div class="title-action">
          {makelink c=$ctrl.name a=new sid=$section->getId() class="btn btn-primary" icon="file" text=$conf.create}
          {makelink c=$ctrl.name a=list sid=$section->getId() class="btn btn-primary" icon="list" text=$conf.list}
        </div>
    </div>
</div>
<div class="row m-t">
    <div class="col-lg-12">
      <div class="panel">
        {element type="lang-tabs"}

        <div class="panel-body">
        <form onsubmit="return false" class="form-horizontal" enctype="multipart/form-data">
          <input type="hidden" name="section" value="{$section->getId()}">
          <input type="hidden" name="id" value="{$item->getId()}">
          <div class="tab-content">
          {foreach from=$pub_langs item=plang}
            {assign var=plid value=$plang->getId()}
            {assign var=plname value=$plang->getSname()}
              <div id="tab-{$plname}" lang="{$plname}" class="tab-pane">
                {element type="but-active-form-lang" actlang=1 obj=$langItems.$plid}
              {if $plid == 1}
                {element type="lang-check-page"}
              {/if}
                {element type="input" label=$conf.name name="name{$plid}" value=$item->getName($plid) require=1}
                {element type="input" label="Url" name="url{$plid}" value=$item->getUrl($plid) require=1}
              </div>
          {/foreach}
              <div id="tab-all" lang="all" class="tab-pane">
                {element type="date" label=$conf.date name="created" value=$item->getCreated('d.m.Y H:i') time=true require=1}
                {element type="picture-cropper" label=$conf.foto name="pict" obj=$item}
                {element type="input" label="Ссылка на источник" name="link" value=$item->getLink()}
              </div>
          {foreach from=$pub_langs item=plang}
            {assign var=plid value=$plang->getId()}
            {assign var=plname value=$plang->getSname()}
              <div id="tab-{$plname}" lang="{$plname}" class="tab-pane">
                {element type="input" label="Автор/источник" name="author{$plid}" value=$item->getAuthor($plid)}
                {element type="editor" label=$conf.short_desc name="preview{$plid}" value=$item->getPreview($plid)}
                {element type="editor" label=$conf.description name="content{$plid}" value=$item->getContent($plid)}
              </div>
          {/foreach}

              {element type="gallery"}

              {include file="admin/include/seo-page-edit.tpl"}

              {element type="dashed"}

              {element type="group_button" btn_cancel=true btn_action="update" btn_title=$conf.update f2a=false}

          </div>
        </form>
        </div>

      </div>
    </div>
</div>
