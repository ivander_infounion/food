<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>{$ctrl.title}</h2>
        {include file="admin/include/breadcrumbs.tpl"}
    </div>
    <div class="col-sm-4">
        <div class="title-action">
          {makelink c=$ctrl.name a=new sid=$section->getId() class="btn btn-primary" icon="file" text=$conf.create}
        </div>
    </div>
</div>
<div class="row m-t">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
        <div class="ibox-content">

            <table class="table table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{$conf.date}</th>
                    <th>{$conf.title}</th>
                    <th class="three-btn"></th>
                </tr>
                </thead>
                <tbody>
                {foreach from=$list item=item name=foritem}
                <tr id="{$ctrl.name}_{$item->getId()}">
                    <td>{$smarty.foreach.foritem.iteration}</td>
                    <td>
                      {makelink c=$ctrl.name a=edit id=$item->getId() sid=$section->getId() text=$item->getCreated('d.m.y')}
                    </td>
                    <td>
                      {makelink c=$ctrl.name a=edit id=$item->getId() sid=$section->getId() text=$item->getName($lang_content)}
                    </td>
                    <td>
                      {element type="but-delete" obj=$item class="pull-right"}
                      {element type="but-active" obj=$item class="pull-right m-r"}
                      {makelink c=$ctrl.name a=edit id=$item->getId() sid=$section->getId() class="pull-right m-r" icon="pencil" alt=$conf.edit tooltip="top"}
                    </td>
                </tr>
                {/foreach}
                </tbody>
            </table>
            {element type="pager" }
        </div>
    </div>
  </div>
</div>
