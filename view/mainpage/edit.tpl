<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>{$ctrl.title}</h2>
        {include file="admin/include/breadcrumbs.tpl"}
    </div>
    <div class="col-sm-4">
        <div class="title-action">

        </div>
    </div>
</div>
<div class="row m-t">
    <div class="col-lg-12">
      <div class="panel">
        {element type="lang-tabs"}

        <div class="panel-body">
        <form onsubmit="return false" class="form-horizontal" id="form_edit" enctype="multipart/form-data">
          <input type="hidden" name="id" value="{$item->getId()}">
          <input type="hidden" name="module" value="{$item->getModuleId()}">
          <div class="tab-content">
          {foreach from=$pub_langs item=plang}
            {assign var=plid value=$plang->getId()}
            {assign var=plname value=$plang->getSname()}
              <div id="tab-{$plname}" lang="{$plname}" class="tab-pane">
                {element type="but-active-form-lang" actlang=1 obj=$langItems.$plid}
              {if $plid == 1}
                {element type="lang-check-page"}
              {/if}
                {element type="input" label=$conf.name name="name{$plid}" value=$item->getName($plid) require=1}
                {element type="input" label="Url" name="url{$plid}" value=$item->getUrl($plid) require=1}
                {element type="editor" label=$conf.description name="content{$plid}" value=$item->getContent($plid)}
                {element type="multiple" label=$conf.banners list=$banners name="banner{$plid}" select_vals=$isset_banners.$plid}
              </div>
          {/foreach}

          {foreach from=$pub_langs item=plang name=publang}
            {assign var=plid value=$plang->getId()}
            {assign var=index value=$smarty.foreach.publang.index}
            {assign var=plname value=$plang->getSname()}
              <div id="tab-{$plname}" lang="{$plname}" class="tab-pane">
                <div class="panel panel-default">
                    <div class="panel-heading">
                      <i class="fa fa-cog"></i>
                      {$conf.sett_home_page}
                    </div>
                    <div class="panel-body">
                      {element type="editor" label="{$conf.block} {$conf.about}" name="about{$plid}" value=$mainpage.$index->getAbout($plid)}
                    </div>
                </div>

              </div>
              {/foreach}

              {include file="admin/include/seo-page-edit.tpl"}

              {element type="dashed"}

              {element type="group_button" btn_cancel=true btn_action="update" btn_title=$conf.update f2a=false}

          </div>
        </form>
        </div>

      </div>
    </div>
</div>
