<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>{$ctrl.title}</h2>
        {include file="admin/include/breadcrumbs.tpl"}
    </div>
    <div class="col-sm-4">
        <div class="title-action">
          {makelink c=$ctrl.name a=list class="btn btn-outline btn-primary" icon="list" text=$conf.apply}
          {makelink c=$ctrl.name a=new class="btn btn-primary" icon="file" text=$conf.create}
        </div>
    </div>
</div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox ">
                        <div class="ibox-content">
                            <div class="dd" id="nestable">
                                <ol class="dd-list">
                                  {foreach from=$list item=item}
                                    <li class="dd-item" data-id="{$item->getId()}">
                            {assign var=children1 value=$item->getChildren()}
                            {if $children1}{assign var=icon value="folder"}{else}{assign var=icon value="file"}{/if}
                                        <div class="dd-handle">
                                          <i class="fa fa-{$icon}"></i> {$item->getName($lang_content)}
                                        </div>
                                        {assign var=children1 value=$item->getChildren()}
                                        {if $children1}
                                        <ol class="dd-list">
                                          {foreach from=$children1 item=child1}
                                        {assign var=children2 value=$child1->getChildren()}
                                        {if $children2}{assign var=icon value="folder"}{else}{assign var=icon value="file"}{/if}
                                            <li class="dd-item" data-id="{$child1->getId()}">
                                                <div class="dd-handle">
                                                  <i class="fa fa-{$icon}"></i> {$child1->getName($lang_content)}
                                                </div>
                                            {if $children2}
                                            <ol class="dd-list">
                                              {foreach from=$children2 item=child2}
                                                <li class="dd-item" data-id="{$child2->getId()}">
                                                  <div class="dd-handle">
                                                    <i class="fa fa-file"></i> {$child2->getName($lang_content)}
                                                  </div>
                                                </li>
                                              {/foreach}
                                            </ol>
                                            {/if}
                                            </li>
                                          {/foreach}
                                        </ol>
                                        {/if}
                                    </li>
                                  {/foreach}
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

