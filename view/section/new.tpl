<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>{$ctrl.title}</h2>
        {include file="admin/include/breadcrumbs.tpl"}
    </div>
    <div class="col-sm-4">
        <div class="title-action">
          {makelink c=$ctrl.name a=list class="btn btn-primary" icon="list" text=$conf.list}
        </div>
    </div>
</div>
<div class="row m-t">
    <div class="col-lg-12">
      <div class="panel">
        {element type="lang-tabs"}

        <div class="panel-body">
        <form onsubmit="return false" class="form-horizontal" enctype="multipart/form-data">

          <div class="tab-content">
          {element type="lang-check-page"}
          {foreach from=$pub_langs item=plang}
            {assign var=plid value=$plang->getId()}
            {assign var=plname value=$plang->getSname()}
              <div id="tab-{$plname}" lang="{$plname}" class="tab-pane">
                {element type="input" label=$conf.name name="name{$plid}" value="" require=1}
                {element type="input" label="Url" name="url{$plid}" value="" require=1}
              </div>
          {/foreach}
              <div id="tab-all" lang="all" class="tab-pane">
                {element type="select" label=$conf.module list=$modules name="module" value="0" empty_opt=true require=1 size=6}
                {element type="checkbox" label=$conf.main_menu name="mainmenu" value="0"}
                {element type="checkbox" label=$conf.foot_menu name="footmenu" value="0"}
              </div>
          {foreach from=$pub_langs item=plang}
            {assign var=plid value=$plang->getId()}
            {assign var=plname value=$plang->getSname()}
              <div id="tab-{$plname}" lang="{$plname}" class="tab-pane">
                {element type="editor" label=$conf.description name="content{$plid}" value=""}
                {element type="multiple" label=$conf.banners list=$banners name="banner{$plid}"}
              </div>
          {/foreach}

              {include file="admin/include/seo-page-new.tpl"}

              {element type="dashed"}

              {element type="group_button" btn_cancel=true btn_action="add" btn_title=$conf.add f2a=false}

          </div>
        </form>
        </div>

      </div>
    </div>
</div>
