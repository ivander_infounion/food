<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>{$ctrl.title}</h2>
        {include file="admin/include/breadcrumbs.tpl"}
    </div>
    <div class="col-sm-4">
        <div class="title-action">
          {makelink c=$ctrl.name a=new class="btn btn-primary" icon="file" text=$conf.create}
          {makelink c=$ctrl.name a=list class="btn btn-primary" icon="list" text=$conf.list}
        </div>
    </div>
</div>
<div class="row m-t">
    <div class="col-lg-12">
      <div class="panel">
        {element type="lang-tabs" btn_active=true}

        <div class="panel-body">
        <form onsubmit="return false" class="form-horizontal" enctype="multipart/form-data">
          <input type="hidden" name="id" value="{$item->getId()}">
          <div class="tab-content">
          {element type="lang-check-page" btn_active=true}
          {foreach from=$pub_langs item=plang}
            {assign var=plid value=$plang->getId()}
            {assign var=plname value=$plang->getSname()}
              <div id="tab-{$plname}" lang="{$plname}" class="tab-pane">
                {element type="input" label=$conf.name name="name{$plid}" value=$item->getName($plid) require=1}
                {element type="input" label="Url" name="url{$plid}" value=$item->getUrl($plid) require=1}
              </div>
          {/foreach}
              <div id="tab-all" lang="all" class="tab-pane">
                {element type="select" label=$conf.module list=$modules name="module" value=$item->getModuleId()  empty_opt=true require=1 size=6}
                {element type="checkbox" label=$conf.main_menu name="mainmenu" value=$item->getMainMenu()}
                {element type="checkbox" label=$conf.foot_menu name="footmenu" value=$item->getFootMenu()}
              </div>
          {foreach from=$pub_langs item=plang}
            {assign var=plid value=$plang->getId()}
            {assign var=plname value=$plang->getSname()}
              <div id="tab-{$plname}" lang="{$plname}" class="tab-pane">
                {element type="editor" label=$conf.description name="content{$plid}" value=$item->getContent($plid)}
                {element type="multiple" label=$conf.banners list=$banners name="banner{$plid}" select_vals=$isset_banners.$plid}
              </div>
          {/foreach}

              {include file="admin/include/seo-page-edit.tpl"}

              {element type="dashed"}

              {element type="group_button" btn_cancel=true btn_action="update" btn_title=$conf.update f2a=false}

          </div>
        </form>
        </div>

      </div>
    </div>
</div>
