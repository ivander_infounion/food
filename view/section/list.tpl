<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>{$ctrl.title}</h2>
        {include file="admin/include/breadcrumbs.tpl"}
    </div>
    <div class="col-sm-4">
        <div class="title-action">
          {makelink c=$ctrl.name a=sorting class="btn btn-outline btn-primary" icon="sort" text=$conf.sorting}
          {makelink c=$ctrl.name a=new class="btn btn-primary" icon="file" text=$conf.create}
        </div>
    </div>
</div>
<div class="row m-t">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
        <div class="ibox-content">

            <table class="table table-hover">
                <thead>
                <tr>
                    <th>{$conf.name}</th>
                    <th>{$conf.main_menu}</th>
                    <th>{$conf.foot_menu}</th>
                    <th class="three-btn"></th>
                </tr>
                </thead>
                <tbody>
                {foreach from=$list item=item name=foritem}
                  {assign var=children1 value=$item->getChildren()}
                  {if $children1}{assign var=icon value="folder"}{else}{assign var=icon value="file"}{/if}
                  <tr id="{$ctrl.name}_{$item->getId()}">
                      <td>
                        {makelink c=$ctrl.name a=edit id=$item->getId() icon=$icon text=$item->getName($lang_content)}
                      </td>
                      <td>
                        {if $item->getMainMenu() == 1}<i class="fa fa-check"></i>{/if}
                      </td>
                      <td>
                        {if $item->getFootMenu() == 1}<i class="fa fa-check"></i>{/if}
                      </td>
                      <td>
                        {element type="but-delete" obj=$item class="pull-right"}
                        {element type="but-active" obj=$item class="pull-right m-r"}
                        {makelink c=$ctrl.name a=edit id=$item->getId() class="pull-right m-r" icon="pencil" alt=$conf.edit tooltip="top"}
                      </td>
                  </tr>
                {if $children1}
                  {foreach from=$children1 item=child1}
                    {assign var=children2 value=$child1->getChildren()}
                    {if $children2}{assign var=icon value="folder"}{else}{assign var=icon value="file"}{/if}
                    <tr id="{$ctrl.name}_{$child1->getId()}">
                        <td style="padding-left:30px;">
                          {makelink c=$ctrl.name a=edit id=$child1->getId() icon=$icon text=$child1->getName($lang)}
                        </td>
                        <td>
                          {if $child1->getMainMenu() == 1}<i class="fa fa-check"></i>{/if}
                        </td>
                        <td>
                          {if $child1->getFootMenu() == 1}<i class="fa fa-check"></i>{/if}
                        </td>
                        <td>
                          {element type="but-delete" obj=$child1 class="pull-right"}
                          {element type="but-active" obj=$child1 class="pull-right m-r"}
                          {makelink c=$ctrl.name a=edit id=$child1->getId() class="pull-right m-r" icon="pencil" alt=$conf.edit tooltip="top"}
                        </td>
                    </tr>
                    {if $children2}
                      {foreach from=$children2 item=child2}
                        <tr id="{$ctrl.name}_{$child2->getId()}">
                            <td style="padding-left:50px;">
                              {makelink c=$ctrl.name a=edit id=$child2->getId() icon='file' text=$child2->getName($lang)}
                            </td>
                            <td>
                              {if $child2->getMainMenu() == 1}<i class="fa fa-check"></i>{/if}
                            </td>
                            <td>
                              {if $child2->getFootMenu() == 1}<i class="fa fa-check"></i>{/if}
                            </td>
                            <td>
                              {element type="but-delete" obj=$child2 class="pull-right"}
                              {element type="but-active" obj=$child2 class="pull-right m-r"}
                              {makelink c=$ctrl.name a=edit id=$child2->getId() class="pull-right m-r" icon="pencil" alt=$conf.edit tooltip="top"}
                            </td>
                        </tr>
                      {/foreach}
                    {/if}
                  {/foreach}
                {/if}
                {/foreach}
                </tbody>
            </table>
            {element type="pager" }
        </div>
    </div>
  </div>
</div>

