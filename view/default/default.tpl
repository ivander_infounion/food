<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="apple-mobile-web-app-capable" content="yes">

    {if $error}
      <title>{$error}</title>
    {else}
      {if $p > 1}{assign var=wordpage value=$conf.page}{assign var=title_page value="$wordpage $p "}{/if}
      {if $page}
      <title>{$title_page}{$page->getTitle($langid,'name')}</title>
      <meta name="keywords" content="{$page->getKeyw($langid)|escape}"/>
      <meta name="description" content="{$title_page|escape}{$page->getDescrip($langid)|escape}"/>
      {elseif $section}
      <title>{$title_page}{$section->getTitle($langid,'name')}</title>
      <meta name="keywords" content="{$section->getKeyw($langid)|escape}"/>
      <meta name="description" content="{$title_page|escape}{$section->getDescrip($langid)|escape}"/>
      {else}
      <title>{$title_page}{if $thistags}{$thistags}. {/if}{$sett->getTitle($langid)}</title>
      <meta name="keywords" content="{if $thistags}{$thistags|escape}. {/if}{$sett->getKeyw($langid)|escape}"/>
      <meta name="description" content="{$title_page|escape}{if $thistags}{$thistags|escape}. {/if}{$sett->getDescrip($langid)|escape}"/>
      {/if}
    {/if}

    <link rel="stylesheet" href="{$d}css/added.css">

  {if $index}
    {$sett->getCountersmain()}
  {/if}
  {$sett->getCountershead()}
  </head>

  <!--Body-->
  <body>

    {include file="default/include/header.tpl"}
    {include file=$fileTpl}
    {include file="default/include/footer.tpl"}
    {include file="default/include/modals.tpl"}

    <script src="{$d}js/JsHttpRequest.js"></script>
    <script src="{$d}js/public.js"></script>
    {$sett->getCounters()}
    <div id="debug"></div>
    <div id="page_language">{$langid}</div>
    <div id="lang_sname">{$lang}</div>
  </body><!--Body Close-->
</html>
