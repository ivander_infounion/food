{if $countpages > 1}
  <ul class="pagination">
    {if $p > 1}
      <li class="prev-page">
        <a class="icon-arrow-left" href="{$langurl}{$page->getUrl($langid)}.html?p={$p-1}"></a>
      </li>
    {/if}
    {section name=number loop=$countpages+1 start=1}
    <li{if $p == $smarty.section.number.index} class="active"{/if}>
      <a href="{$langurl}{$page->getUrl($langid)}.html?p={$smarty.section.number.index}">{$smarty.section.number.index}</a>
    </li>
    {/section}
    {if $p < $countpages}
    <li class="next-page"><a class="icon-arrow-right" href="{$langurl}{$page->getUrl($langid)}.html?p={$p+1}"></a></li>
    {/if}
  </ul>
{/if}