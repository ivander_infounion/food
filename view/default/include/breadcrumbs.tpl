<!--Breadcrumbs-->
    <ol class="breadcrumb">
    {foreach from=$bread item=br}
      {if $br.url=='#'}
        <li>{$br.name}</li>
      {else}
        <li><a href="{$br.url}">{$br.name}</a></li>
      {/if}
    {/foreach}
    </ol><!--Breadcrumbs Close-->