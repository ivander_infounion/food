{* <style type="text/css">
table {
    background-color: white!important;
}  
</style> *}

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>{$ctrl.title}</h2>
        {include file="admin/include/breadcrumbs.tpl"}
    </div>
    <div class="col-sm-4">
        <div class="title-action">
          {makelink c=$ctrl.name a=new class="btn btn-primary" icon="file" text=$conf.create}
          {makelink c=$ctrl.name a=list class="btn btn-primary" icon="list" text=$conf.list}
        </div>
    </div>
</div>
<div class="row m-t">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
            <form onsubmit="return false" class="form-horizontal" enctype="multipart/form-data">
              <input type="hidden" name="id" value="{$item->getId()}">
              
              {element type="static-text" label="Отправленно"  value=$item->getSended('d.m.y')}
              {element type="static-text" label="От кого"  value=$item->getFrom()}
              {element type="static-text" label="Кому"  value=$item->getTo()}
              {element type="static-text" label="Тема сообщения"  value=$item->getSubject()}
              {element type="static-text" label="Статус"  value=$item->getStatus()}
              
              <div>{$item->getContent()|replace:'body':'div'}</div>
              
              {element type="dashed"}

            </form>
            </div>
        </div>
    </div>
</div>

