<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>{$ctrl.title}</h2>
        {include file="admin/include/breadcrumbs.tpl"}
    </div>
    <div class="col-sm-4">
        <div class="title-action">
        </div>
    </div>
</div>
<div class="row m-t">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
        <div class="ibox-content">

            <table class="table table-hover" id="table-dnd">
                <thead>
                <tr class="nodrop nodrag">
                    <th>#</th>
                    <th>Дата</th>
                    <th>Статус</th>
                    <th>Кому</th>
                    <th>Тема</th>
                </tr>
                </thead>
                <tbody>
                {foreach from=$list item=item name=foritem}
                <tr id="{$ctrl.name}_{$item->getId()}" lang="{$item->getId()}">
                    <td>{$item->getId()}</td>
                    <td>
                      {makelink c=$ctrl.name a=edit id=$item->getId() text=$item->getSended('d.m.y')}
                    </td>
                    <td>
                      {makelink c=$ctrl.name a=edit id=$item->getId() text="{if $item->getStatusCode() == 1} Отправлено успешно {else} Ошибка отправки{/if}"}
                    </td>
                    <td>
                      {makelink c=$ctrl.name a=edit id=$item->getId() text=$item->getTo()}
                    </td>
                    <td>
                      {makelink c=$ctrl.name a=edit id=$item->getId() text=$item->getSubject()}
                    </td>

                </tr>
                {/foreach}
                </tbody>
            </table>
            {element type="pager" }
        </div>
    </div>
  </div>
</div>
