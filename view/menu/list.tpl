<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>{$ctrl.title}</h2>
        {include file="admin/include/breadcrumbs.tpl"}
    </div>
    <div class="col-sm-4">
        <div class="title-action">
          {makelink c=$ctrl.name a=sorting class="btn btn-outline btn-primary" icon="sort" text=$conf.sorting}
          {makelink c=$ctrl.name a=new class="btn btn-primary" icon="file" text=$conf.create}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-content">
                <div class="dd" id="nestable">
                    <ol class="dd-list">
                      {foreach from=$list item=item}
                        <li class="dd-item" data-id="{$item->getId()}" id="{$ctrl.name}_{$item->getId()}">
                            <div class="dd-handle">
                              {makelink c=$ctrl.name a=edit id=$item->getId() icon=$item->getIcon() text=$item->getName($lang)}
                              {element type="but-delete" obj=$item class="pull-right"}
                              {element type="but-active" obj=$item class="pull-right m-r"}
                              {makelink c=$ctrl.name a=edit id=$item->getId() class="pull-right m-r" icon="pencil" alt=$conf.edit tooltip="top"}
                            </div>
                            {assign var=children1 value=$item->getChildren()}
                            {if $children1}
                            <ol class="dd-list">
                              {foreach from=$children1 item=child1}
                                <li class="dd-item" data-id="{$child1->getId()}" id="{$ctrl.name}_{$child1->getId()}">
                                    <div class="dd-handle">
                                      {makelink c=$ctrl.name a=edit id=$child1->getId() icon=$child1->getIcon() text=$child1->getName($lang)}
                              {element type="but-delete" obj=$child1 class="pull-right"}
                              {element type="but-active" obj=$child1 class="pull-right m-r"}
                              {makelink c=$ctrl.name a=edit id=$child1->getId() class="pull-right m-r" icon="pencil" alt=$conf.edit tooltip="top"}
                                    </div>
                                </li>
                                {assign var=children2 value=$child1->getChildren()}
                                {if $children2}
                                <ol class="dd-list">
                                  {foreach from=$children2 item=child2}
                                    <li class="dd-item" data-id="{$child2->getId()}" id="{$ctrl.name}_{$child2->getId()}">
                                      <div class="dd-handle">
                                        {makelink c=$ctrl.name a=edit id=$child2->getId() icon=$child2->getIcon() text=$child2->getName($lang)}
                              {element type="but-delete" obj=$child2 class="pull-right"}
                              {element type="but-active" obj=$child2 class="pull-right m-r"}
                              {makelink c=$ctrl.name a=edit id=$child2->getId() class="pull-right m-r" icon="pencil" alt=$conf.edit tooltip="top"}
                                      </div>
                                    </li>
                                  {/foreach}
                                </ol>
                                {/if}
                              {/foreach}
                            </ol>
                            {/if}
                        </li>
                      {/foreach}
                    </ol>
                </div>

            </div>
        </div>
    </div>

</div>
