<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>{$ctrl.title}</h2>
        {include file="admin/include/breadcrumbs.tpl"}
    </div>
    <div class="col-sm-4">
        <div class="title-action">
          {makelink c=$ctrl.name a=list class="btn btn-outline btn-primary" icon="list" text=$conf.apply}
          {makelink c=$ctrl.name a=new class="btn btn-primary" icon="file" text=$conf.create}
        </div>
    </div>
</div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox ">
<!--                        <div class="ibox-title">
                            <h5>Nestable basic list</h5>
                        </div>-->
                        <div class="ibox-content">
<!--                            <p  class="m-b-lg">
                                <strong>Nestable</strong> is an interactive hierarchical list. You can drag and drop to rearrange the order. It works well on touch-screens.
                            </p>-->
                            <div class="dd" id="nestable">
                                <ol class="dd-list">
                                  {foreach from=$list item=item}
                                    <li class="dd-item" data-id="{$item->getId()}">
                                        <div class="dd-handle">
                                          {$item->getHtmlIcon()} {$item->getName($lang)}
                                        </div>
                                        {assign var=children1 value=$item->getChildren()}
                                        {if $children1}
                                        <ol class="dd-list">
                                          {foreach from=$children1 item=child1}
                                            <li class="dd-item" data-id="{$child1->getId()}">
                                                <div class="dd-handle">
                                                  {$child1->getHtmlIcon()} {$child1->getName($lang)}
                                                </div>
                                            {assign var=children2 value=$child1->getChildren()}
                                            {if $children2}
                                            <ol class="dd-list">
                                              {foreach from=$children2 item=child2}
                                                <li class="dd-item" data-id="{$child2->getId()}">
                                                  <div class="dd-handle">
                                                    {$child2->getHtmlIcon()} {$child2->getName($lang)}
                                                  </div>
                                                </li>
                                              {/foreach}
                                            </ol>
                                            {/if}
                                            </li>
                                          {/foreach}
                                        </ol>
                                        {/if}
                                    </li>
                                  {/foreach}
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

