<div class="row border-bottom">
  <nav class="navbar navbar-static-top  " role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <a class="navbar-minimalize minimalize-styl-2 btn btn-white" href="#" onclick="ajax_query('cms','changelm',1)"><i class="fa fa-bars"></i> </a>
        <!--<form role="search" class="navbar-form-custom">
            <div class="form-group">
                <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
            </div>
        </form>-->
    </div>
    <ul class="nav navbar-top-links navbar-right">
        <li>
            <span class="m-r-sm text-muted welcome-message">{$today}</span>
        </li>

        <li>
          {makelink c="admin" a="logout" icon="sign-out" text=$conf.logout}
        </li>
    </ul>

  </nav>
</div>
