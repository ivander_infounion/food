{if $ctrl.title != '' || $breadcrumbs}
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>{$ctrl.title}</h2>
      {if $breadcrumbs}
        <ol class="breadcrumb">
          {foreach from=$breadcrumbs item=bread name=forbread}
            {if $smarty.foreach.forbread.last}
            <li class="active">
                <strong>{$bread.title}</strong>
            </li>
            {else}
            <li>
                <a href="{$d}{$bread.url}">{$bread.title}</a>
            </li>
            {/if}
          {/foreach}

        </ol>
      {/if}
    </div>
    <div class="col-sm-4">
        <!--<div class="title-action">
            <a href="" class="btn btn-primary">This is action area</a>
        </div>-->
    </div>
</div>
{/if}