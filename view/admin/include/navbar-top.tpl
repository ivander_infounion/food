<div class="row border-bottom">
  <nav class="navbar navbar-static-top  " role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <a class="navbar-minimalize minimalize-styl-2 btn btn-white" href="#" onclick="ajax_query('cms','changelm',1)"><i class="fa fa-bars"></i> </a>
        <!--<form role="search" class="navbar-form-custom">
            <div class="form-group">
                <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
            </div>
        </form>-->

    </div>
    <div class="m-l-sm text-muted welcome-message m-t-md pull-left">{$today}</div>

          <div class="dropdown  pull-right m-t" style="margin-right:25px;">
              <a data-toggle="dropdown" class="dropdown-toggle" href="#" style="color:#999C9E;font-size:13px">
              {$admin->getName()}
              </a>
              <ul class="dropdown-menu animated fadeInRight m-t-sm" style="right:0px;left:auto;">
                  <li>{makelink c="admins" a="edit" icon="user" text=$conf.profile}</li>
                  <li class="divider"></li>
                  <li>{makelink c="admin" a="logout" icon="sign-out" text=$conf.logout}</li>
              </ul>
          </div>

          {if $admin->getPict() != ''}
            <img alt="image" class="img-circle pull-right m-r-sm m-t-xs" style="width:40px;" src="{$d}{$admin->getRealPathPict()}" />
          {/if}

  </nav>
</div>
