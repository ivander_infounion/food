<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                  <a href="{$d}admin/">
                  {if $logo_start != ''}
                    <img src="{$d}{$logo_start}" style="height:40px;" alt="InfoUnion CMS">
                  {else}
                    <h3>CMS</h3>
                  {/if}
                  </a>
                </div>
                <div class="logo-element">
                  <a href="{$d}admin/">
                  {if $logo_start != ''}
                    <img src="{$d}{$logo_start}" style="width:60px;" alt="InfoUnion CMS">
                  {else}
                    <h5>CMS</h5>
                  {/if}                  </a>
                </div>
            </li>

<li{if $menu_content} class="active"{/if}>
  <a href="#">
    <i class="fa fa-file-text-o"></i>
    <span class="nav-label">{$conf.content}</span> <span class="fa arrow"></span>
  </a>
  <ul class="nav nav-second-level">
{foreach from=$topsections item=menuitem name=formenu}
{if $menuitem->getDepth() == 0}
<li{if ($current_section.parent1 == $menuitem->getId())} class="active"{/if} onclick="location.href='{$menuitem->getLink()}'">

{assign var=children1 value=$menuitem->getChildren($actmenu)}
{if $children1}{assign var=icon value="folder"}{else}{assign var=icon value="file"}{/if}

  <a href="{$menuitem->getLink()}">
    <i class="fa fa-{$icon}"></i>
    {$menuitem->getName($lang_content)}{if $children1} <span class="fa arrow"></span>{/if}
  </a>
  {if $children1}
    <ul class="nav nav-third-level">
    {foreach from=$children1 item=child1}
      {assign var=children2 value=$child1->getChildren($actmenu)}
      {if $children2}{assign var=icon value="folder"}{else}{assign var=icon value="file"}{/if}
      <li{if $current_section.parent2 == $child1->getId()} class="active"{/if} onclick="location.href='{$child1->getLink()}'">
        <a href="{$child1->getLink()}">
          <i class="fa fa-{$icon}"></i> {$child1->getName($lang_content)}{if $children2} <span class="fa arrow"></span>{/if}
        </a>
        {if $children2}
        <ul class="nav nav-third-level">
        {foreach from=$children2 item=child2}
          <li{if $current_section.id == $child2->getId()} class="active"{/if}>
            <a href="{$child2->getLink()}"><i class="fa fa-file"></i> {$child2->getName($lang_content)}</a>
          </li>
        {/foreach}
        </ul>
        {/if}
      </li>
    {/foreach}
    </ul>
  {/if}
</li>
{/if}
{/foreach}
  </ul>
</li>

{if $admin->getLogin() != 'root'}{assign var=actmenu value=1}{/if}

{foreach from=$menu item=menuitem name=formenu}
{if $menuitem->getDepth() == 0}
<li{if ($current_menu.parent1 == $menuitem->getId() && !$menu_content)} class="active"{/if}>

{assign var=children1 value=$menuitem->getChildren($actmenu)}

  <a href="{$menuitem->getLink()}">
    {$menuitem->getHtmlIcon()}
    <span class="nav-label">{$menuitem->getName($lang)}</span>{if $children1} <span class="fa arrow"></span>{/if}
  </a>
  {if $children1}
    <ul class="nav nav-second-level">
    {foreach from=$children1 item=child1}
      {assign var=children2 value=$child1->getChildren($actmenu)}
      <li{if $current_menu.parent2 == $child1->getId()} class="active"{/if}>
        <a href="{$child1->getLink()}">
          {$child1->getHtmlIcon()} {$child1->getName($lang)}{if $children2} <span class="fa arrow"></span>{/if}
        </a>
        {if $children2}
        <ul class="nav nav-third-level">
        {foreach from=$children2 item=child2}
          <li{if $current_menu.id == $child2->getId()} class="active"{/if}>
            <a href="{$child2->getLink()}">{$child2->getHtmlIcon()} {$child2->getName($lang)}</a>
          </li>
        {/foreach}
        </ul>
        {/if}
      </li>
    {/foreach}
    </ul>
  {/if}
</li>
{/if}
{/foreach}


        </ul>

    </div>
</nav>