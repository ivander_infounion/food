<div class="footer">
    <div class="pull-right" id="debug">
    </div>
    <div>
        <strong>Copyright</strong> InfoUnion CMS v3.0 &copy; 2015{if $current_year != '2015'} - {$current_year}{/if}
    </div>
</div>