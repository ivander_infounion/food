{foreach from=$pub_langs item=plang}
  {assign var=plid value=$plang->getId()}
  {assign var=plname value=$plang->getSname()}
  <div id="tab-{$plname}" lang="{$plname}" class="tab-pane">
      <div class="panel-group" id="accordion">
          <div class="panel panel-default">
              <div class="panel-heading">
                  <h5 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeo{$plname}">{$conf.fields_seo}</a>
                  </h5>
              </div>
              <div id="collapseSeo{$plname}" class="panel-collapse collapse">
                  <div class="panel-body">
                    {element type="input" label=$conf.title name="header{$plid}" value=$item->getHeader($plid)}
                    {element type="input" label="Title" name="title{$plid}" value=$item->getTitle($plid)}
                    {element type="input" label="Keywords" name="keyw{$plid}" value=$item->getKeyw($plid)}
                    {element type="input" label="Keywords {$conf.in_header}" name="hkeyw{$plid}" value=$item->getHkeyw($plid)}
                    {element type="input" label="Keywords {$conf.in_footer}" name="fkeyw{$plid}" value=$item->getFKeyw($plid)}
                    {element type="textarea" label="Description" name="descrip{$plid}" value=$item->getDescrip($plid) size="2"}
                  </div>
              </div>
          </div>
      </div>
  </div>
{/foreach}
