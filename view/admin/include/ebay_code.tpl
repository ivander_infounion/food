<p><a href="http://mwbrothers.it" target="_blank"><img alt="" height="315" src="http://mwbrothers.it/img/Logo_for_ebay.gif" width="1200"></a></p>
<h1>
<span style="">Coprisedili MW brothers per {$cover->getName($lang_content)}&nbsp;{$series->getName($lang_content)}</span>
</span></h1>
<p><font size="4"><font face="Arial, Helvetica, sans-serif">Questa serie di prodotti combina la praticita, la perfezione di linee e colori, ed un livello particolare di comfort. Viene utilizzata una combinazione di similpelle dall' azienda "ERA" ( Germania) e tessuti  «Fezko» per auto  di alta qualita. Tutti i materiali,  compresi i fili, sono stati prodotti e certificati in Europa. Desideri un elevato comfort e stile per il tuo abitacolo? Scegli la serie Comfort!</font></font></p>
{foreach from=$pictures item=pict name=forpict}
<img src="{$defaulturl}{$pict->getRealPathPict()}" height="300px" style ="float:left;margin-left: 5px;margin-top: 5px; "alt="">
{/foreach}
<div style="display:block;float:left">
<p align="center"><font face="Arial, Helvetica, sans-serif"><font size="6">Serie Comfort</font></font></p>
<p><font face="Arial, Helvetica, sans-serif">I coprisedili di serie Comfort di MW Brothers sono realizzati in materiali moderni, di alta qualita di produzione europea. Durante la cucitura vengono usati fili di elevata resistenza della marca inglese Coats, similpelle e tessuto per auto di produttori leader (Aunde, Fezko, Era).</font></p>
<p align="center">&nbsp;<img alt="" height="265" src="http://mwbrothers.it/content/cover/39/DSC_0516Q.jpg" width="400">&nbsp;<img alt="" height="265" src="http://mwbrothers.it/content/cover/66/DSC06749Q.jpg" width="353">&nbsp;<img alt="" height="265" src="http://mwbrothers.it/content/cover/90/DSC_0450Q.jpg" width="400"></p>
<p align="center"><font size="6"><font face="Arial, Helvetica, sans-serif">Serie Dynamic</font></font></p>
<p><font face="Arial, Helvetica, sans-serif">I coprisedili di serie Dynamic sono in grado in breve tempo di trasformare l’abitacolo del veicolo, prolungandone di una decade la praticita e il comfort.&nbsp;Abbiamo applicato l’esperienza di creare dime della precedente serie Leather, per creare prodotti con una vestibilita impeccabile e design moderno.</font></p>
<p align="center"><img alt="" height="265" src="http://mwbrothers.it/content/cover/37/DSC_0167Q.jpg" width="400">&nbsp;<img alt="" height="265" src="http://mwbrothers.it/content/cover/37/DSC_0229QQ.jpg" width="175">&nbsp;<img alt="" height="265" src="http://mwbrothers.it/content/cover/37/DSC_0187Q.jpg" width="400"></p>
<p align="center"><span style="font-family: Arial, Helvetica, sans-serif; font-size: xx-large;">Serie Leather</span></p>
<p><font face="Arial, Helvetica, sans-serif">Se si desidera un abitacolo di lusso, come in auto di classe business, cambiare tutta tappezzeria non e l’unico modo.&nbsp; Offriamo una soluzione semplice, efficace e meno costosa. Questa e costituita dai coprisedili di marca di serie Leather.</font></p>
<p align="center"><font face="Arial, Helvetica, sans-serif"><img alt="" height="265" src="http://mwbrothers.it/content/cover/258/DSC_1465Q.jpg" width="400">&nbsp;<img alt="" height="265" src="http://mwbrothers.it/content/cover/38/DSC_0533q.jpg" width="400">&nbsp;<img alt="" height="265" src="http://mwbrothers.it/content/cover/68/DSC_0310QQ.jpg" width="175"></font></p>
<h2 style="text-align: center;">Guarda il video come fare il montaggio dei nostri coprisedili</h2>
<p align="center"><a href="https://www.youtube.com/watch?v=LxuNybAsHAI" target="_blank"><img alt="" height="347" src="http://mwbrothers.it/content/upload/194639166.png" width="619"></a></p>
<div class="video-block" style="box-sizing: border-box; color: #676a6c; font-family: 'open sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 18.5714px; background-color: #ffffff;">
<p><font size="4">Oltre alle caratteristiche estetiche e funzionali, non abbiamo dimenticato il piu importante – la sicurezza. Tutti i coprisedili sono cuciti da una cucitura speciale dove si trovano i cuscini di sicurezza. Cio, in caso di necessita, provvedra il loro funzionamento affidabile.</font></p>
<p><font size="4"><img alt="" height="200" src="http://mwbrothers.it/content/upload/3.png" width="300"></font></p>
</div>
<p><span style="color: #676a6c; font-family: 'open sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: large; line-height: 20.6349px; background-color: #ffffff;">Compri coprisedili automobile della societa di MWbrothers e Lei sempre si sentira comodamente in abitacolo dell'auto, senza preoccuparsi di sedili.</span></p>
<p>&nbsp;</p>
<h2><font size="5"><font face="Comic Sans MS, cursive">5 anni di garanzia su tutti i coprisedili</font></font></h2>
<p>&nbsp;</p>
<p><font size="5"><font face="Arial, Helvetica, sans-serif">Le nostre merci non si &nbsp;vendono in negozi soliti, puo ordinare solo &nbsp;nel sito al rappresentante ufficiale della societa MWbrothers</font></font></p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #676a6c; font-family: 'open sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 18.5714px; background-color: #ffffff;"><span style="box-sizing: border-box; font-size: x-large; line-height: 20.6349px;"><img src="http://mwbrothers.it/content/upload/sprEBPnew.png" style="box-sizing: border-box; border: 0px; vertical-align: middle; width: 362.066px; height: 43.4479px;"></span></p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #676a6c; font-family: 'open sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 18.5714px; background-color: #ffffff;">&nbsp;</p>
<h4><font color="#0000FF"><font size="4"><font face="Arial, Helvetica, sans-serif">Garanzia di rimborso entro 14 giorni dopo acquisto a condizione di conservazione di merce e impacchettamento</font></font></font></h4>
<p>&nbsp;</p>
<p><b>Catalogo:</b>&nbsp;<a href="http://mwbrothers.it/catalogo.html">http://mwbrothers.it/catalogo.html</a></p>
<p><b>Vodafone:</b> (+39) 344-236-22-72</p>
<p><b>E-Mail:</b> mwbrothersita@gmail.com</p>
<p><b>Indirizzo:</b> Italy, Trento, via Brennero 116</p>
<p>&nbsp;</p>
</div>