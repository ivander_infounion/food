<div class="modal inmodal fade" id="modal-confirm" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header"><h3></h3></div>
<!--            <div class="modal-body align-center">-->

<!--            </div>-->
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">{$conf.no}</button>
                <button type="button" class="btn btn-primary" id="btn-modal-confirm">{$conf.yes}</button>
            </div>
        </div>
    </div>
</div>