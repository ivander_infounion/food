{if $breadcrumbs}
  <ol class="breadcrumb">
    {foreach from=$breadcrumbs item=bread name=forbread}
      {if $smarty.foreach.forbread.last}
      <li class="active">
          <strong>{$bread.title}</strong>
      </li>
      {else}
      <li>
          <a href="{$bread.url}">{$bread.title}</a>
      </li>
      {/if}
    {/foreach}

  </ol>
{/if}