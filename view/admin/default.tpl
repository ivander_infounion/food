<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{if $ctrl.title != ''}{$ctrl.title} - {/if}{if $action.title != ''}{$action.title} - {/if} {$conf.admin_header}</title>
    <link href="{$d}img/mwbrothers-admin.ico" rel="icon" type="image/x-icon" >
    <link href="{$d}img/mwbrothers-admin.ico" rel="shortcut icon" type="image/x-icon" >
    <link href="{$d}admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="{$d}admin/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="{$d}admin/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="{$d}admin/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="{$d}admin/css/plugins/chosen/chosen.css" rel="stylesheet">

    <script src="{$d}admin/js/jquery-2.1.1.js"></script>
    {* <link href="{$d}admin/css/plugins/cropper/cropper.min.css" rel="stylesheet"> *}
    <link href="{$d}admin/css/animate.css" rel="stylesheet">
{if $editor}
  {if $editor_kind == 'summernote'}
    <link href="{$d}admin/css/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="{$d}admin/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
  {elseif $editor_kind == 'ckeditor'}
    <script type="text/javascript" src="{$d}admin/ckeditor/ckeditor.js"></script>
  {/if}
{/if}
{if $gallery}
    <link href="{$d}admin/css/plugins/blueimp/blueimp-gallery.min.css" rel="stylesheet">
{/if}
{if $datepick}
    <link href="{$d}admin/css/plugins/datetimepicker/jquery.datetimepicker.css" rel="stylesheet">
{/if}

    <link rel="stylesheet" type="text/css" href="{$d}admin/css/plugins/cropper/cropper.css">
    
    <script src="{$d}admin/js/plugins/cropper/cropper.js"></script>

    <link href="{$d}admin/css/style.css" rel="stylesheet">
    <link href="{$d}admin/css/admin-iu.css" rel="stylesheet">

</head>

<body class="{if $admin}{$admin->getSkin()}{/if}{if $admin->getLeftMenu()==1} mini-navbar{/if}">

    <div id="wrapper">

      {include file="admin/include/navbar-side.tpl"}

        <div id="page-wrapper" class="gray-bg p-b-lg">

          {include file="admin/include/navbar-top.tpl"}

      {if $fileTpl}
        {include file=$fileTpl}
      {else}
        <div class="row">
            <div class="col-lg-12">
                <div class="wrapper wrapper-content">
                    <div class="animated fadeInRightBig">
                        <h3 class="font-bold"><i class="fa fa-arrow-left"></i> {$conf.select_section_left}</h3>

                        <div class="error-desc">
                          {if $alert}{$alert.status}{/if}
                        </div>
                    </div>
                </div>
            </div>
        </div>
      {/if}

          {include file="admin/include/footer.tpl"}

        </div>
    </div>

    {include file="admin/include/modal-confirm.tpl"}

    <!-- Mainly scripts -->
    <script src="{$d}admin/js/bootstrap.min.js"></script>
    <script src="{$d}admin/js/JsHttpRequest.js"></script>
    <script src="{$d}admin/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="{$d}admin/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="{$d}admin/js/inspinia.js"></script>
    <script src="{$d}admin/js/plugins/pace/pace.min.js"></script>
    <!-- Toastr script -->
    <script src="{$d}admin/js/plugins/toastr/toastr.min.js"></script>
    <script src="{$d}admin/js/plugins/iCheck/icheck.min.js"></script>
    <!-- Chosen -->
    <script src="{$d}admin/js/plugins/chosen/chosen.jquery.js"></script>

  <!-- CMS v3.0 script -->
    <script src="{$d}admin/js/cms-toastr.js"></script>
    <script src="{$d}admin/js/cms-files.js"></script>
    <script src="{$d}admin/js/cms-script.js"></script>

    <script>
        var config = {
                '.chosen-select'           : {'{'}width:"100%"},
                '.chosen-select-deselect'  : {'{'}allow_single_deselect:true},
                '.chosen-select-no-single' : {'{'}disable_search_threshold:10},
                '.chosen-select-no-results': {'{'}no_results_text:'Oops, nothing found!'},
                '.chosen-select-width'     : {'{'}width:"95%"}
            }
            for (var selector in config) {
                $(selector).chosen(config[selector]);
            }
      $(document).ready(function(){
        {foreach from=$toastr item=tsr}
          toastr['{$tsr.kind}']('{$tsr.msg}', '{$tsr.title}');
        {/foreach}

        {if $tablang}
        //*** Page with language tabs
          $(".showlang").on('ifChecked', function(event){
            var lid = $(this).attr("data-id");
            if (lid != 1) {
              $("#tab-lang-"+lid).show();
              $("#lang > option[value='"+lid+"']").prop('selected', true);
            }
          });
          $(".showlang").on('ifUnchecked', function(event){
            var lid = $(this).attr("data-id");
            if (lid != 1) {
              $("#tab-lang-"+lid).hide();
              $("#lang > option[value='"+lid+"']").removeAttr("selected");
              if ($("#tab-lang-"+lid).hasClass("active")) {
                tabLang('{$slang.1}');
                $("#tab-lang-"+lid).removeClass("active");
                $("#tab-lang-1").addClass("active");
              }
            }
          });
          tabLang('{$slang.1}');
        {/if}

      });
    </script>
{if $tablednd}
    <script type="text/javascript" src="{$d}admin/js/plugins/tablednd/jquery.tablednd.0.7.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {'{'}
        // Initialise the table
        $("#table-dnd").tableDnD({'{'}
          onDrop: function(table, row) {'{'}
            var rows = table.tBodies[0].rows;
            res = new Array;
            for (var i=0; i<rows.length; i++) {
              res[i] = rows[i].lang;
            }
            ajax_query('{$ctrl.name}', 'reorder{$tablednd}', res);
          },
        });
    });
    </script>
{/if}
{if $datepick}
    <script src="{$d}admin/js/plugins/datetimepicker/jquery.datetimepicker.js"></script>
    <script type="text/javascript">
    {foreach from=$datepick item=dp}
      $('#{$dp.name}').datetimepicker({'{'}
        lang: '{$lang}',
        mask:'39.19.2999{if $dp.time} 29:59{/if}',
        format:  'd.m.Y{if $dp.time} H:i{/if}',
        formatDate:  'd.m.Y',
        {if $dp.time}timepicker: true,{/if}
      });
    {/foreach}
    </script>

{/if}
{if $editor}
  {if $editor_kind == 'summernote'}
    <script src="{$d}admin/js/plugins/summernote/summernote.min.js"></script>
    <script>
        $(document).ready(function(){
            $('.summernote').summernote({
              onImageUpload: function(files, editor, welEditable) {
                sendFile(files[0], editor, welEditable);
              }
            });
       });
    </script>
{/if}
{/if}
{if $gallery}
<script src="{$d}admin/js/plugins/blueimp/jquery.blueimp-gallery.min.js"></script>
<script>
//var gallery = $('#blueimp-gallery').data('gallery');
</script>
{/if}
{if $plugin_nestable}
    <!-- Nestable List -->
    <script src="{$d}admin/js/plugins/nestable/jquery.nestable.js"></script>

    <script>
         $(document).ready(function(){

             var updateOutput = function (e) {
                 var list = e.length ? e : $(e.target);
                 if (list.nestable('serialize').length) {
                    ajax_query('{$ctrl.name}','reordertree',window.JSON.stringify(list.nestable('serialize')));
                 }
             };
             // activate Nestable for list 1
             $('#nestable').nestable({
                 group: 1
             }).on('change', updateOutput);

             // output initial serialised data
             updateOutput($('#nestable').data());

         });
    </script>
{/if}
{* old *}
{* {if $plugin_cropper}
    <!-- Image cropper -->
    <script src="{$d}admin/js/plugins/cropper/cropper.min.js"></script>

    <script>
        $(document).ready(function(){

            var $image = $(".image-crop > img")
            $($image).cropper({
                aspectRatio: 1,
                preview: ".img-preview",
                done: function(data) {
                    // Output the result data for cropping image.
                }
            });

            var $inputImage = $("#inputImage");
            if (window.FileReader) {
                $inputImage.change(function() {
                    var fileReader = new FileReader(),
                            files = this.files,
                            file;

                    if (!files.length) {
                        return;
                    }

                    file = files[0];

                    if (/^image\/\w+$/.test(file.type)) {
                        fileReader.readAsDataURL(file);
                        fileReader.onload = function () {
                            $inputImage.val("");
                            $image.cropper("reset", true).cropper("replace", this.result);
                        };
                    } else {
                        showMessage("Please choose an image file.");
                    }
                });
            } else {
                $inputImage.addClass("hide");
            }

            $("#download").click(function() {
                window.open($image.cropper("getDataURL"));
            });

            $("#zoomIn").click(function() {
                $image.cropper("zoom", 0.1);
            });

            $("#zoomOut").click(function() {
                $image.cropper("zoom", -0.1);
            });

            $("#rotateLeft").click(function() {
                $image.cropper("rotate", 45);
            });

            $("#rotateRight").click(function() {
                $image.cropper("rotate", -45);
            });

            $("#setDrag").click(function() {
                $image.cropper("setDragMode", "crop");
            });
    });
    </script>

{/if} *}


<script type="text/javascript">
$(document).ready(function(){
{literal}
  $('.number').bind("change keyup input click", function() {
      if (this.value.match(/[^0-9.]/g)) {
          this.value = this.value.replace(/[^0-9.]/g, '');
      }
      if (this.value.match(/[\.]{1,}/g)) {
          this.value = this.value.replace(/[\.]{1,}/g, '.');
      }
      if (this.value.match(/[\.]{1}[0-9]{3,}/)) {
        this.value = this.value.substr(0,this.value.length-1);
      }
  });  
});  
{/literal}
  
</script>
</body>
</html>