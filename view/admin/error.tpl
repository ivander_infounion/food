<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
            <div class="animated fadeInRightBig">
              <div class="panel panel-danger">
                  <div class="panel-heading">
                    <i class="fa fa-warning"></i>
                    <b>{$error}</b>
                  </div>
                  {if $error_desc}
                  <div class="panel-body">
                    <p>{$error_desc}</p>
                  </div>
                  {/if}
                  {if $error_footer}
                  <div class="panel-footer">
                    {$error_footer}
                  </div>
                  {/if}
              </div>

            </div>
        </div>
    </div>
</div>