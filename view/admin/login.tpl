<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{$ctrl.title}</title>
  <link href="{$d}img/fav-icon-admin.png" rel="icon" type="image/x-icon" >
  <link href="{$d}img/fav-icon-admin.png" rel="shortcut icon" type="image/x-icon" >

    <link href="{$d}admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="{$d}admin/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="{$d}admin/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="{$d}admin/css/animate.css" rel="stylesheet">
    <link href="{$d}admin/css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen  animated fadeInDown">
        <div>
            {if $logo_start != ''}
            <div style="padding:25px;border-radius:3px">
              <img src="{$d}{$logo_start}" alt="">
            </div>
            {else}
            <div style="background-color: #18A689;padding:25px;border-radius:3px">
              <h2 style="color:white;">CMS</h2>
            </div>
            {/if}
            <h3>{$conf.admin_header}</h3>
            <p>
            </p>
            <form class="m-t" role="form" onsubmit="return false">
                <div class="form-group" id="group-login">
                    <input type="text" class="form-control" name="login" placeholder="{$conf.login}" >
                    <span class="help-block m-b-none m-t-none" id="help-login"></span>
                </div>
                <div class="form-group" id="group-pass">
                    <input type="password" class="form-control" name="pass" placeholder="{$conf.password}" >
                    <span class="help-block m-b-none m-t-none" id="help-pass"></span>
                </div>
                <div class="form-group">
                    <div class="checkbox i-checks"><label> <input type="checkbox" name="remember" value="1"><i></i> {$conf.remember_me} </label></div>
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b" onclick='ajax_query("admin", "login", form2array(this.form))'>{$conf.loginin}</button>

<!--                <a href="#"><small>Forgot password?</small></a>-->
<!--                <p class="text-muted text-center"><small>Do not have an account?</small></p>-->
            </form>
            <p class="m-t"> <small>InfoUnion CMS base on Bootstrap 3 &copy; 2015{if $current_year != '2015'} - {$current_year}{/if}</small> </p>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="{$d}admin/js/jquery-2.1.1.js"></script>
    <script src="{$d}admin/js/bootstrap.min.js"></script>
    <script src="{$d}admin/js/JsHttpRequest.js"></script>
    <script src="{$d}admin/js/cms-login.js"></script>
    <script src="{$d}admin/js/plugins/iCheck/icheck.min.js"></script>
    <script>
        $(document).ready(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>
<div id=debug></div>

</body>
</html>
