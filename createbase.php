<?php

  header('Content-type: text/html; charset=UTF-8');
  require 'autoload.php';

  $db = DB::getInstance();
  $db->debug = true;

  $db->query("SET foreign_key_checks = 0");
  $trans = new TranslateCollection();

  $table = new AdminTable();
  $tmod = new LogsTable();
  $t4 = new AccessTable();
  $t44 = new adminHasaccessTable();
  $lang = new LangTable();
  $ln = new LangnameTable();

  $t44->dropTable();
  $t4->dropTable();
  $tmod->dropTable();
  $table->dropTable();
  $ln->dropTable();
  $lang->dropTable();

  $lang->createTable();
  $ln->createTable();
  $t4->createTable();
  $table->createTable();
  $tmod->createTable();

  $deflang = Settings::get('deflang');
  $trans_lang = Settings::get('lang');
  $collang = new LangCollection();
  $collangname = new LangnameCollection();
  $lang = $collang->add(
      array(
          'sname' => $deflang,
          'active' => 1,
        )
    );
  $langname = $collangname->add(
      array(
          'lang' => 1,
          'translate_id' => 1,
          'name' => $trans_lang[ $deflang ],
          'shortname' => mb_substr($trans_lang[$deflang], 0, 3, "UTF-8"),
          'order' => 1,
        )
    );

  $user = new AdminCollection();
  $user->add(array(
    'name'=>'Developer',
    'login'=>'root',
    'pass'=>md5('start'),
    'email'=>'voloshanivsky@gmail.com',
    'active'=>1,
    'lang'=>1,
  ));
$tm = new MenuTable();
$tml = new menuHaslangTable();
$tml->dropTable();
$tm->dropTable();
$tm->createTable();

$col = new MenuCollection();
$menu_names = array(
    'ru' => array(
        1 => 'Дерево сайта',
        2 => 'Справочники',
        3 => 'Баннеры',
        4 => 'Настройки',
        5 => 'Настройки сайта',
        6 => 'Список языковых версий',
        7 => 'Локализация',
        8 => 'Администраторы',
        9 => 'Администрирование',
        10 => 'Настройки меню',
        11 => 'Модули',
        12 => 'История писем',
      ),
    'en' => array(
        1 => 'Site map',
        2 => 'Reference books',
        3 => 'Banners',
        4 => 'Settings',
        5 => 'Site settings',
        6 => 'Languages',
        7 => 'Localization',
        8 => 'Administrators',
        9 => 'Administration',
        10 => 'Menu settings',
        11 => 'Modules',
        12 => 'Mail history',
      ),
    'ua' => array(
        1 => 'Дерево сайту',
        2 => 'Довідники',
        3 => 'Банери',
        4 => 'Налаштування',
        5 => 'Налаштування сайту',
        6 => 'Список мовних версій',
        7 => 'Локалізація',
        8 => 'Адміністратори',
        9 => 'Адміністрування',
        10 => 'Налаштування меню',
        11 => 'Модулі',
        12 => 'Історія листів',
      ),
  );
$col->add(array('name1'=>$menu_names[$deflang][1], 'lang' => array(1),
                'ctrl'=>'section','action'=>'list','params'=>'','icon'=>'sitemap','active'=>1,
                'parent'=>0,'depth'=>0,'children'=>0,'order'=>1));

$col->add(array('name1'=>$menu_names[$deflang][2], 'lang' => array(1),
                'ctrl'=>'','action'=>'','params'=>'','icon'=>'inbox','active'=>1,
                'parent'=>0,'depth'=>0,'order'=>2,'children'=>1));
$col->add(array('name1'=>$menu_names[$deflang][3], 'lang' => array(1), 
                'ctrl'=>'banner','action'=>'list','params'=>'','icon'=>'image','active'=>1,
                'parent'=>2,'depth'=>1,'order'=>3));

$col->add(array('name1'=>$menu_names[$deflang][4], 'lang' => array(1), 
                'ctrl'=>'','action'=>'','params'=>'','icon'=>'cog','active'=>1,
                'parent'=>0,'depth'=>0,'children'=>3,'order'=>4));
$col->add(array('name1'=>$menu_names[$deflang][5], 'lang' => array(1), 
                'ctrl'=>'sett','action'=>'edit','params'=>'','icon'=>'cog','active'=>1,
                'parent'=>4,'depth'=>1,'children'=>0,'order'=>5));
$col->add(array('name1'=>$menu_names[$deflang][6], 'lang' => array(1), 
                'ctrl'=>'lang','action'=>'list','params'=>'','icon'=>'language','active'=>1,
                'parent'=>4,'depth'=>1,'order'=>6));
$col->add(array('name1'=>$menu_names[$deflang][7], 'lang' => array(1), 
                'ctrl'=>'translate','action'=>'list','params'=>'','icon'=>'language','active'=>1,
                'parent'=>4,'depth'=>1,'order'=>7));

$col->add(array('name1'=>$menu_names[$deflang][8], 'lang' => array(1), 
                'ctrl'=>'admins','action'=>'list','params'=>'','icon'=>'users','active'=>1,
                'parent'=>0,'depth'=>0,'children'=>0,'order'=>8));

$col->add(array('name1'=>$menu_names[$deflang][9], 'lang' => array(1),
                'ctrl'=>'','action'=>'','params'=>'','icon'=>'wrench','active'=>1,
                'parent'=>0,'depth'=>0,'children'=>2,'order'=>9));
$col->add(array('name1'=>$menu_names[$deflang][10], 'lang' => array(1), 
                'ctrl'=>'menu','action'=>'list','params'=>'','icon'=>'navicon','active'=>0,
                'parent'=>9,'depth'=>1,'children'=>0,'order'=>10));
$col->add(array('name1'=>$menu_names[$deflang][11], 'lang' => array(1),
                'ctrl'=>'module','action'=>'list','params'=>'','icon'=>'magnet','active'=>0,
                'parent'=>9,'depth'=>1,'order'=>11));

$col->add(array('name1'=>$menu_names[$deflang][12], 'lang' => array(1),
                'ctrl'=>'logsmail','action'=>'list','params'=>'','icon'=>'envelope','active'=>0,
                'parent'=>2,'depth'=>1,'order'=>12));

$t = new ModuleTable();
$tl = new moduleHaslangTable();
$tl->dropTable();
$t->dropTable();
$t->createTable();

$colm = new ModuleCollection();
$colm->add(array(
    'lang' => array(1),
    'name1' => 'Модуль "Главная страница"',
    'name2' => 'Module "Main page"',
    'name3' => 'Модуль "Головна сторінка"',
    'controller' => 'mainpage',
    'order' => 1,
  ));
$colm->add(array(
    'lang' => array(1),
    'name1' => 'Модуль "Текстовая страница"',
    'name2' => 'Module "Textpage"',
    'name3' => 'Модуль "Текстова сторінка"',
    'controller' => 'textpage',
    'order' => 2,
  ));
$colm->add(array(
    'lang' => array(1),
    'name1' => 'Модуль "Новости/статьи"',
    'name2' => 'Module "News/articles"',
    'name3' => 'Модуль "Новини/статті"',
    'controller' => 'article',
    'order' => 3,
  ));


$t = new TranslateTable();
$tl = new translateHaslangTable();
$tl->dropTable();
$t->dropTable();
$t->createTable();

$translate = array();
$translate['en'] = array(
  'access' => 'Access',
  'access_denied' => 'Access denied',
  'access_locked' => 'Access locked',
  'add' => 'Add',
  'added_ok' => 'The record was successfully added',
  'address' => 'Address',
  'admin_header' => 'Content management system',
  'and' => 'and',
  'apply' => 'Apply',
  'attempt' => 'attempt',
  'attempt_2' => 'attempt',
  'banners' => 'Banners',
  'cancel' => 'Cancel',
  'cant_del' => 'Can not be deleted',
  'category' => 'Category',
  'check_fields' => 'Check fields',
  'choose' => 'Choose',
  'close' => 'Close',
  'close2' => 'Close',
  'codes_counters' => 'Codes counters',
  'content' => 'Content',
  'counters' => 'Counters',
  'create' => 'Create',
  'create_water' => 'Add protect images',
  'ctrl' => 'Controller',
  'date' => 'Date',
  'default' => 'default',
  'deleted_ok' => 'Deleted successfully',
  'description' => 'Description',
  'edit' => 'Edit',
  'error_authorization' => 'Time out session. Log in the system again.',
  'exists_email' => 'User already exists with this email',
  'exists_login' => 'User already exists with this login',
  'fields_seo' => 'SEO information',
  'file' => 'File',
  'fill_the_fields' => 'Fill in the fields',
  'find' => 'Find',
  'five_times_wrong' => 'Five times you enter the wrong password, the user is locked!',
  'foot_menu' => 'Footer menu',
  'form_feedback' => 'Feedback form',
  'foto' => 'Foto',
  'full_access' => 'Full access',
  'gallery' => 'Gallery',
  'group' => 'Group',
  'icon' => 'Icon',
  'img' => 'Image',
  'in_footer' => 'in footer',
  'in_header' => 'in header',
  'is_work' => 'This page is under construct',
  'key' => 'Key',
  'lang' => 'Language',
  'lang_ver' => 'Language version',
  'lang_ver_page' => 'Language version of the page',
  'lang_versions' => 'Language version of the site',
  'link' => 'Link',
  'list' => 'List',
  'locked' => 'Locked',
  'login' => 'Username',
  'loginin' => 'Login',
  'logout' => 'Log out',
  'main_menu' => 'Main menu',
  'mainpage' => 'Main page',
  'message' => 'Message',
  'module' => 'Module',
  'more' => 'More',
  'name' => 'Name',
  'name_person' => 'Name',
  'new' => 'New',
  'next' => 'Next',
  'no' => 'No',
  'no_params' => 'Not enough parameters',
  'not_saved' => 'Not saved',
  'on_the_main' => 'on the main page',
  'open' => 'Open',
  'page' => 'Page',
  'page_not_found' => 'Page not found',
  'page_unavailable' => 'Page unavailable',
  'params' => 'Parametres',
  'params_acc' => 'Parameters of access',
  'password' => 'Password',
  'pers_data' => 'Personal data',
  'phone' => 'Phone',
  'pict' => 'Picture',
  'picts' => 'Pictures',
  'profile' => 'Profile',
  'publish' => 'Publish content',
  'remaining' => 'Remaining',
  'remaining_2' => 'Remaining',
  'remember_me' => 'Remember me',
  'remove' => 'Remove',
  'remove_record' => 'Remove record?',
  'req_field' => 'This field id required',
  'review' => 'Review',
  'reviews' => 'Reviews',
  'saved_ok' => 'Saved successfully',
  'search' => 'Search',
  'select_section_left' => 'To change the contents of the page, select the section on the left',
  'send' => 'send',
  'short_desc' => 'Short description',
  'show_on_mainpage' => 'Show on the main page',
  'site' => 'Site',
  'sitemap' => 'Sitemap',
  'social' => 'Social network',
  'sorting' => 'Reorder',
  'stop_public' => 'Content published',
  'theme' => 'Theme',
  'theme_review' => 'Theme review',
  'title' => 'Title',
  'update' => 'Update',
  'upl_failed' => 'File upload failed',
  'url' => 'Url',
  'user_not_found' => 'User not found',
  'view' => 'View',
  'wrong_pass' => 'Wrong password',
  'yes' => 'Yes',
  'your' => 'Your',
  'your2' => 'Your',
  'short_name' => 'Short name',
);
$translate['ru'] = array(
  'access' => 'Доступ',
  'access_denied' => 'Доступ закрыт',
  'access_locked' => 'Доступ заблокирован',
  'add' => 'Добавить',
  'added_ok' => 'Запись успешно добавлена',
  'address' => 'Адрес',
  'admin_header' => 'Система управления сайтом',
  'and' => 'и',
  'apply' => 'Применить',
  'attempt' => 'попыток',
  'attempt_2' => 'попытка',
  'banners' => 'Баннеры',
  'cancel' => 'Отмена',
  'cant_del' => 'Невозможно удалить',
  'category' => 'Категория',
  'check_fields' => 'Проверьте поля',
  'choose' => 'Выберите',
  'close' => 'Закрыто',
  'close2' => 'Закрыть',
  'codes_counters' => 'Коды счетчиков',
  'content' => 'Контент',
  'counters' => 'Счетчики',
  'create' => 'Создать',
  'create_water' => 'Добавить защиту изображений',
  'ctrl' => 'Контроллер',
  'date' => 'Дата',
  'default' => 'по умолчанию',
  'deleted_ok' => 'Успешно удалено',
  'description' => 'Описание',
  'edit' => 'Редактировать',
  'error_authorization' => 'Время сессии вышло. Авторизуйтесь в системе еще раз.',
  'exists_email' => 'Уже существует пользователь с таким email',
  'exists_login' => 'Уже существует пользователь с таким логином',
  'fields_seo' => 'Информация SEO',
  'file' => 'Файл',
  'fill_the_fields' => 'Заполните поля',
  'find' => 'Найти',
  'five_times_wrong' => 'Вы 5 раз ввели неверный пароль, пользователь заблокирован!',
  'foot_menu' => 'Нижнее меню',
  'form_feedback' => 'Форма обратной связи',
  'foto' => 'Фото',
  'full_access' => 'Полный доступ',
  'gallery' => 'Галерея',
  'group' => 'Группа',
  'icon' => 'Иконка',
  'img' => 'Изображение',
  'in_footer' => 'в подвале',
  'in_header' => 'в шапке',
  'is_work' => 'Эта страница в разработке',
  'key' => 'Ключ',
  'lang' => 'Язык',
  'lang_ver' => 'Языковые версии',
  'lang_ver_page' => 'Языковые версии страницы',
  'lang_versions' => 'Языковые версии сайта',
  'link' => 'Ссылка',
  'list' => 'Список',
  'locked' => 'Заблокироно',
  'login' => 'Логин',
  'loginin' => 'Войти',
  'logout' => 'Выйти',
  'main_menu' => 'Главное меню',
  'mainpage' => 'Главная',
  'message' => 'Сообщение',
  'module' => 'Модуль',
  'more' => 'Подробнее',
  'name' => 'Название',
  'name_person' => 'Имя',
  'new' => 'Новый',
  'next' => 'Следующий',
  'no' => 'Нет',
  'no_params' => 'Недостаточно параметров',
  'not_saved' => 'Не сохранено',
  'on_the_main' => 'на главной',
  'open' => 'Открыто',
  'page' => 'Страница',
  'page_not_found' => 'Страница не найдена',
  'page_unavailable' => 'Страница недоступна',
  'params' => 'Параметры',
  'params_acc' => 'Параметры доступа',
  'password' => 'Пароль',
  'pers_data' => 'Персональные данные',
  'phone' => 'Телефон',
  'pict' => 'Изображение',
  'picts' => 'Изображения',
  'profile' => 'Профиль',
  'publish' => 'Опубликовать контент',
  'remaining' => 'Осталось',
  'remaining_2' => 'Осталась',
  'remember_me' => 'Запомнить меня',
  'remove' => 'Удалить',
  'remove_record' => 'Удалить запись?',
  'req_field' => 'Обязательное поле',
  'review' => 'Отзыв',
  'reviews' => 'Отзывы',
  'saved_ok' => 'Изменения сохранены',
  'search' => 'Поиск',
  'select_section_left' => 'Чтобы изменить содержимое страницы выберите нужный раздел слева',
  'send' => 'Отправить',
  'short_desc' => 'Краткое описание',
  'show_on_mainpage' => 'Показывать на главной',
  'site' => 'Сайт',
  'sitemap' => 'Дерево сайта',
  'social' => 'Социальные сети',
  'sorting' => 'Изменить порядок',
  'stop_public' => 'Контент опубликован',
  'theme' => 'Тема',
  'theme_review' => 'Тема отзыва',
  'title' => 'Заголовок',
  'update' => 'Сохранить',
  'upl_failed' => 'Ошибка загрузки файла',
  'url' => 'Url',
  'user_not_found' => 'Пользователь не найден',
  'view' => 'Просмотр',
  'wrong_pass' => 'Неверный пароль',
  'yes' => 'Да',
  'your' => 'Ваш',
  'your2' => 'Ваш',
  'short_name' => 'Краткое название',
);
$translate['ua'] = array(
  'access' => 'Доступ',
  'access_denied' => 'Доступ закрито',
  'access_locked' => 'Доступ заблоковано',
  'add' => 'Додати',
  'added_ok' => 'Запис успішно додано',
  'address' => 'Адреса',
  'admin_header' => 'Система управління сайтом',
  'and' => 'і',
  'apply' => 'Застосувати',
  'attempt' => 'спроби',
  'attempt_2' => 'спроба',
  'banners' => 'Банери',
  'cancel' => 'Відмінити',
  'cant_del' => 'Неможливо видалити',
  'category' => 'Категорія',
  'check_fields' => 'Перевірьте поля',
  'choose' => 'Виберіть',
  'close' => 'Закрито',
  'close2' => 'Закрити',
  'codes_counters' => 'Коди лічильників',
  'content' => 'Контент',
  'counters' => 'Лічильники',
  'create' => 'Створити',
  'create_water' => 'Додати захист зображень',
  'ctrl' => 'Контролер',
  'date' => 'Дата',
  'default' => 'по замовчуванню',
  'deleted_ok' => 'Успішно видалено',
  'description' => 'Опис',
  'edit' => 'Редагувати',
  'error_authorization' => 'Час сессії вийшов. Авторизуйтесь в системі ще раз.',
  'exists_email' => 'Вже існує користувач з таким email',
  'exists_login' => 'Вже існує користувач з таким логіном',
  'fields_seo' => 'Информация SEO',
  'file' => 'Файл',
  'fill_the_fields' => 'Заповніть поля',
  'find' => 'Знайти',
  'five_times_wrong' => 'Ви 5 разів ввели невірний пароль, користувача заблоковано!',
  'foot_menu' => 'Нижнє меню',
  'form_feedback' => 'Форма зворотнього зв\'язку',
  'foto' => 'Фото',
  'full_access' => 'Повний доступ',
  'gallery' => 'Гелерея',
  'group' => 'Група',
  'icon' => 'Іконка',
  'img' => 'Зображення',
  'in_footer' => 'у підвалі',
  'in_header' => 'в шапці',
  'is_work' => 'Сторінка знаходиться в розробці',
  'key' => 'Ключ',
  'lang' => 'Мова',
  'lang_ver' => 'Мовні версії',
  'lang_ver_page' => 'Мовні версії сторінки',
  'lang_versions' => 'Мовні версії сайту',
  'link' => 'Посилання',
  'list' => 'Список',
  'locked' => 'Заблоковано',
  'login' => 'Логін',
  'loginin' => 'Увійти',
  'logout' => 'Вийти',
  'main_menu' => 'Головне меню',
  'mainpage' => 'Головна',
  'message' => 'Повідомлення',
  'module' => 'Модуль',
  'more' => 'Детальніше',
  'name' => 'Назва',
  'name_person' => 'Ім\'я',
  'new' => 'Новий',
  'next' => 'Наступний',
  'no' => 'Ні',
  'no_params' => 'Недостатньо параметрів',
  'not_saved' => 'Не збережено',
  'on_the_main' => 'на головній',
  'open' => 'Відкрито',
  'page' => 'Сторінка',
  'page_not_found' => 'Сторінка не знайдена',
  'page_unavailable' => 'Сторінка недоступна',
  'params' => 'Параметри',
  'params_acc' => 'Параметри доступу',
  'password' => 'Пароль',
  'pers_data' => 'Персональні дані',
  'phone' => 'Телефон',
  'pict' => 'Зображення',
  'picts' => 'Зображення',
  'profile' => 'Профіль',
  'publish' => 'Опублікувати контент',
  'remaining' => 'Залишилось',
  'remaining_2' => 'Залишилась',
  'remember_me' => 'Запам\'ятати мене',
  'remove' => 'Видалити',
  'remove_record' => 'Видалити запис?',
  'req_field' => 'Обов\'язкове поле',
  'review' => 'Відгук',
  'reviews' => 'Відгуки',
  'saved_ok' => 'Зміни збережено',
  'search' => 'Пошук',
  'select_section_left' => 'Щоб змінити вміст сторінки виберіть потрібний розділ зліва',
  'send' => 'Відправити',
  'short_desc' => 'Короткий опис',
  'show_on_mainpage' => 'Показувати на головній',
  'site' => 'Сайт',
  'sitemap' => 'Дерево сайту',
  'social' => 'Соціальні мережі',
  'sorting' => 'Змінити порядок',
  'stop_public' => 'Контент опублікований',
  'theme' => 'Тема',
  'theme_review' => 'Тема відгуку',
  'title' => 'Заголовок',
  'update' => 'Зберегти',
  'upl_failed' => 'Помилка завантаження файлу',
  'url' => 'Url',
  'user_not_found' => 'Користувача не знайдено',
  'view' => 'Перегляд',
  'wrong_pass' => 'Невірний пароль',
  'yes' => 'Так',
  'your' => 'Ваш',
  'your2' => 'Ваше',
  'short_name' => 'Коротка назва',
);
$col = new TranslateCollection();
foreach ($translate[$deflang] as $key => $value) {
  $item = $col->add(array(
      'lang' => array(1),
      'name' => $key,
      'value1' => $value,
    ));
}

$t1 = new SettTable();
$t2 = new settHaslangTable();
$t2->dropTable();
$t1->dropTable();
$t1->createTable();

$col = new SettCollection();
$col->add(array(
        'lang' => array(1),
        'counters' => '',
        'countershead' => '',
        'countersmain' => '',
        'email' => 'info@',
        'phone' => '',
        'facebook' => '',
        'twitter' => '',
        'google' => '',
        'youtube' => '',
        'pinterest' => '',
        'yelp' => '',
        'vk' => '',
        'linkedin' => '',
));


$t1 = new urlHasbannerTable();
$t1->dropTable();
$t = new BannerTable();
$tbl = new bannerHaslangTable();
$tbl->dropTable();
$t->dropTable();
$t->createTable();

$tu = new UrlTable();
$t1 = new SectionTable();
$t2 = new sectionHaslangTable();
$art = new ArticleTable();
$arthl = new articleHaslangTable();
$artf = new ArticlefileTable();
$artfhl = new articlefileHaslangTable();

$artfhl->dropTable();
$artf->dropTable();
$arthl->dropTable();
$art->dropTable();
$t2->dropTable();
$t1->dropTable();
$tu->dropTable();
$tu->createTable();
$t1->createTable();
$art->createTable();
$artf->createTable();

$db->query("SET foreign_key_checks = 1");


//v 3.5 by ivander

//logsmail - save all outcomming messages to database
$logsmail = new LogsmailTable();
$logsmail->createTable();

$callback = new CallbackTable();
$callback->createTable();

?>