<?php
require 'autoload.php';
if (!Admin::getAuthenticatedInstance()) {
  return;
}
  function translit($content){
    $transA=array('А'=>'a','Б'=>'b','В'=>'v','Г'=>'g','Ґ'=>'g','Д'=>'d','Е'=>'e','Є'=>'e','Ё'=>'io','Ж'=>'zh','З'=>'z','И'=>'y','І'=>'i','Й'=>'y','Ї'=>'i','К'=>'k','Л'=>'l','М'=>'m','Н'=>'n','О'=>'o','П'=>'p','Р'=>'r','С'=>'s','Т'=>'t','У'=>'u','Ў'=>'u','Ф'=>'f','Х'=>'h','Ц'=>'ts','Ч'=>'ch','Ш'=>'sh','Щ'=>'shch','Ъ'=>'','Ы'=>'y','Ь'=>'','Э'=>'e','Ю'=>'iu','Я'=>'ia');
    $transB=array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','ґ'=>'g','д'=>'d','е'=>'e','ё'=>'io','є'=>'e','ж'=>'zh','з'=>'z','и'=>'y','і'=>'i','й'=>'y','ї'=>'i','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ў'=>'u','ф'=>'f','х'=>'h','ц'=>'ts','ч'=>'ch','ш'=>'sh','щ'=>'shch','ъ'=>'','ы'=>'y','ь'=>'','э'=>'e','ю'=>'iu','я'=>'ia','&quot;'=>'','&amp;'=>'','µ'=>'u','№'=>'');
//    $content=trim(strip_tags_smart($content));
    $content=strtr($content,$transA);
    $content=strtr($content,$transB);
    $content=preg_replace("/\s+/ums","-",$content);
    $content=preg_replace('/[\-]+/ui','-',$content);
    $content=preg_replace('/[\.]+/u','_',$content);
    $content=preg_replace("/[^a-z0-9\_\-\.]+/umi","",$content);
    $content=str_replace("/[_]+/u","_",$content);
    return $content;
  }
if ($_FILES['file']['name']) {
    if (!$_FILES['file']['error']) {
        $fileName = $_FILES[ 'file' ]['name'];
        $pos = strrpos($fileName,'.');
        $ext = substr($fileName,$pos+1);
        $name = substr($fileName,0,$pos);
        $filename = translit($name) . '.' . $ext;
        $destination = 'content/upload/' . $filename; //change this directory
        $location = $_FILES["file"]["tmp_name"];
        move_uploaded_file($location, $destination);
        echo Settings::get('defaulturl') . 'content/upload/' . $filename;//change this URL
    }
    else
    {
      echo  $message = 'Ooops!  Your upload triggered the following error:  '.$_FILES['file']['error'];
    }
} else {
  echo '';
}
?>
