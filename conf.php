<?php
# заполните
$this->params = array(
  # доступ к базе данных
  'DB' => array(
    'user' => 'root',
    'pass' => '',
    'database' => 'food',
    'host' => 'localhost',
//    'database' => 'mwb_new',
//    'host' => 'localhost',
//    'user' => 'mwb_new',
//    'pass' => 'S4_wOfmfC4xT',
  ),
  'cookieurl' => '/',
  # domen
  'url' => 'http://food/index.php',
  'defaulturl' => 'http://food/',
  'baseurl' => '/admin/',
  'd' => '/',

  'deflang' => 'ru',
  'lang' => array('ru' => 'Русский'),
  # количество и порядок списка языков сайта
  // 'lang_index' => array(1 => 'ru'),
  // 'lang_id' => array('ru' => 1),
  
  'logo_start' => 'admin/img/logo-IU.png',
  'encoding' => 'utf-8',

  'Smarty' => array(
    'templates' => 'view',
    'compile' => 'smarty/templates_c',
    'cache' => 'smarty/cache',
    'config' => 'smarty/configs'
  ),

  'months' => array(
            'en' => array(
                     '1' => 'january',
                     '2' => 'february',
                     '3' => 'march',
                     '4' => 'april',
                     '5' => 'may',
                     '6' => 'june',
                     '7' => 'july',
                     '8' => 'august',
                     '9' => 'september',
                     '10' => 'october',
                     '11' => 'november',
                     '12' => 'december',
                    ),
            'ru' => array(
                     '1' => 'января',
                     '2' => 'февраля',
                     '3' => 'марта',
                     '4' => 'апреля',
                     '5' => 'мая',
                     '6' => 'июня',
                     '7' => 'июля',
                     '8' => 'августа',
                     '9' => 'сентября',
                     '10' => 'октября',
                     '11' => 'ноября',
                     '12' => 'декабря',
                    ),
            'ua' => array(
                     '1' => 'січня',
                     '2' => 'лютого',
                     '3' => 'березня',
                     '4' => 'квітня',
                     '5' => 'травня',
                     '6' => 'червня',
                     '7' => 'липня',
                     '8' => 'серпня',
                     '9' => 'вересня',
                     '10' => 'жовтня',
                     '11' => 'листопада',
                     '12' => 'грудня',
                    ),
            ),


);
?>